/**
 * Executor class for a simple interpreter.
 *
 * (c) 2020 by Ronald Mak
 * Department of Computer Science
 * San Jose State University
 */
#include <iostream>
#include <string>
#include <vector>
#include <set>

#include "../Object.h"
#include "../intermediate/Symtab.h"
#include "../intermediate/Node.h"
#include "Executor.h"

namespace backend {

using namespace std;
using namespace intermediate;

set<NodeType> Executor::singletons;
set<NodeType> Executor::relationals;

void Executor::initialize()
{
    singletons.insert(VARIABLE);
    singletons.insert(INTEGER_CONSTANT);
    singletons.insert(REAL_CONSTANT);
    singletons.insert(STRING_CONSTANT);

    relationals.insert(EQ);
    relationals.insert(LT);
    relationals.insert(NEQ);
    relationals.insert(LTEQ);
    relationals.insert(GT);
    relationals.insert(GTEQ);
    relationals.insert(AND);
    relationals.insert(OR);
}

Object Executor::visit(Node *node)
{
    switch (node->type)
    {
        case PROGRAM :  return visitProgram(node);

        case COMPOUND :
        case ASSIGN :
        case LOOP :
        case WRITE :
        case WRITELN :  return visitStatement(node);

        case TEST:      return visitTest(node);

        case IF :   return visitIf(node);
        case CASE: return visitCase(node);

        default :       return visitExpression(node);
    }
}

Object Executor::visitProgram(Node *programNode)
{
    Node *compoundNode = programNode->children[0];
    return visit(compoundNode);
}

Object Executor::visitStatement(Node *statementNode)
{
    lineNumber = statementNode->lineNumber;

    switch (statementNode->type)
    {
        case COMPOUND :  return visitCompound(statementNode);
        case ASSIGN :    return visitAssign(statementNode);
        case LOOP :      return visitLoop(statementNode);
        case WRITE :     return visitWrite(statementNode);
        case WRITELN :   return visitWriteln(statementNode);


        default :        return Object();
    }
}

Object Executor::visitCompound(Node *compoundNode)
{
    for (Node *statementNode : compoundNode->children) visit(statementNode);

    return Object();
}

Object Executor::visitAssign(Node *assignNode)
{
    Node *lhs = assignNode->children[0];
    Node *rhs = assignNode->children[1];

    // Evaluate the right-hand-side expression;
    double value = visit(rhs).D;

    // Store the value into the variable's symbol table entry.
    string variableName = lhs->text;
    SymtabEntry *variableId = lhs->entry;
    variableId->setValue(value);

    return Object();
}

Object Executor::visitLoop(Node *loopNode)
{
    bool b = false;
    do
    {
        for (Node *node : loopNode->children)
        {
            Object value = visit(node);  // statement or test

            // Evaluate the test condition. Stop looping if true.
            b = (node->type == TEST) && value.B;
            if (b) break;
        }
    } while (!b);

    return Object();
}

Object Executor::visitIf(Node *ifNode)
{
    vector<Node *> children = ifNode->children;
    Object value = visit(children.at(0));
    if(value.B) {
        visit(children.at(1));
    }
    else {
        if(children.size() == 3) {
            visit(children.at(2));
        }
    }

    return Object();
}

Object Executor::visitCase(Node *caseNode)
{
    vector<Node *> children = caseNode->children;
    Object value = visit(children.at(0));
    double compare_value = value.D;
    double condition;
    bool condition_found = false;

    for(vector<Node *>::iterator case_compound = children.begin() + 1; case_compound != children.end(); case_compound++) {
        Node *statement = (*case_compound)->children.at((*case_compound)->children.size() - 1);
        for(vector<Node *>::iterator case_condition = (*case_compound)->children.begin(); case_condition != (*case_compound)->children.end() - 1; case_condition++) {
            value = visit(* case_condition);
            condition = value.D;
            if (compare_value == condition) {
                visit(statement);
                condition_found = true;
                break;
            }

        }
        if (condition_found) {
            break;
        }
    }

    return Object();
}


Object Executor::visitTest(Node *testNode)
{
    return visit(testNode->children[0]);
}

Object Executor::visitWrite(Node *writeNode)
{
    printValue(writeNode->children);
    return Object();
}

Object Executor::visitWriteln(Node *writelnNode)
{
    if (writelnNode->children.size() > 0) printValue(writelnNode->children);
    cout << endl;

    return Object();
}

void Executor::printValue(vector<Node *> children)
{
    long fieldWidth    = -1;
    long decimalPlaces = 0;

    // Use any specified field width and count of decimal places.
    if (children.size() > 1)
    {
        fieldWidth = visit(children[1]).L;

        if (children.size() > 2)
        {
            decimalPlaces = visit(children[2]).L;
        }
    }

    // Print the value with a format.
    Node *valueNode = children[0];
    if (valueNode->type == VARIABLE)
    {
        string format = "%";
        if (fieldWidth >= 0)    format += to_string(fieldWidth);
        if (decimalPlaces >= 0) format += "." + to_string(decimalPlaces);
        format += "f";

        double value = visit(valueNode).D;
        printf(format.c_str(), value);
    }
    else  // Node *type STRING_CONSTANT
    {
        string format = "%";
        if (fieldWidth > 0) format += to_string(fieldWidth);
        format += "s";

        string value = visit(valueNode).S;
        printf(format.c_str(), value.c_str());
    }
}

Object Executor::visitExpression(Node *expressionNode)
{
    // Single-operand expressions.
    if (singletons.find(expressionNode->type) != singletons.end())
    {
        switch (expressionNode->type)
        {
            case VARIABLE         : return visitVariable(expressionNode);
            case INTEGER_CONSTANT : return visitIntegerConstant(expressionNode);
            case REAL_CONSTANT    : return visitRealConstant(expressionNode);
            case STRING_CONSTANT  : return visitStringConstant(expressionNode);

            default: return Object();
        }
    }

    if (expressionNode->type  == NOT) {
        Object value = visit(expressionNode->children[0]);
        return !value.B;
    }

    // Binary expressions.

    double value1, value2;
    if (expressionNode->type == AND || expressionNode->type == OR) {
        value1 = visit(expressionNode->children[0]).B ? 1 : 0;
        value2 = visit(expressionNode->children[1]).B ? 1 : 0;
    }
    else {
        value1 = visit(expressionNode->children[0]).D;
        value2 = visit(expressionNode->children[1]).D;
    }

    // Relational expressions.
    if (relationals.find(expressionNode->type) != relationals.end())
    {
        bool value = false;

        switch (expressionNode->type)
        {
            case EQ     : value = value1 == value2; break;
            case LT     : value = value1 <  value2; break;
            case NEQ    : value = value1 != value2; break;
            case LTEQ   : value = value1 <=  value2; break;
            case GT     : value = value1  > value2; break;
            case GTEQ   : value = value1 >= value2; break;
            case AND    : value = value1 && value2; break;
            case OR     : value = value1 ||  value2; break;

            default : break;
        }

        return Object(value);
    }

    double value = 0.0;

    // Arithmetic expressions.
    switch (expressionNode->type)
    {
        case ADD :      value = value1 + value2; break;
        case SUBTRACT : value = value1 - value2; break;
        case MULTIPLY : value = value1 * value2; break;

        case DIVIDE :
        {
            if (value2 != 0.0) value = value1/value2;
            else
            {
                runtimeError(expressionNode, "Division by zero");
                return new Object(0.0);
            }

            break;
        }

        default : break;
    }

    return Object(value);
}

Object Executor::visitVariable(Node *variableNode)
{
    // Obtain the variable's value from its symbol table entry.
    SymtabEntry *variableId = variableNode->entry;
    double value = variableId->getValue();

    return value;
}

Object Executor::visitIntegerConstant(Node *integerConstantNode)
{
    return integerConstantNode->value;
}

Object Executor::visitRealConstant(Node *realConstantNode)
{
    return realConstantNode->value;
}

Object Executor::visitStringConstant(Node *stringConstantNode)
{
    return stringConstantNode->value;
}

void Executor::runtimeError(Node *node, string message)
{
    printf("RUNTIME ERROR at line %d: %s: %s\n",
           lineNumber, message.c_str(), node->text.c_str());
    exit(-2);
}

}  // namespace backend
