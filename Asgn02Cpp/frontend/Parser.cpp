/**
 * Parser class for a simple interpreter.
 *
 * (c) 2020 by Ronald Mak
 * Department of Computer Science
 * San Jose State University
 */
#include <string>
#include <map>

#include "Token.h"
#include "Parser.h"

namespace frontend {

using namespace std;

set<TokenType> Parser::statementStarters;
set<TokenType> Parser::statementFollowers;
set<TokenType> Parser::relationalOperators;
set<TokenType> Parser::simpleExpressionOperators;
set<TokenType> Parser::termOperators;

void Parser::initialize()
{
    // Tokens that can start a statement.
    statementStarters.insert(BEGIN);
    statementStarters.insert(IDENTIFIER);
    statementStarters.insert(REPEAT);
    statementStarters.insert(WHILE); // Assignment3
    statementStarters.insert(FOR); // Assignment3
    statementStarters.insert(TokenType::WRITE);
    statementStarters.insert(TokenType::WRITELN);


    // Tokens that can immediately follow a statement.
    statementFollowers.insert(SEMICOLON);
    statementFollowers.insert(END);
    statementFollowers.insert(UNTIL);
    statementFollowers.insert(DO);    // Assignment3
    statementFollowers.insert(TO);    // Assignment3

    statementFollowers.insert(END_OF_FILE);

    relationalOperators.insert(EQUALS);
    relationalOperators.insert(NOT_EQUALS);
    relationalOperators.insert(LESS_THAN);
    relationalOperators.insert(LESS_EQUALS);
    relationalOperators.insert(GREATER_THAN);
    relationalOperators.insert(GREATER_EQUALS);


    simpleExpressionOperators.insert(PLUS);
    simpleExpressionOperators.insert(MINUS);

    termOperators.insert(STAR);
    termOperators.insert(SLASH);
    termOperators.insert(DIV);
}

Node *Parser::parseProgram()
{
    Node *programNode = new Node(NodeType::PROGRAM);

    currentToken = scanner->nextToken();  // first token!

    if (currentToken->type == TokenType::PROGRAM)
    {
        currentToken = scanner->nextToken();  // consume PROGRAM
    }
    else syntaxError("Expecting PROGRAM");

    if (currentToken->type == IDENTIFIER)
    {
        string programName = currentToken->text;
        symtab->enter(programName);
        programNode->text = programName;

        currentToken = scanner->nextToken();  // consume program name
    }
    else syntaxError("Expecting program name");

    if (currentToken->type == SEMICOLON)
    {
        currentToken = scanner->nextToken();  // consume ;
    }
    else syntaxError("Missing ;");

    if (currentToken->type != BEGIN) syntaxError("Expecting BEGIN");

    // The PROGRAM node adopts the COMPOUND tree.
    programNode->adopt(parseCompoundStatement());

    if (currentToken->type == SEMICOLON) syntaxError("Expecting .");
    return programNode;
}

Node *Parser::parseStatement()
{
    Node *stmtNode = nullptr;
    int savedLineNumber = currentToken->lineNumber;
    lineNumber = savedLineNumber;

    switch (currentToken->type)
    {
        case IDENTIFIER : stmtNode = parseAssignmentStatement(); break;
        case BEGIN :      stmtNode = parseCompoundStatement();   break;
        case REPEAT :     stmtNode = parseRepeatStatement();     break;

        case WHILE :     stmtNode = parseWhileStatement();     break;
        case IF :     stmtNode = parseIfStatement();     break;
        case FOR :     stmtNode = parseForStatement();     break;
        case TokenType::CASE : stmtNode = parseCaseStatement();     break;

        case WRITE :      stmtNode = parseWriteStatement();      break;
        case WRITELN :    stmtNode = parseWritelnStatement();    break;
        case SEMICOLON :  stmtNode = nullptr; break;  // empty statement

        default : syntaxError("Unexpected token");
    }

    if (stmtNode != nullptr) stmtNode->lineNumber = savedLineNumber;
    return stmtNode;
}

Node *Parser::parseAssignmentStatement()
{
    // The current token should now be the left-hand-side variable name.

    Node *assignmentNode = new Node(ASSIGN);

    // Enter the variable name into the symbol table
    // if it isn't already in there.
    string variableName = currentToken->text;
    SymtabEntry *variableId = symtab->lookup(toLowerCase(variableName));
    if (variableId == nullptr) variableId = symtab->enter(variableName);

    // The assignment node adopts the variable node as its first child.
    Node *lhsNode  = new Node(VARIABLE);
    lhsNode->text  = variableName;
    lhsNode->entry = variableId;
    assignmentNode->adopt(lhsNode);

    currentToken = scanner->nextToken();  // consume the LHS variable;

    if (currentToken->type == COLON_EQUALS)
    {
        currentToken = scanner->nextToken();  // consume :=
    }
    else syntaxError("Missing :=");

    // The assignment node adopts the expression node as its second child.
    Node *rhsNode = parseExpression();
    assignmentNode->adopt(rhsNode);

    return assignmentNode;
}

Node *Parser::parseCompoundStatement()
{
    Node *compoundNode = new Node(COMPOUND);
    compoundNode->lineNumber = currentToken->lineNumber;

    currentToken = scanner->nextToken();  // consume BEGIN
    parseStatementList(compoundNode, END);

    if (currentToken->type == END)
    {
        currentToken = scanner->nextToken();  // consume END
    }
    else syntaxError("Expecting END");

    return compoundNode;
}

void Parser::parseStatementList(Node *parentNode, TokenType terminalType)
{
    while (   (currentToken->type != terminalType)
           && (currentToken->type != END_OF_FILE))
    {
        Node *stmtNode = parseStatement();
        if (stmtNode != nullptr) parentNode->adopt(stmtNode);

        // A semicolon separates statements.
        if (currentToken->type == SEMICOLON)
        {
            while (currentToken->type == SEMICOLON)
            {
                currentToken = scanner->nextToken();  // consume ;
            }
        }
        else if (statementStarters.find(currentToken->type) !=
                                                        statementStarters.end())
        {
            syntaxError("Missing ;");
        }
    }
}

Node *Parser::parseRepeatStatement()
{
    // The current token should now be REPEAT.

    // Create a LOOP node.
    Node *loopNode = new Node(LOOP);
    currentToken = scanner->nextToken();  // consume REPEAT

    parseStatementList(loopNode, UNTIL);

    if (currentToken->type == UNTIL)
    {
        // Create a TEST node. It adopts the test expression node.
        Node *testNode = new Node(TEST);
        lineNumber = currentToken->lineNumber;
        testNode->lineNumber = lineNumber;
        currentToken = scanner->nextToken();  // consume UNTIL

        testNode->adopt(parseExpression());

        // The LOOP node adopts the TEST node as its final child.
        loopNode->adopt(testNode);
    }
    else syntaxError("Expecting UNTIL");

    return loopNode;
}
Node *Parser::parseWhileStatement()
{
    // The current token should now be WHILE.

    // Create a LOOP node.
    Node *loopNode = new Node(LOOP);
    currentToken = scanner->nextToken();  // consume WHILE

    // Expression While a < b
    Node *testNode = new Node(TEST);
    lineNumber = currentToken->lineNumber;
    testNode->lineNumber = lineNumber;

    Node *notNode = new Node(NodeType::NOT);
    notNode->adopt(parseExpression());
    testNode->adopt(notNode);

    // The LOOP node adopts the TEST node as its first child.
    loopNode->adopt(testNode);

    if (currentToken->type == DO)
    {
        currentToken = scanner->nextToken();  // consume DO

        if (currentToken->type == BEGIN) {
            currentToken = scanner->nextToken();  // consume BEGIN
            parseStatementList(loopNode, END);
            currentToken = scanner->nextToken();  // consume END
        }
        else syntaxError("Expecting BEGIN");

    }
    else syntaxError("Expecting DO");


    return loopNode;
}

Node *Parser::parseIfStatement()
{
    // Create a IF node. It adopts the test expression node.
    Node *ifNode = new Node(NodeType::IF);
    currentToken = scanner->nextToken();  // consume IF

    Node *expNode = parseExpression();
    lineNumber = currentToken->lineNumber;
    expNode->lineNumber = lineNumber;

    ifNode->adopt(expNode);

    if (currentToken->type == THEN) {
        currentToken = scanner->nextToken();  // consume THEN
        if (currentToken->type == BEGIN) {

            Node *compoundNode = new Node(COMPOUND);
            currentToken = scanner->nextToken();  // consume BEGIN
            parseStatementList(compoundNode, END);
            currentToken = scanner->nextToken();  // consume END
            ifNode->adopt(compoundNode);
        }
        else {
            ifNode->adopt(parseStatement());
            //currentToken = scanner->nextToken();
        }
        if (currentToken->type == ELSE) {
            currentToken = scanner->nextToken();  // consume ELSE
            if (currentToken->type == BEGIN) {
                Node *compoundNode = new Node(COMPOUND);
                currentToken = scanner->nextToken();  // consume BEGIN
                parseStatementList(compoundNode, END);
                currentToken = scanner->nextToken();  // consume END
                ifNode->adopt(compoundNode);
            } else {
                ifNode->adopt(parseStatement());
            }
        }
    }
    else {
        syntaxError("Expecting THEN");
    }
    return ifNode;
}

Node *Parser::parseConstantList(Node *parentNode) {

    Node *constantNode;
    while (currentToken->type != COLON) {
        constantNode = parseFactor();
        parentNode->adopt(constantNode);

        if (currentToken->type == COMMA) {
            currentToken = scanner->nextToken(); // consume comma
        }
        else if (currentToken->type != COLON) {
            syntaxError("Expecting COMMA");
        }
    }
    currentToken = scanner->nextToken(); // consume colon

}
Node *Parser::parseCaseStatement() {
    //Create CASE node
    Node *caseNode = new Node(NodeType::CASE);
    currentToken = scanner->nextToken(); // consume CASE

    Node *expNode = parseExpression();
    lineNumber = currentToken->lineNumber;
    expNode->lineNumber = lineNumber;
    caseNode->adopt(expNode);

    if (currentToken->type == OF) {
        currentToken = scanner->nextToken(); //consume OF
        while (currentToken->type != END) {
            Node *compoundNode = new Node(COMPOUND);
            parseConstantList(compoundNode);
            Node *caseStatement;
            if (currentToken->type == BEGIN) {

                caseStatement = new Node(COMPOUND);
                currentToken = scanner->nextToken();  // consume BEGIN
                parseStatementList(caseStatement, END);
                currentToken = scanner->nextToken();  // consume END
            } else {
                caseStatement = parseStatement();
            }
            if (currentToken->type != END) {
                if (currentToken->type != SEMICOLON) {
                    syntaxError("Expecting SEMICOLON");
                }
                currentToken = scanner->nextToken();  // consume SEMICOLON
            }

            compoundNode->adopt(caseStatement);
            caseNode->adopt(compoundNode);

        }

    }
    else {
        syntaxError("Expecting OF");
    }
    if(currentToken->type == END) {
        currentToken = scanner->nextToken();  // consume END
    }
    else {
        syntaxError("Expecting END");
    }

    return caseNode;
}

/*
    1. Identifier
    2. Loop
        - Loop Node
        - expresion
            + TEST Node
        - TO/ DOWNTO (expression)
        - DO (statement)

 */
Node *Parser::parseForStatement()
{
    bool up = true;
    currentToken = scanner->nextToken(); //consume FOR

    Node *compoundNode = new Node(COMPOUND);
    Node *assignNode = parseStatement();
    compoundNode->adopt(assignNode);
    // Create a LOOP node.
    Node *loopNode = new Node(LOOP);

    Node *var = assignNode->children[0];

    // TO / DOWN TO - Expression
    if (currentToken->type == TO)
    {
        currentToken = scanner->nextToken(); //consume TO

        // Create a TEST node. It adopts the test expression node.
        Node *testNode = new Node(TEST);
        lineNumber = currentToken->lineNumber;
        testNode->lineNumber = lineNumber;

        // Create a GT node.
        Node *exprNode = parseExpression();
        Node *gt = new Node(GT);
        gt->adopt(var);
        gt->adopt(exprNode);

        testNode->adopt(gt);

        // The LOOP node adopts the TEST node as its final child.
        loopNode->adopt(testNode);
    }

    else if (currentToken->type == DOWNTO)
    {
        up = false;

        currentToken = scanner->nextToken(); //consume DOWNTO

        // Create the TEST node
        Node *testNode = new Node(TEST);
        lineNumber = currentToken->lineNumber;
        testNode->lineNumber = lineNumber;

        // Create the GT node

        Node *exprNode = parseExpression();
        Node *lt = new Node(LT);

        // The GT node adopts the VAR node as its final child.
        lt->adopt(var);

        // The GT node adopts the EXPRESSION node as its final child.
        lt->adopt(exprNode);

        // The TEST node adopts the LT node as its final child.
        testNode->adopt(lt);

        // The LOOP node adopts the TEST node as its final child.
        loopNode->adopt(testNode);
    }

    else syntaxError("Expecting DO");


    // DO - Statement
    if (currentToken->type == DO)
    {
        currentToken = scanner->nextToken();  // consume DO

        if (currentToken->type == BEGIN) {
            currentToken = scanner->nextToken();  // consume BEGIN
            parseStatementList(loopNode, END);
            currentToken = scanner->nextToken();  // consume END
        }
        else syntaxError("Expecting BEGIN");

    }
    else syntaxError("Expecting DO");

    Node *assignmentNode = new Node(ASSIGN);
    assignmentNode->adopt(var);
    Node *compute;
    if (up) {
        compute = new Node(ADD);
    }
    else {
        compute = new Node(SUBTRACT);
    }

    compute->adopt(var);
    Node *integerConstant = new Node(INTEGER_CONSTANT);
    integerConstant->value = Object((double) 1);
    compute->adopt(integerConstant);
    assignmentNode->adopt(compute);
    loopNode->adopt(assignmentNode);

    compoundNode->adopt(loopNode);
    return compoundNode;
}

Node *Parser::parseWriteStatement()
{
    // The current token should now be WRITE.

    // Create a WRITE node-> It adopts the variable or string node.
    Node *writeNode = new Node(NodeType::WRITE);
    currentToken = scanner->nextToken();  // consume WRITE

    parseWriteArguments(writeNode);
    if (writeNode->children.size() == 0)
    {
        syntaxError("Invalid WRITE statement");
    }

    return writeNode;
}

Node *Parser::parseWritelnStatement()
{
    // The current token should now be WRITELN.

    // Create a WRITELN node. It adopts the variable or string node.
    Node *writelnNode = new Node(NodeType::WRITELN);
    currentToken = scanner->nextToken();  // consume WRITELN

    if (currentToken->type == LPAREN) parseWriteArguments(writelnNode);
    return writelnNode;
}

void Parser::parseWriteArguments(Node *node)
{
    // The current token should now be (

    bool hasArgument = false;

    if (currentToken->type == LPAREN)
    {
        currentToken = scanner->nextToken();  // consume (
    }
    else syntaxError("Missing left parenthesis");

    if (currentToken->type == IDENTIFIER)
    {
        node->adopt(parseVariable());
        hasArgument = true;
    }
    else if (   (currentToken->type == CHARACTER)
             || (currentToken->type == STRING))
    {
        node->adopt(parseStringConstant());
        hasArgument = true;
    }
    else syntaxError("Invalid WRITE or WRITELN statement");

    // Look for a field width and a count of decimal places.
    if (hasArgument)
    {
        if (currentToken->type == COLON)
        {
            currentToken = scanner->nextToken();  // consume ,

            if (currentToken->type == INTEGER)
            {
                // Field width
                node->adopt(parseIntegerConstant());

                if (currentToken->type == COLON)
                {
                    currentToken = scanner->nextToken();  // consume ,

                    if (currentToken->type == INTEGER)
                    {
                        // Count of decimal places
                        node->adopt(parseIntegerConstant());
                    }
                    else syntaxError("Invalid count of decimal places");
                }
            }
            else syntaxError("Invalid field width");
        }
    }

    if (currentToken->type == RPAREN)
    {
        currentToken = scanner->nextToken();  // consume )
    }
    else syntaxError("Missing right parenthesis");
}

Node *Parser::parseExpression()
{
    // The current token should now be an identifier or a number.

    Node *lhsNode = nullptr, *current, *temp;
    if (currentToken->type == NOT) {
        lhsNode = new Node(NodeType::NOT);
        currentToken = scanner->nextToken();  // consume NOT
        lhsNode->adopt(parseExpression());
    }

//    while (currentToken->type == AND || currentToken->type == OR) {
//        if (currentToken->type == AND) {
//            current = new Node(NodeType::AND);
//        }
//        else if (currentToken->type == OR) {
//            current = new Node(NodeType::OR);
//        }
//        current->adopt(lhsNode);
//        currentToken = scanner->nextToken();  // consume OR
//        current->adopt(parseExpression());
//        lhsNode = current;
//    }

    while (currentToken->type == AND || currentToken->type == OR) {
        if (currentToken->type == AND) {
            current = new Node(NodeType::AND);
            if (lhsNode && lhsNode->type == NodeType::OR) {
                temp = lhsNode->children.at(1);
                lhsNode->children.pop_back();
                current->adopt(temp);

                currentToken = scanner->nextToken();  // consume AND
                current->adopt(parseExpression());
                lhsNode->adopt(current);
            }
            else {
                current->adopt(lhsNode);
                currentToken = scanner->nextToken();  // consume AND
                current->adopt(parseExpression());
                lhsNode = current;
            }
        }
        else if (currentToken->type == OR) {
            current = new Node(NodeType::OR);
            current->adopt(lhsNode);
            currentToken = scanner->nextToken();  // consume OR
            current->adopt(parseExpression());
            lhsNode = current;
        }
    }


    if (lhsNode) {
        return lhsNode;
    }

    // The expression's root node->
    Node *exprNode = parseSimpleExpression();

    // The current token might now be a relational operator.
    if (relationalOperators.find(currentToken->type) != relationalOperators.end())
    {

        TokenType tokenType = currentToken->type;
        Node *opNode = tokenType == EQUALS    ? new Node(EQ)
                    : tokenType == NOT_EQUALS ? new Node(NEQ)
                    : tokenType == LESS_THAN ? new Node(LT)
                    : tokenType == LESS_EQUALS ? new Node(LTEQ)
                    : tokenType == GREATER_THAN ? new Node(GT)
                    : tokenType == GREATER_EQUALS ? new Node(GTEQ)
                    :                          nullptr;

        currentToken = scanner->nextToken();  // consume relational operator

        // The relational operator node adopts the first simple expression
        // node as its first child and the second simple expression node
        // as its second child. Then it becomes the expression's root node.
        if (opNode != nullptr)
        {
            opNode->adopt(exprNode);
            opNode->adopt(parseSimpleExpression());
            exprNode = opNode;
        }
    }

    return exprNode;
}

Node *Parser::parseSimpleExpression()
{
    // The current token should now be an identifier or a number.
    // The simple expression's root node->
    Node *simpExprNode = parseTerm();

    // Keep parsing more terms as long as the current token
    // is a + or - operator.
    while (simpleExpressionOperators.find(currentToken->type) !=
                                                simpleExpressionOperators.end())
    {
        Node *opNode = currentToken->type == PLUS ? new Node(ADD)
                                                : new Node(SUBTRACT);

        currentToken = scanner->nextToken();  // consume the operator

        // The add or subtract node adopts the first term node as its
        // first child and the next term node as its second child.
        // Then it becomes the simple expression's root node.
        opNode->adopt(simpExprNode);
        opNode->adopt(parseTerm());
        simpExprNode = opNode;
    }

    return simpExprNode;
}

Node *Parser::parseTerm()
{
    // The current token should now be an identifier or a number.

    // The term's root node->
    Node *termNode = parseFactor();

    // Keep parsing more factors as long as the current token
    // is a * or / operator.
    while (termOperators.find(currentToken->type) != termOperators.end())
    {
        Node *opNode = currentToken->type == STAR ? new Node(MULTIPLY)
                                                : new Node(DIVIDE);

        currentToken = scanner->nextToken();  // consume the operator

        // The multiply or divide node adopts the first factor node as its
        // as its first child and the next factor node as its second child.
        // Then it becomes the term's root node.
        opNode->adopt(termNode);
        opNode->adopt(parseFactor());
        termNode = opNode;
    }

    return termNode;
}

Node *Parser::parseFactor()
{
    // The current token should now be an identifier or a number or (

    if      (currentToken->type == IDENTIFIER) return parseVariable();
    else if (currentToken->type == INTEGER)    return parseIntegerConstant();
    else if (currentToken->type == REAL)       return parseRealConstant();
    else if (currentToken->type == STRING)       return parseStringConstant();
    else if (currentToken->type == MINUS) {
        currentToken = scanner->nextToken();  // consume -
        Node *muliplierNode = new Node (MULTIPLY);
        Node *integerConstant = new Node(INTEGER_CONSTANT);
        integerConstant->value = Object((double) -1);
        muliplierNode->adopt(integerConstant);
        muliplierNode->adopt(parseFactor());
        return muliplierNode;
    }
    else if (currentToken->type == LPAREN)
    {
        currentToken = scanner->nextToken();  // consume (
        Node *exprNode = parseExpression();

        if (currentToken->type == RPAREN)
        {
            currentToken = scanner->nextToken();  // consume )
        }
        else syntaxError("Expecting )");

        return exprNode;
    }

    else syntaxError("Unexpected token");
    return nullptr;
}

Node *Parser::parseVariable()
{
    // The current token should now be an identifier.

    // Has the variable been "declared"?
    string variableName = currentToken->text;
    SymtabEntry *variableId = symtab->lookup(toLowerCase(variableName));
    if (variableId == nullptr) semanticError("Undeclared identifier");

    Node *node  = new Node(VARIABLE);
    node->text  = variableName;
    node->entry = variableId;

    currentToken = scanner->nextToken();  // consume the identifier
    return node;
}

Node *Parser::parseIntegerConstant()
{
    // The current token should now be a number.

    Node *integerNode = new Node(INTEGER_CONSTANT);
    integerNode->value = currentToken->value;

    currentToken = scanner->nextToken();  // consume the number
    return integerNode;
}

Node *Parser::parseRealConstant()
{
    // The current token should now be a number.

    Node *realNode = new Node(REAL_CONSTANT);
    realNode->value = currentToken->value;

    currentToken = scanner->nextToken();  // consume the number
    return realNode;
}

Node *Parser::parseStringConstant()
{
    // The current token should now be string.

    Node *stringNode = new Node(STRING_CONSTANT);
    stringNode->value = currentToken->value;

    currentToken = scanner->nextToken();  // consume the string
    return stringNode;
}

void Parser::syntaxError(string message)
{
    printf("SYNTAX ERROR at line %d: %s at '%s'\n",
           lineNumber, message.c_str(), currentToken->text.c_str());
    errorCount++;

    // Recover by skipping the rest of the statement.
    // Skip to a statement follower token.
    while (statementFollowers.find(currentToken->type) ==
                                                    statementFollowers.end())
    {
        currentToken = scanner->nextToken();
    }
}

void Parser::semanticError(string message)
{
    printf("SEMANTIC ERROR at line %d: %s at '%s'\n",
           lineNumber, message.c_str(), currentToken->text.c_str());
    errorCount++;
}

}  // namespace frontend
