

### To compile the project
```
#this will generate the Make file
cmake build . 
#this will generate the run binary file SimpleCpp
make 
```

### Test with sample output
```bash
./SimpleCpp -scan ScannerTest.txt
./SimpleCpp  -parse TestWhile.txt
./SimpleCpp  -execute TestWhile.txt

```

### Run to output
```bash
./SimpleCpp -parse TestIf.txt > Outputs/ParseTreeIf.out.txt 
./SimpleCpp -parse TestCase.txt > Outputs/ParseTreeCase.out.txt 
./SimpleCpp -parse TestFor.txt > Outputs/ParseTreeFor.out.txt 
./SimpleCpp -parse TestWhile.txt > Outputs/ParseTreeWhile.out.txt 

./SimpleCpp -execute TestCase.txt > Outputs/ExecuteTestCase.out.txt
./SimpleCpp -execute TestIf.txt > Outputs/ExecuteTestIf.out.txt
./SimpleCpp -execute TestFor.txt > Outputs/ExecuteTestFor.out.txt
./SimpleCpp -execute TestWhile.txt > Outputs/ExecuteTestWhile.out.txt
```