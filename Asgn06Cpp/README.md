

## To set up environment
```bash
export CLASSPATH="${PWD}/lib/antlr-4.9.1-complete.jar:$CLASSPATH"
alias antlr4="java -jar ${PWD}/lib/antlr-4.9.1-complete.jar"
alias grun='java org.antlr.v4.gui.TestRig'
```
## Create parser library from antlr4
```bash
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o generated Pascal.g4
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o generated Commander.g4
```
## Compile

### Compile the project with g++
```bash
g++ -std=c++11 -I${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/antlr4-runtime \
 -L${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/lib -I${PWD} \
 -I${PWD}/generated -lantlr4-runtime ${PWD}/generated/*.cpp \
 backend/compiler/*.cpp backend/converter/*.cpp backend/interpreter/*.cpp  \
 frontend/*.cpp intermediate/symtab/*.cpp intermediate/type/*.cpp intermediate/util/*.cpp \
 -o Assignment6 Pascal.cpp
```

### Compile the project with Cmake
```bash
cmake build .
make
```

## Run sample program
```bash
./Assignment6 -compile TestIf.pas
jasmin TestIf.j
java TestIf
```
### Generate outputs
```bash
mkdir -p outputs/jasmin  outputs/object outputs/runtime outputs/execute
pushd .; cd outputs/jasmin; ls -ltr ../../*.pas  | awk -F" " {'print $9'} | xargs -n 1 -I {} ../../Assignment6 -compile {}; popd
pushd .; cd outputs/jasmin; ls -ltr *.j  | awk -F" " {'print $9'} | xargs -n 1 -I {} jasmin {} -d ../object; popd
pushd .; cd outputs/object; ls -ltr *.class  | awk -F" " {'print $9'}  | awk -F"." {'print $1'} | xargs -n 1 -I {} bash -c "java {} > ../runtime/{}.out"; popd
pushd .; cd outputs/execute; ls -ltr ../../*.pas  | awk -F" " {'print $9'} |  awk -F"\/" {'print $3'} | xargs -n 1 -I {}  bash -c "../../Assignment6 -execute ../../{} > {}.out" ; popd
```