

### To compile the project
```
#this will generate the Make file
cmake build . 
#this will generate the run binary file SimpleCpp
make 
```

### Test with sample output
```bash
./SimpleCpp -scan ScannerTest.txt
```