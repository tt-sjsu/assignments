/**
 * Token class for a simple interpreter.
 *
 * (c) 2020 by Ronald Mak
 * Department of Computer Science
 * San Jose State University
 */
#include <string>
#include <map>
#include <ctype.h>

#include "../Object.h"
#include "Token.h"

namespace frontend {

using namespace std;

map<string, TokenType> Token::reservedWords;

void Token::initialize()
{
    reservedWords["PROGRAM"] = PROGRAM;
    reservedWords["BEGIN"]   = BEGIN;
    reservedWords["END"]     = END;
    reservedWords["REPEAT"]  = REPEAT;
    reservedWords["UNTIL"]   = UNTIL;
    reservedWords["WRITE"]   = WRITE;
    reservedWords["WRITELN"] = WRITELN;

    reservedWords["DIV"] = DIV;
    reservedWords["MOD"] = MOD;
    reservedWords["AND"] = AND;
    reservedWords["OR"] = OR;
    reservedWords["NOT"] = NOT;
    reservedWords["CONST"] = CONST;
    reservedWords["TYPE"] = TYPE;
    reservedWords["VAR"] = VAR;
    reservedWords["PROCEDURE"] = PROCEDURE;
    reservedWords["FUNCTION"] = FUNCTION;
    reservedWords["WHILE"] = WHILE;
    reservedWords["DO"] = DO;
    reservedWords["FOR"] = FOR;
    reservedWords["TO"] = TO;
    reservedWords["DOWNTO"] = DOWNTO;
    reservedWords["IF"] = IF;
    reservedWords["THEN"] = THEN;
    reservedWords["CASE"] = CASE;
    reservedWords["OF"] = OF;

    reservedWords["IDENTIFIER"] = IDENTIFIER;
    reservedWords["INTEGER"] = INTEGER;
    reservedWords["REAL"] = REAL;
    reservedWords["CHARACTER"] = CHARACTER;
    reservedWords["STRING"] = STRING;
    reservedWords["END_OF_FILE"] = END_OF_FILE;
    reservedWords["ERROR"] = ERROR;

}

Token *Token::Word(char firstChar, Source *source)
{
    Token *token = new Token(firstChar);
    token->lineNumber = source->lineNumber();

    // Loop to get the rest of the characters of the word token.
    // Append letters and digits to the token.
    for (char ch = source->nextChar(); isalnum(ch); ch = source->nextChar())
    {
        token->text += ch;
    }

    // Is it a reserved word or an identifier?
    string upper = toUpperCase(token->text);
    if (Token::reservedWords.find(upper) != Token::reservedWords.end())
    {
        token->type = Token::reservedWords[upper];
    }
    else
    {
        token->type = TokenType::IDENTIFIER;
    }

    return token;
}

Token *Token::Number(char firstChar, Source *source)
{
    Token *token = new Token(firstChar);
    token->lineNumber = source->lineNumber();
    int pointCount = 0;

    // Loop to get the rest of the characters of the number token.
    // Append digits to the token.
    for (char ch = source->nextChar();
         isdigit(ch) || (ch == '.');
         ch = source->nextChar())
    {
        if (ch == '.') pointCount++;
        token->text += ch;
    }

    // Integer constant.
    if (pointCount == 0)
    {
        token->type    = TokenType::INTEGER;
        token->value.L = stol(token->text);
        token->value.D = token->value.L;  // allow using integer value as double
    }

    // Real constant.
    else if (pointCount == 1)
    {
        token->type    = TokenType::REAL;
        token->value.D = stod(token->text);
    }

    else {
        tokenError(token, "Invalid number");
    }

    return token;
}

Token *Token::String(char firstChar, Source *source)
{
    Token *token = new Token(firstChar);  // the leading '
    token->lineNumber = source->lineNumber();

    // Loop to append the rest of the characters of the string,
    // up to but not including the closing quote.

    char temp, ch;
    bool check = false;
    while (!check) {
        for (ch = source->nextChar(); ch != '\'' && ch != EOF; ch = source->nextChar()) {
            token->text += ch;
        }
        temp = source->nextChar(); // read next character
        // 'It''s Friday' | 'abc''123'
        if (temp != '\'') {
            //Continue to build string
            source->putbackChar(temp, '\'');
            check = true;
        }
        else {
            token->text += temp;
        }

    }
    if (ch != EOF) {
        token->text += '\'';  // the closing quote
    }
    temp = source->nextChar(); // read next character

    token->type = token->text.length() == 3 ? TokenType::CHARACTER : TokenType::STRING;

    // Don't include the leading and trailing '.
    token->value.S = token->text.substr(1, token->text.length() - 2);

    if (temp == EOF) {
        tokenError(token, "String not closed");
    }
    return token;
}

Token *Token::SpecialSymbol(char firstChar, Source *source)
{
    Token *token = new Token(firstChar);
    token->lineNumber = source->lineNumber();

    switch (firstChar)
    {
        case ',' : token->type = TokenType::COMMA;              break;
        case ';' : token->type = TokenType::SEMICOLON;          break;
        case '+' : token->type = TokenType::PLUS;               break;
        case '-' : token->type = TokenType::MINUS;              break;
        case '*' : token->type = TokenType::STAR;               break;
        case '/' : token->type = TokenType::SLASH;              break;
        case '(' : token->type = TokenType::LPAREN;             break;
        case ')' : token->type = TokenType::RPAREN;             break;
        case '=' : token->type = TokenType::EQUALS;             break;
        case '[' : token->type = TokenType::LBRACKET;           break;
        case ']' : token->type = TokenType::RBRACKET;           break;
        case '^' : token->type = TokenType::CARAT;              break;

        case '<' :
        {
            char nextChar = source->nextChar();

            // Is it the := symbol?
            if (nextChar == '>')
            {
                token->text += nextChar;
                token->type = TokenType::NOT_EQUALS;
            }
            else if (nextChar == '=') {
                token->text += nextChar;
                token->type = TokenType::LESS_EQUALS;
            }
            else  // No, it's just the : symbol.
            {
                token->type = TokenType::LESS_THAN;
                return token;  // already consumed :
            }
            break;
        }
        case '>' :
        {
            char nextChar = source->nextChar();
            // Is it the := symbol?
            if (nextChar == '=')
            {
                token->text += nextChar;
                token->type = TokenType::GREATER_EQUALS;
            }
            else  // No, it's just the : symbol.
            {
                token->type = TokenType::GREATER_THAN;
                return token;  // already consumed :
            }
            break;
        }
        case '.' :
        {
            char nextChar = source->nextChar();

            // Is it the := symbol?
            if (nextChar == '.')
            {
                token->text += nextChar;
                token->type = TokenType::DOT_DOT;
            }
                // No, it's just the : symbol.
            else
            {
                token->type = TokenType::PERIOD;
                return token;  // already consumed :
            }

            break;
        }

        case ':' :
        {
            char nextChar = source->nextChar();

            // Is it the := symbol?
            if (nextChar == '=')
            {
                token->text += nextChar;
                token->type = TokenType::COLON_EQUALS;
            }
            // No, it's just the : symbol.
            else
            {
                token->type = TokenType::COLON;
                return token;  // already consumed :
            }

            break;
        }

        case EOF : token->type = END_OF_FILE; break;

        default: {
            token->lineNumber = source->lineNumber();
            tokenError(token, "Invalid token");
            token->type = TokenType::ERROR;
        }
    }

    source->nextChar();  // consume the special symbol
    return token;
}

void Token::tokenError(Token *token, string message)
{
    printf("TOKEN ERROR at line %d: %s at '%s'\n",
           token->lineNumber, message.c_str(), token->text.c_str());
}

}  // namespace frontend
