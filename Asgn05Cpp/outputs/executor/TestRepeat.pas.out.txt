001 PROGRAM TestRepeat;
002 
003 VAR
004     i, j : integer;
005     
006 BEGIN
007     i := 1;
008 
009     REPEAT
010         writeln('i = ', i);
011         i := i + 1;
012     UNTIL i > 5;
013     
014     writeln;
015     
016     i := 1;
017     
018     REPEAT
019         j := 10;
020         
021         REPEAT
022             writeln('i = ', i, ', j = ', j);
023             j := j + 10
024         UNTIL j > 30;
025         
026         i := i + 1
027     UNTIL i > 2;
028 END.

PASS 1 Syntax: There were no syntax errors.

PASS 2 Semantics:

===== CROSS-REFERENCE TABLE =====

*** PROGRAM TestRepeat ***

Identifier       Line numbers    Type specification
----------       ------------    ------------------
i                004 007 012 010 011 011 016 027 022 026 026
                                 Defined as: variable
                                 Scope nesting level: 1
                                 Type form = scalar, Type id = integer
j                004 019 024 022 023 023
                                 Defined as: variable
                                 Scope nesting level: 1
                                 Type form = scalar, Type id = integer

PASS 3 Execution:

i = 1
i = 2
i = 3
i = 4
i = 5

i = 1, j = 10
i = 1, j = 20
i = 1, j = 30
i = 2, j = 10
i = 2, j = 20
i = 2, j = 30

                  39 statements executed.
                   0 runtime errors.
                   1 milliseconds execution time.
