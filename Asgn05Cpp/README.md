

## To set up environment
```bash
export CLASSPATH="${PWD}/lib/antlr-4.9.1-complete.jar:$CLASSPATH"
alias antlr4="java -jar ${PWD}/lib/antlr-4.9.1-complete.jar"
alias grun='java org.antlr.v4.gui.TestRig'
```
## Create parser library from antlr4
```bash
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o generated Pascal.g4
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o generated Commander.g4
```
## Compile

### Compile the project with g++
```bash
g++ -std=c++11 -I${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/antlr4-runtime \
 -L${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/lib -I${PWD} \
 -I${PWD}/generated -lantlr4-runtime ${PWD}/generated/*.cpp \
 backend/debugger/*.cpp backend/converter/*.cpp backend/interpreter/*.cpp  \
 frontend/*.cpp intermediate/symtab/*.cpp intermediate/type/*.cpp intermediate/util/*.cpp \
 -o Assignment5 Pascal.cpp
```

### Compile the project with Cmake
```bash
cmake build .
make
```

## Run sample program
```bash
./Assignment5 -execute HelloWorld.pas
```

### To build grun UI tool and run
```bash
antlr4 Pascal.g4 -o grun-java
javac grun-java/*.java

cd grun-java;
grun Assignment5 program -gui HelloWorld.pas
```
### Run converter
```bash
./Assignment5 -convert HelloWorld.pas # This will generate HelloWorld.cpp
g++ -std=c++11 HelloWorld.cpp -o HelloWorld
./HelloWorld


./Assignment5 -convert TestWhile.pas # This will generate HelloWorld.cpp
g++ -std=c++11 TestWhile.cpp -o TestWhile
./TestWhile


./Assignment5 -convert TestIf.pas # This will generate HelloWorld.cpp
g++ -std=c++11 TestIf.cpp -o TestIf
./TestIf./

```



### Generate outputs
```bash
mkdir -p outputs/cpp  outputs/runtime_cpp outputs/build outputs/executor
pushd .; cd outputs/cpp; ls -ltr ../../*.pas  | awk -F" " {'print $9'} | xargs -n 1 -I {} ../../Assignment5 -convert {}; popd
g++ -std=c++11 outputs/cpp/Hello.cpp -o outputs/build/Hello
g++ -std=c++11 outputs/cpp/HelloWorld.cpp -o outputs/build/HelloWorld
g++ -std=c++11 outputs/cpp/Newton3.cpp -o outputs/build/Newton3
g++ -std=c++11 outputs/cpp/TestCase.cpp -o outputs/build/TestCase
g++ -std=c++11 outputs/cpp/TestFor.cpp -o outputs/build/TestFor
g++ -std=c++11 outputs/cpp/TestIf.cpp -o outputs/build/TestIf
g++ -std=c++11 outputs/cpp/TestProcedure.cpp -o outputs/build/TestProcedure
g++ -std=c++11 outputs/cpp/TestRepeat.cpp -o outputs/build/TestRepeat
g++ -std=c++11 outputs/cpp/TestWhile.cpp -o outputs/build/TestWhile

./outputs/build/Hello > outputs/runtime_cpp/Hello.out.txt
./outputs/build/HelloWorld > outputs/runtime_cpp/HelloWorld.out.txt
./outputs/build/Newton3 > outputs/runtime_cpp/Newton3.out.txt
./outputs/build/TestCase > outputs/runtime_cpp/TestCase.out.txt
./outputs/build/TestFor > outputs/runtime_cpp/TestFor.out.txt
./outputs/build/TestIf > outputs/runtime_cpp/TestIf.out.txt
./outputs/build/TestProcedure > outputs/runtime_cpp/TestProcedure.out.txt
./outputs/build/TestRepeat > outputs/runtime_cpp/TestRepeat.out.txt
./outputs/build/TestWhile > outputs/runtime_cpp/TestWhile.out.txt
ls -ltr *.pas  | awk -F" " {'print $9'} | xargs -I {} bash -c " ./Assignment5 -execute {} >   outputs/executor/{}.out.txt"
```


## Testing purpose, ignore this
```bash
#(https://github.com/bkiers/rrd-antlr4)
java -jar PATH/TO/rrd-antlr4-0.1.2.jar /Volumes/Data/CMPE152/git/assignments/Asgn04Cpp/Pcl4.g4 
```