
    #include <map>
    #include "intermediate/symtab/Symtab.h"
    #include "intermediate/type/Typespec.h"
    using namespace intermediate::symtab;
    using namespace intermediate::type;


// Generated from Cmm.g4 by ANTLR 4.9.1


#include "CmmVisitor.h"

#include "CmmParser.h"


using namespace antlrcpp;
using namespace antlr4;

CmmParser::CmmParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

CmmParser::~CmmParser() {
  delete _interpreter;
}

std::string CmmParser::getGrammarFileName() const {
  return "Cmm.g4";
}

const std::vector<std::string>& CmmParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& CmmParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

CmmParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ProgramHeaderContext* CmmParser::ProgramContext::programHeader() {
  return getRuleContext<CmmParser::ProgramHeaderContext>(0);
}

CmmParser::DeclarationsContext* CmmParser::ProgramContext::declarations() {
  return getRuleContext<CmmParser::DeclarationsContext>(0);
}


size_t CmmParser::ProgramContext::getRuleIndex() const {
  return CmmParser::RuleProgram;
}


antlrcpp::Any CmmParser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProgramContext* CmmParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, CmmParser::RuleProgram);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(184);
    programHeader();
    setState(185);
    declarations();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramHeaderContext ------------------------------------------------------------------

CmmParser::ProgramHeaderContext::ProgramHeaderContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ProgramHeaderContext::PROJECT() {
  return getToken(CmmParser::PROJECT, 0);
}

CmmParser::ProgramIdentifierContext* CmmParser::ProgramHeaderContext::programIdentifier() {
  return getRuleContext<CmmParser::ProgramIdentifierContext>(0);
}

CmmParser::ProgramParametersContext* CmmParser::ProgramHeaderContext::programParameters() {
  return getRuleContext<CmmParser::ProgramParametersContext>(0);
}


size_t CmmParser::ProgramHeaderContext::getRuleIndex() const {
  return CmmParser::RuleProgramHeader;
}


antlrcpp::Any CmmParser::ProgramHeaderContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProgramHeader(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProgramHeaderContext* CmmParser::programHeader() {
  ProgramHeaderContext *_localctx = _tracker.createInstance<ProgramHeaderContext>(_ctx, getState());
  enterRule(_localctx, 2, CmmParser::RuleProgramHeader);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(187);
    match(CmmParser::PROJECT);
    setState(188);
    programIdentifier();
    setState(190);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__1) {
      setState(189);
      programParameters();
    }
    setState(192);
    match(CmmParser::T__0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramParametersContext ------------------------------------------------------------------

CmmParser::ProgramParametersContext::ProgramParametersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> CmmParser::ProgramParametersContext::IDENTIFIER() {
  return getTokens(CmmParser::IDENTIFIER);
}

tree::TerminalNode* CmmParser::ProgramParametersContext::IDENTIFIER(size_t i) {
  return getToken(CmmParser::IDENTIFIER, i);
}


size_t CmmParser::ProgramParametersContext::getRuleIndex() const {
  return CmmParser::RuleProgramParameters;
}


antlrcpp::Any CmmParser::ProgramParametersContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProgramParameters(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProgramParametersContext* CmmParser::programParameters() {
  ProgramParametersContext *_localctx = _tracker.createInstance<ProgramParametersContext>(_ctx, getState());
  enterRule(_localctx, 4, CmmParser::RuleProgramParameters);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(194);
    match(CmmParser::T__1);
    setState(195);
    match(CmmParser::IDENTIFIER);
    setState(200);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(196);
      match(CmmParser::T__2);
      setState(197);
      match(CmmParser::IDENTIFIER);
      setState(202);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(203);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramIdentifierContext ------------------------------------------------------------------

CmmParser::ProgramIdentifierContext::ProgramIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ProgramIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::ProgramIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleProgramIdentifier;
}


antlrcpp::Any CmmParser::ProgramIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProgramIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProgramIdentifierContext* CmmParser::programIdentifier() {
  ProgramIdentifierContext *_localctx = _tracker.createInstance<ProgramIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 6, CmmParser::RuleProgramIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(205);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

CmmParser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::DeclarationsContext* CmmParser::BlockContext::declarations() {
  return getRuleContext<CmmParser::DeclarationsContext>(0);
}

CmmParser::StatementListContext* CmmParser::BlockContext::statementList() {
  return getRuleContext<CmmParser::StatementListContext>(0);
}


size_t CmmParser::BlockContext::getRuleIndex() const {
  return CmmParser::RuleBlock;
}


antlrcpp::Any CmmParser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::BlockContext* CmmParser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 8, CmmParser::RuleBlock);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(207);
    declarations();
    setState(208);
    statementList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclarationsContext ------------------------------------------------------------------

CmmParser::DeclarationsContext::DeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ConstantsPartContext* CmmParser::DeclarationsContext::constantsPart() {
  return getRuleContext<CmmParser::ConstantsPartContext>(0);
}

CmmParser::TypesPartContext* CmmParser::DeclarationsContext::typesPart() {
  return getRuleContext<CmmParser::TypesPartContext>(0);
}

CmmParser::VariablesPartContext* CmmParser::DeclarationsContext::variablesPart() {
  return getRuleContext<CmmParser::VariablesPartContext>(0);
}

CmmParser::RoutinesPartContext* CmmParser::DeclarationsContext::routinesPart() {
  return getRuleContext<CmmParser::RoutinesPartContext>(0);
}


size_t CmmParser::DeclarationsContext::getRuleIndex() const {
  return CmmParser::RuleDeclarations;
}


antlrcpp::Any CmmParser::DeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitDeclarations(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::DeclarationsContext* CmmParser::declarations() {
  DeclarationsContext *_localctx = _tracker.createInstance<DeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 10, CmmParser::RuleDeclarations);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(213);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::CONST) {
      setState(210);
      constantsPart();
      setState(211);
      match(CmmParser::T__0);
    }
    setState(218);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::TYPEDEF) {
      setState(215);
      typesPart();
      setState(216);
      match(CmmParser::T__0);
    }
    setState(223);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx)) {
    case 1: {
      setState(220);
      variablesPart();
      setState(221);
      match(CmmParser::T__0);
      break;
    }

    default:
      break;
    }
    setState(226);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 5, _ctx)) {
    case 1: {
      setState(225);
      routinesPart();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantsPartContext ------------------------------------------------------------------

CmmParser::ConstantsPartContext::ConstantsPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ConstantsPartContext::CONST() {
  return getToken(CmmParser::CONST, 0);
}

CmmParser::ConstantDefinitionsListContext* CmmParser::ConstantsPartContext::constantDefinitionsList() {
  return getRuleContext<CmmParser::ConstantDefinitionsListContext>(0);
}


size_t CmmParser::ConstantsPartContext::getRuleIndex() const {
  return CmmParser::RuleConstantsPart;
}


antlrcpp::Any CmmParser::ConstantsPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitConstantsPart(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ConstantsPartContext* CmmParser::constantsPart() {
  ConstantsPartContext *_localctx = _tracker.createInstance<ConstantsPartContext>(_ctx, getState());
  enterRule(_localctx, 12, CmmParser::RuleConstantsPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(228);
    match(CmmParser::CONST);
    setState(229);
    constantDefinitionsList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantDefinitionsListContext ------------------------------------------------------------------

CmmParser::ConstantDefinitionsListContext::ConstantDefinitionsListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::ConstantDefinitionContext *> CmmParser::ConstantDefinitionsListContext::constantDefinition() {
  return getRuleContexts<CmmParser::ConstantDefinitionContext>();
}

CmmParser::ConstantDefinitionContext* CmmParser::ConstantDefinitionsListContext::constantDefinition(size_t i) {
  return getRuleContext<CmmParser::ConstantDefinitionContext>(i);
}


size_t CmmParser::ConstantDefinitionsListContext::getRuleIndex() const {
  return CmmParser::RuleConstantDefinitionsList;
}


antlrcpp::Any CmmParser::ConstantDefinitionsListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitConstantDefinitionsList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ConstantDefinitionsListContext* CmmParser::constantDefinitionsList() {
  ConstantDefinitionsListContext *_localctx = _tracker.createInstance<ConstantDefinitionsListContext>(_ctx, getState());
  enterRule(_localctx, 14, CmmParser::RuleConstantDefinitionsList);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(231);
    constantDefinition();
    setState(236);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(232);
        match(CmmParser::T__0);
        setState(233);
        constantDefinition(); 
      }
      setState(238);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantDefinitionContext ------------------------------------------------------------------

CmmParser::ConstantDefinitionContext::ConstantDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ConstantIdentifierContext* CmmParser::ConstantDefinitionContext::constantIdentifier() {
  return getRuleContext<CmmParser::ConstantIdentifierContext>(0);
}

CmmParser::ConstantContext* CmmParser::ConstantDefinitionContext::constant() {
  return getRuleContext<CmmParser::ConstantContext>(0);
}


size_t CmmParser::ConstantDefinitionContext::getRuleIndex() const {
  return CmmParser::RuleConstantDefinition;
}


antlrcpp::Any CmmParser::ConstantDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitConstantDefinition(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ConstantDefinitionContext* CmmParser::constantDefinition() {
  ConstantDefinitionContext *_localctx = _tracker.createInstance<ConstantDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 16, CmmParser::RuleConstantDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(239);
    constantIdentifier();
    setState(240);
    match(CmmParser::T__4);
    setState(241);
    constant();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantIdentifierContext ------------------------------------------------------------------

CmmParser::ConstantIdentifierContext::ConstantIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ConstantIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::ConstantIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleConstantIdentifier;
}


antlrcpp::Any CmmParser::ConstantIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitConstantIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ConstantIdentifierContext* CmmParser::constantIdentifier() {
  ConstantIdentifierContext *_localctx = _tracker.createInstance<ConstantIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 18, CmmParser::RuleConstantIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(243);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ConstantContext ------------------------------------------------------------------

CmmParser::ConstantContext::ConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ConstantContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}

CmmParser::UnsignedNumberContext* CmmParser::ConstantContext::unsignedNumber() {
  return getRuleContext<CmmParser::UnsignedNumberContext>(0);
}

CmmParser::SignContext* CmmParser::ConstantContext::sign() {
  return getRuleContext<CmmParser::SignContext>(0);
}

CmmParser::CharacterConstantContext* CmmParser::ConstantContext::characterConstant() {
  return getRuleContext<CmmParser::CharacterConstantContext>(0);
}

CmmParser::StringConstantContext* CmmParser::ConstantContext::stringConstant() {
  return getRuleContext<CmmParser::StringConstantContext>(0);
}


size_t CmmParser::ConstantContext::getRuleIndex() const {
  return CmmParser::RuleConstant;
}


antlrcpp::Any CmmParser::ConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ConstantContext* CmmParser::constant() {
  ConstantContext *_localctx = _tracker.createInstance<ConstantContext>(_ctx, getState());
  enterRule(_localctx, 20, CmmParser::RuleConstant);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(254);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::T__5:
      case CmmParser::T__6:
      case CmmParser::IDENTIFIER:
      case CmmParser::INTEGER:
      case CmmParser::REAL: {
        enterOuterAlt(_localctx, 1);
        setState(246);
        _errHandler->sync(this);

        _la = _input->LA(1);
        if (_la == CmmParser::T__5

        || _la == CmmParser::T__6) {
          setState(245);
          sign();
        }
        setState(250);
        _errHandler->sync(this);
        switch (_input->LA(1)) {
          case CmmParser::IDENTIFIER: {
            setState(248);
            match(CmmParser::IDENTIFIER);
            break;
          }

          case CmmParser::INTEGER:
          case CmmParser::REAL: {
            setState(249);
            unsignedNumber();
            break;
          }

        default:
          throw NoViableAltException(this);
        }
        break;
      }

      case CmmParser::CHARACTER: {
        enterOuterAlt(_localctx, 2);
        setState(252);
        characterConstant();
        break;
      }

      case CmmParser::STRING: {
        enterOuterAlt(_localctx, 3);
        setState(253);
        stringConstant();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SignContext ------------------------------------------------------------------

CmmParser::SignContext::SignContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::SignContext::getRuleIndex() const {
  return CmmParser::RuleSign;
}


antlrcpp::Any CmmParser::SignContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitSign(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::SignContext* CmmParser::sign() {
  SignContext *_localctx = _tracker.createInstance<SignContext>(_ctx, getState());
  enterRule(_localctx, 22, CmmParser::RuleSign);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(256);
    _la = _input->LA(1);
    if (!(_la == CmmParser::T__5

    || _la == CmmParser::T__6)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypesPartContext ------------------------------------------------------------------

CmmParser::TypesPartContext::TypesPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::TypeDefinitionsListContext* CmmParser::TypesPartContext::typeDefinitionsList() {
  return getRuleContext<CmmParser::TypeDefinitionsListContext>(0);
}


size_t CmmParser::TypesPartContext::getRuleIndex() const {
  return CmmParser::RuleTypesPart;
}


antlrcpp::Any CmmParser::TypesPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTypesPart(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TypesPartContext* CmmParser::typesPart() {
  TypesPartContext *_localctx = _tracker.createInstance<TypesPartContext>(_ctx, getState());
  enterRule(_localctx, 24, CmmParser::RuleTypesPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(258);
    typeDefinitionsList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeDefinitionsListContext ------------------------------------------------------------------

CmmParser::TypeDefinitionsListContext::TypeDefinitionsListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::TypeDefinitionContext *> CmmParser::TypeDefinitionsListContext::typeDefinition() {
  return getRuleContexts<CmmParser::TypeDefinitionContext>();
}

CmmParser::TypeDefinitionContext* CmmParser::TypeDefinitionsListContext::typeDefinition(size_t i) {
  return getRuleContext<CmmParser::TypeDefinitionContext>(i);
}


size_t CmmParser::TypeDefinitionsListContext::getRuleIndex() const {
  return CmmParser::RuleTypeDefinitionsList;
}


antlrcpp::Any CmmParser::TypeDefinitionsListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTypeDefinitionsList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TypeDefinitionsListContext* CmmParser::typeDefinitionsList() {
  TypeDefinitionsListContext *_localctx = _tracker.createInstance<TypeDefinitionsListContext>(_ctx, getState());
  enterRule(_localctx, 26, CmmParser::RuleTypeDefinitionsList);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(260);
    typeDefinition();
    setState(265);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(261);
        match(CmmParser::T__0);
        setState(262);
        typeDefinition(); 
      }
      setState(267);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 10, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeDefinitionContext ------------------------------------------------------------------

CmmParser::TypeDefinitionContext::TypeDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::TypeDefinitionContext::TYPEDEF() {
  return getToken(CmmParser::TYPEDEF, 0);
}

CmmParser::TypeIdentifierContext* CmmParser::TypeDefinitionContext::typeIdentifier() {
  return getRuleContext<CmmParser::TypeIdentifierContext>(0);
}

CmmParser::TypeSpecificationContext* CmmParser::TypeDefinitionContext::typeSpecification() {
  return getRuleContext<CmmParser::TypeSpecificationContext>(0);
}


size_t CmmParser::TypeDefinitionContext::getRuleIndex() const {
  return CmmParser::RuleTypeDefinition;
}


antlrcpp::Any CmmParser::TypeDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTypeDefinition(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TypeDefinitionContext* CmmParser::typeDefinition() {
  TypeDefinitionContext *_localctx = _tracker.createInstance<TypeDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 28, CmmParser::RuleTypeDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(268);
    match(CmmParser::TYPEDEF);
    setState(269);
    typeIdentifier();
    setState(270);
    match(CmmParser::T__4);
    setState(271);
    typeSpecification();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeIdentifierContext ------------------------------------------------------------------

CmmParser::TypeIdentifierContext::TypeIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::TypeIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::TypeIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleTypeIdentifier;
}


antlrcpp::Any CmmParser::TypeIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTypeIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TypeIdentifierContext* CmmParser::typeIdentifier() {
  TypeIdentifierContext *_localctx = _tracker.createInstance<TypeIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 30, CmmParser::RuleTypeIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(273);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TypeSpecificationContext ------------------------------------------------------------------

CmmParser::TypeSpecificationContext::TypeSpecificationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::TypeSpecificationContext::getRuleIndex() const {
  return CmmParser::RuleTypeSpecification;
}

void CmmParser::TypeSpecificationContext::copyFrom(TypeSpecificationContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
  this->type = ctx->type;
}

//----------------- SimpleTypespecContext ------------------------------------------------------------------

CmmParser::SimpleTypeContext* CmmParser::SimpleTypespecContext::simpleType() {
  return getRuleContext<CmmParser::SimpleTypeContext>(0);
}

CmmParser::SimpleTypespecContext::SimpleTypespecContext(TypeSpecificationContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::SimpleTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitSimpleTypespec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ArrayTypespecContext ------------------------------------------------------------------

CmmParser::ArrayTypeContext* CmmParser::ArrayTypespecContext::arrayType() {
  return getRuleContext<CmmParser::ArrayTypeContext>(0);
}

CmmParser::ArrayTypespecContext::ArrayTypespecContext(TypeSpecificationContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::ArrayTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitArrayTypespec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- RecordTypespecContext ------------------------------------------------------------------

CmmParser::RecordTypeContext* CmmParser::RecordTypespecContext::recordType() {
  return getRuleContext<CmmParser::RecordTypeContext>(0);
}

CmmParser::RecordTypespecContext::RecordTypespecContext(TypeSpecificationContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::RecordTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRecordTypespec(this);
  else
    return visitor->visitChildren(this);
}
CmmParser::TypeSpecificationContext* CmmParser::typeSpecification() {
  TypeSpecificationContext *_localctx = _tracker.createInstance<TypeSpecificationContext>(_ctx, getState());
  enterRule(_localctx, 32, CmmParser::RuleTypeSpecification);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(278);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::T__1:
      case CmmParser::T__5:
      case CmmParser::T__6:
      case CmmParser::IDENTIFIER:
      case CmmParser::INTEGER:
      case CmmParser::REAL:
      case CmmParser::CHARACTER:
      case CmmParser::STRING: {
        _localctx = dynamic_cast<TypeSpecificationContext *>(_tracker.createInstance<CmmParser::SimpleTypespecContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(275);
        simpleType();
        break;
      }

      case CmmParser::ARRAY: {
        _localctx = dynamic_cast<TypeSpecificationContext *>(_tracker.createInstance<CmmParser::ArrayTypespecContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(276);
        arrayType();
        break;
      }

      case CmmParser::RECORD: {
        _localctx = dynamic_cast<TypeSpecificationContext *>(_tracker.createInstance<CmmParser::RecordTypespecContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(277);
        recordType();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SimpleTypeContext ------------------------------------------------------------------

CmmParser::SimpleTypeContext::SimpleTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::SimpleTypeContext::getRuleIndex() const {
  return CmmParser::RuleSimpleType;
}

void CmmParser::SimpleTypeContext::copyFrom(SimpleTypeContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
  this->type = ctx->type;
}

//----------------- SubrangeTypespecContext ------------------------------------------------------------------

CmmParser::SubrangeTypeContext* CmmParser::SubrangeTypespecContext::subrangeType() {
  return getRuleContext<CmmParser::SubrangeTypeContext>(0);
}

CmmParser::SubrangeTypespecContext::SubrangeTypespecContext(SimpleTypeContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::SubrangeTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitSubrangeTypespec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- EnumerationTypespecContext ------------------------------------------------------------------

CmmParser::EnumerationTypeContext* CmmParser::EnumerationTypespecContext::enumerationType() {
  return getRuleContext<CmmParser::EnumerationTypeContext>(0);
}

CmmParser::EnumerationTypespecContext::EnumerationTypespecContext(SimpleTypeContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::EnumerationTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitEnumerationTypespec(this);
  else
    return visitor->visitChildren(this);
}
//----------------- TypeIdentifierTypespecContext ------------------------------------------------------------------

CmmParser::TypeIdentifierContext* CmmParser::TypeIdentifierTypespecContext::typeIdentifier() {
  return getRuleContext<CmmParser::TypeIdentifierContext>(0);
}

CmmParser::TypeIdentifierTypespecContext::TypeIdentifierTypespecContext(SimpleTypeContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::TypeIdentifierTypespecContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTypeIdentifierTypespec(this);
  else
    return visitor->visitChildren(this);
}
CmmParser::SimpleTypeContext* CmmParser::simpleType() {
  SimpleTypeContext *_localctx = _tracker.createInstance<SimpleTypeContext>(_ctx, getState());
  enterRule(_localctx, 34, CmmParser::RuleSimpleType);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(283);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 12, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<SimpleTypeContext *>(_tracker.createInstance<CmmParser::TypeIdentifierTypespecContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(280);
      typeIdentifier();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<SimpleTypeContext *>(_tracker.createInstance<CmmParser::EnumerationTypespecContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(281);
      enumerationType();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<SimpleTypeContext *>(_tracker.createInstance<CmmParser::SubrangeTypespecContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(282);
      subrangeType();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EnumerationTypeContext ------------------------------------------------------------------

CmmParser::EnumerationTypeContext::EnumerationTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::EnumerationConstantContext *> CmmParser::EnumerationTypeContext::enumerationConstant() {
  return getRuleContexts<CmmParser::EnumerationConstantContext>();
}

CmmParser::EnumerationConstantContext* CmmParser::EnumerationTypeContext::enumerationConstant(size_t i) {
  return getRuleContext<CmmParser::EnumerationConstantContext>(i);
}


size_t CmmParser::EnumerationTypeContext::getRuleIndex() const {
  return CmmParser::RuleEnumerationType;
}


antlrcpp::Any CmmParser::EnumerationTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitEnumerationType(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::EnumerationTypeContext* CmmParser::enumerationType() {
  EnumerationTypeContext *_localctx = _tracker.createInstance<EnumerationTypeContext>(_ctx, getState());
  enterRule(_localctx, 36, CmmParser::RuleEnumerationType);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(285);
    match(CmmParser::T__1);
    setState(286);
    enumerationConstant();
    setState(291);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(287);
      match(CmmParser::T__2);
      setState(288);
      enumerationConstant();
      setState(293);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(294);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EnumerationConstantContext ------------------------------------------------------------------

CmmParser::EnumerationConstantContext::EnumerationConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ConstantIdentifierContext* CmmParser::EnumerationConstantContext::constantIdentifier() {
  return getRuleContext<CmmParser::ConstantIdentifierContext>(0);
}


size_t CmmParser::EnumerationConstantContext::getRuleIndex() const {
  return CmmParser::RuleEnumerationConstant;
}


antlrcpp::Any CmmParser::EnumerationConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitEnumerationConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::EnumerationConstantContext* CmmParser::enumerationConstant() {
  EnumerationConstantContext *_localctx = _tracker.createInstance<EnumerationConstantContext>(_ctx, getState());
  enterRule(_localctx, 38, CmmParser::RuleEnumerationConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(296);
    constantIdentifier();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SubrangeTypeContext ------------------------------------------------------------------

CmmParser::SubrangeTypeContext::SubrangeTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::ConstantContext *> CmmParser::SubrangeTypeContext::constant() {
  return getRuleContexts<CmmParser::ConstantContext>();
}

CmmParser::ConstantContext* CmmParser::SubrangeTypeContext::constant(size_t i) {
  return getRuleContext<CmmParser::ConstantContext>(i);
}


size_t CmmParser::SubrangeTypeContext::getRuleIndex() const {
  return CmmParser::RuleSubrangeType;
}


antlrcpp::Any CmmParser::SubrangeTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitSubrangeType(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::SubrangeTypeContext* CmmParser::subrangeType() {
  SubrangeTypeContext *_localctx = _tracker.createInstance<SubrangeTypeContext>(_ctx, getState());
  enterRule(_localctx, 40, CmmParser::RuleSubrangeType);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(298);
    constant();
    setState(299);
    match(CmmParser::T__7);
    setState(300);
    constant();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayTypeContext ------------------------------------------------------------------

CmmParser::ArrayTypeContext::ArrayTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ArrayTypeContext::ARRAY() {
  return getToken(CmmParser::ARRAY, 0);
}

CmmParser::ArrayDimensionListContext* CmmParser::ArrayTypeContext::arrayDimensionList() {
  return getRuleContext<CmmParser::ArrayDimensionListContext>(0);
}

tree::TerminalNode* CmmParser::ArrayTypeContext::OF() {
  return getToken(CmmParser::OF, 0);
}

CmmParser::TypeSpecificationContext* CmmParser::ArrayTypeContext::typeSpecification() {
  return getRuleContext<CmmParser::TypeSpecificationContext>(0);
}


size_t CmmParser::ArrayTypeContext::getRuleIndex() const {
  return CmmParser::RuleArrayType;
}


antlrcpp::Any CmmParser::ArrayTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitArrayType(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ArrayTypeContext* CmmParser::arrayType() {
  ArrayTypeContext *_localctx = _tracker.createInstance<ArrayTypeContext>(_ctx, getState());
  enterRule(_localctx, 42, CmmParser::RuleArrayType);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(302);
    match(CmmParser::ARRAY);
    setState(303);
    match(CmmParser::T__8);
    setState(304);
    arrayDimensionList();
    setState(305);
    match(CmmParser::T__9);
    setState(306);
    match(CmmParser::OF);
    setState(307);
    typeSpecification();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayDimensionListContext ------------------------------------------------------------------

CmmParser::ArrayDimensionListContext::ArrayDimensionListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::SimpleTypeContext *> CmmParser::ArrayDimensionListContext::simpleType() {
  return getRuleContexts<CmmParser::SimpleTypeContext>();
}

CmmParser::SimpleTypeContext* CmmParser::ArrayDimensionListContext::simpleType(size_t i) {
  return getRuleContext<CmmParser::SimpleTypeContext>(i);
}


size_t CmmParser::ArrayDimensionListContext::getRuleIndex() const {
  return CmmParser::RuleArrayDimensionList;
}


antlrcpp::Any CmmParser::ArrayDimensionListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitArrayDimensionList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ArrayDimensionListContext* CmmParser::arrayDimensionList() {
  ArrayDimensionListContext *_localctx = _tracker.createInstance<ArrayDimensionListContext>(_ctx, getState());
  enterRule(_localctx, 44, CmmParser::RuleArrayDimensionList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(309);
    simpleType();
    setState(314);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(310);
      match(CmmParser::T__2);
      setState(311);
      simpleType();
      setState(316);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RecordTypeContext ------------------------------------------------------------------

CmmParser::RecordTypeContext::RecordTypeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::RecordTypeContext::RECORD() {
  return getToken(CmmParser::RECORD, 0);
}

CmmParser::RecordFieldsContext* CmmParser::RecordTypeContext::recordFields() {
  return getRuleContext<CmmParser::RecordFieldsContext>(0);
}

tree::TerminalNode* CmmParser::RecordTypeContext::END() {
  return getToken(CmmParser::END, 0);
}


size_t CmmParser::RecordTypeContext::getRuleIndex() const {
  return CmmParser::RuleRecordType;
}


antlrcpp::Any CmmParser::RecordTypeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRecordType(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RecordTypeContext* CmmParser::recordType() {
  RecordTypeContext *_localctx = _tracker.createInstance<RecordTypeContext>(_ctx, getState());
  enterRule(_localctx, 46, CmmParser::RuleRecordType);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(317);
    match(CmmParser::RECORD);
    setState(318);
    recordFields();
    setState(320);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__0) {
      setState(319);
      match(CmmParser::T__0);
    }
    setState(322);
    match(CmmParser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RecordFieldsContext ------------------------------------------------------------------

CmmParser::RecordFieldsContext::RecordFieldsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::VariableDeclarationsListContext* CmmParser::RecordFieldsContext::variableDeclarationsList() {
  return getRuleContext<CmmParser::VariableDeclarationsListContext>(0);
}


size_t CmmParser::RecordFieldsContext::getRuleIndex() const {
  return CmmParser::RuleRecordFields;
}


antlrcpp::Any CmmParser::RecordFieldsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRecordFields(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RecordFieldsContext* CmmParser::recordFields() {
  RecordFieldsContext *_localctx = _tracker.createInstance<RecordFieldsContext>(_ctx, getState());
  enterRule(_localctx, 48, CmmParser::RuleRecordFields);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(324);
    variableDeclarationsList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariablesPartContext ------------------------------------------------------------------

CmmParser::VariablesPartContext::VariablesPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::VariableDeclarationsListContext* CmmParser::VariablesPartContext::variableDeclarationsList() {
  return getRuleContext<CmmParser::VariableDeclarationsListContext>(0);
}


size_t CmmParser::VariablesPartContext::getRuleIndex() const {
  return CmmParser::RuleVariablesPart;
}


antlrcpp::Any CmmParser::VariablesPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariablesPart(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariablesPartContext* CmmParser::variablesPart() {
  VariablesPartContext *_localctx = _tracker.createInstance<VariablesPartContext>(_ctx, getState());
  enterRule(_localctx, 50, CmmParser::RuleVariablesPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(326);
    variableDeclarationsList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationsListContext ------------------------------------------------------------------

CmmParser::VariableDeclarationsListContext::VariableDeclarationsListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::VariableDeclarationsContext *> CmmParser::VariableDeclarationsListContext::variableDeclarations() {
  return getRuleContexts<CmmParser::VariableDeclarationsContext>();
}

CmmParser::VariableDeclarationsContext* CmmParser::VariableDeclarationsListContext::variableDeclarations(size_t i) {
  return getRuleContext<CmmParser::VariableDeclarationsContext>(i);
}


size_t CmmParser::VariableDeclarationsListContext::getRuleIndex() const {
  return CmmParser::RuleVariableDeclarationsList;
}


antlrcpp::Any CmmParser::VariableDeclarationsListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariableDeclarationsList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariableDeclarationsListContext* CmmParser::variableDeclarationsList() {
  VariableDeclarationsListContext *_localctx = _tracker.createInstance<VariableDeclarationsListContext>(_ctx, getState());
  enterRule(_localctx, 52, CmmParser::RuleVariableDeclarationsList);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(328);
    variableDeclarations();
    setState(333);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(329);
        match(CmmParser::T__0);
        setState(330);
        variableDeclarations(); 
      }
      setState(335);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationsContext ------------------------------------------------------------------

CmmParser::VariableDeclarationsContext::VariableDeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::TypeSpecificationContext* CmmParser::VariableDeclarationsContext::typeSpecification() {
  return getRuleContext<CmmParser::TypeSpecificationContext>(0);
}

CmmParser::VariableIdentifierListContext* CmmParser::VariableDeclarationsContext::variableIdentifierList() {
  return getRuleContext<CmmParser::VariableIdentifierListContext>(0);
}


size_t CmmParser::VariableDeclarationsContext::getRuleIndex() const {
  return CmmParser::RuleVariableDeclarations;
}


antlrcpp::Any CmmParser::VariableDeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariableDeclarations(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariableDeclarationsContext* CmmParser::variableDeclarations() {
  VariableDeclarationsContext *_localctx = _tracker.createInstance<VariableDeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 54, CmmParser::RuleVariableDeclarations);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(336);
    typeSpecification();
    setState(337);
    variableIdentifierList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableIdentifierListContext ------------------------------------------------------------------

CmmParser::VariableIdentifierListContext::VariableIdentifierListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::VariableIdentifierContext *> CmmParser::VariableIdentifierListContext::variableIdentifier() {
  return getRuleContexts<CmmParser::VariableIdentifierContext>();
}

CmmParser::VariableIdentifierContext* CmmParser::VariableIdentifierListContext::variableIdentifier(size_t i) {
  return getRuleContext<CmmParser::VariableIdentifierContext>(i);
}


size_t CmmParser::VariableIdentifierListContext::getRuleIndex() const {
  return CmmParser::RuleVariableIdentifierList;
}


antlrcpp::Any CmmParser::VariableIdentifierListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariableIdentifierList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariableIdentifierListContext* CmmParser::variableIdentifierList() {
  VariableIdentifierListContext *_localctx = _tracker.createInstance<VariableIdentifierListContext>(_ctx, getState());
  enterRule(_localctx, 56, CmmParser::RuleVariableIdentifierList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(339);
    variableIdentifier();
    setState(344);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(340);
      match(CmmParser::T__2);
      setState(341);
      variableIdentifier();
      setState(346);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableIdentifierContext ------------------------------------------------------------------

CmmParser::VariableIdentifierContext::VariableIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::VariableIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::VariableIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleVariableIdentifier;
}


antlrcpp::Any CmmParser::VariableIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariableIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariableIdentifierContext* CmmParser::variableIdentifier() {
  VariableIdentifierContext *_localctx = _tracker.createInstance<VariableIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 58, CmmParser::RuleVariableIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(347);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RoutinesPartContext ------------------------------------------------------------------

CmmParser::RoutinesPartContext::RoutinesPartContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::RoutineDefinitionContext *> CmmParser::RoutinesPartContext::routineDefinition() {
  return getRuleContexts<CmmParser::RoutineDefinitionContext>();
}

CmmParser::RoutineDefinitionContext* CmmParser::RoutinesPartContext::routineDefinition(size_t i) {
  return getRuleContext<CmmParser::RoutineDefinitionContext>(i);
}


size_t CmmParser::RoutinesPartContext::getRuleIndex() const {
  return CmmParser::RuleRoutinesPart;
}


antlrcpp::Any CmmParser::RoutinesPartContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRoutinesPart(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RoutinesPartContext* CmmParser::routinesPart() {
  RoutinesPartContext *_localctx = _tracker.createInstance<RoutinesPartContext>(_ctx, getState());
  enterRule(_localctx, 60, CmmParser::RuleRoutinesPart);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(349);
    routineDefinition();
    setState(353);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 18, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(350);
        routineDefinition(); 
      }
      setState(355);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 18, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RoutineDefinitionContext ------------------------------------------------------------------

CmmParser::RoutineDefinitionContext::RoutineDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::RoutineDefinitionContext::BEGIN() {
  return getToken(CmmParser::BEGIN, 0);
}

CmmParser::BlockContext* CmmParser::RoutineDefinitionContext::block() {
  return getRuleContext<CmmParser::BlockContext>(0);
}

tree::TerminalNode* CmmParser::RoutineDefinitionContext::END() {
  return getToken(CmmParser::END, 0);
}

CmmParser::ProcedureHeadContext* CmmParser::RoutineDefinitionContext::procedureHead() {
  return getRuleContext<CmmParser::ProcedureHeadContext>(0);
}

CmmParser::FunctionHeadContext* CmmParser::RoutineDefinitionContext::functionHead() {
  return getRuleContext<CmmParser::FunctionHeadContext>(0);
}


size_t CmmParser::RoutineDefinitionContext::getRuleIndex() const {
  return CmmParser::RuleRoutineDefinition;
}


antlrcpp::Any CmmParser::RoutineDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRoutineDefinition(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RoutineDefinitionContext* CmmParser::routineDefinition() {
  RoutineDefinitionContext *_localctx = _tracker.createInstance<RoutineDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 62, CmmParser::RuleRoutineDefinition);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(358);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::VOID: {
        setState(356);
        procedureHead();
        break;
      }

      case CmmParser::IDENTIFIER: {
        setState(357);
        functionHead();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    setState(360);
    match(CmmParser::BEGIN);
    setState(361);
    block();
    setState(362);
    match(CmmParser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureHeadContext ------------------------------------------------------------------

CmmParser::ProcedureHeadContext::ProcedureHeadContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ProcedureHeadContext::VOID() {
  return getToken(CmmParser::VOID, 0);
}

CmmParser::RoutineIdentifierContext* CmmParser::ProcedureHeadContext::routineIdentifier() {
  return getRuleContext<CmmParser::RoutineIdentifierContext>(0);
}

CmmParser::ParametersContext* CmmParser::ProcedureHeadContext::parameters() {
  return getRuleContext<CmmParser::ParametersContext>(0);
}


size_t CmmParser::ProcedureHeadContext::getRuleIndex() const {
  return CmmParser::RuleProcedureHead;
}


antlrcpp::Any CmmParser::ProcedureHeadContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProcedureHead(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProcedureHeadContext* CmmParser::procedureHead() {
  ProcedureHeadContext *_localctx = _tracker.createInstance<ProcedureHeadContext>(_ctx, getState());
  enterRule(_localctx, 64, CmmParser::RuleProcedureHead);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(364);
    match(CmmParser::VOID);
    setState(365);
    routineIdentifier();
    setState(366);
    match(CmmParser::T__1);
    setState(368);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::VAR

    || _la == CmmParser::IDENTIFIER) {
      setState(367);
      parameters();
    }
    setState(370);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionHeadContext ------------------------------------------------------------------

CmmParser::FunctionHeadContext::FunctionHeadContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::TypeIdentifierContext* CmmParser::FunctionHeadContext::typeIdentifier() {
  return getRuleContext<CmmParser::TypeIdentifierContext>(0);
}

CmmParser::RoutineIdentifierContext* CmmParser::FunctionHeadContext::routineIdentifier() {
  return getRuleContext<CmmParser::RoutineIdentifierContext>(0);
}

CmmParser::ParametersContext* CmmParser::FunctionHeadContext::parameters() {
  return getRuleContext<CmmParser::ParametersContext>(0);
}


size_t CmmParser::FunctionHeadContext::getRuleIndex() const {
  return CmmParser::RuleFunctionHead;
}


antlrcpp::Any CmmParser::FunctionHeadContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFunctionHead(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FunctionHeadContext* CmmParser::functionHead() {
  FunctionHeadContext *_localctx = _tracker.createInstance<FunctionHeadContext>(_ctx, getState());
  enterRule(_localctx, 66, CmmParser::RuleFunctionHead);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(372);
    typeIdentifier();
    setState(373);
    routineIdentifier();
    setState(374);
    match(CmmParser::T__1);
    setState(376);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::VAR

    || _la == CmmParser::IDENTIFIER) {
      setState(375);
      parameters();
    }
    setState(378);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RoutineIdentifierContext ------------------------------------------------------------------

CmmParser::RoutineIdentifierContext::RoutineIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::RoutineIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::RoutineIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleRoutineIdentifier;
}


antlrcpp::Any CmmParser::RoutineIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRoutineIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RoutineIdentifierContext* CmmParser::routineIdentifier() {
  RoutineIdentifierContext *_localctx = _tracker.createInstance<RoutineIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 68, CmmParser::RuleRoutineIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(380);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParametersContext ------------------------------------------------------------------

CmmParser::ParametersContext::ParametersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ParameterDeclarationsListContext* CmmParser::ParametersContext::parameterDeclarationsList() {
  return getRuleContext<CmmParser::ParameterDeclarationsListContext>(0);
}


size_t CmmParser::ParametersContext::getRuleIndex() const {
  return CmmParser::RuleParameters;
}


antlrcpp::Any CmmParser::ParametersContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParameters(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ParametersContext* CmmParser::parameters() {
  ParametersContext *_localctx = _tracker.createInstance<ParametersContext>(_ctx, getState());
  enterRule(_localctx, 70, CmmParser::RuleParameters);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(382);
    parameterDeclarationsList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterDeclarationsListContext ------------------------------------------------------------------

CmmParser::ParameterDeclarationsListContext::ParameterDeclarationsListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::ParameterDeclarationsContext *> CmmParser::ParameterDeclarationsListContext::parameterDeclarations() {
  return getRuleContexts<CmmParser::ParameterDeclarationsContext>();
}

CmmParser::ParameterDeclarationsContext* CmmParser::ParameterDeclarationsListContext::parameterDeclarations(size_t i) {
  return getRuleContext<CmmParser::ParameterDeclarationsContext>(i);
}


size_t CmmParser::ParameterDeclarationsListContext::getRuleIndex() const {
  return CmmParser::RuleParameterDeclarationsList;
}


antlrcpp::Any CmmParser::ParameterDeclarationsListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParameterDeclarationsList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ParameterDeclarationsListContext* CmmParser::parameterDeclarationsList() {
  ParameterDeclarationsListContext *_localctx = _tracker.createInstance<ParameterDeclarationsListContext>(_ctx, getState());
  enterRule(_localctx, 72, CmmParser::RuleParameterDeclarationsList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(384);
    parameterDeclarations();
    setState(389);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(385);
      match(CmmParser::T__2);
      setState(386);
      parameterDeclarations();
      setState(391);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterDeclarationsContext ------------------------------------------------------------------

CmmParser::ParameterDeclarationsContext::ParameterDeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::TypeIdentifierContext* CmmParser::ParameterDeclarationsContext::typeIdentifier() {
  return getRuleContext<CmmParser::TypeIdentifierContext>(0);
}

CmmParser::ParameterIdentifierListContext* CmmParser::ParameterDeclarationsContext::parameterIdentifierList() {
  return getRuleContext<CmmParser::ParameterIdentifierListContext>(0);
}

tree::TerminalNode* CmmParser::ParameterDeclarationsContext::VAR() {
  return getToken(CmmParser::VAR, 0);
}


size_t CmmParser::ParameterDeclarationsContext::getRuleIndex() const {
  return CmmParser::RuleParameterDeclarations;
}


antlrcpp::Any CmmParser::ParameterDeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParameterDeclarations(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ParameterDeclarationsContext* CmmParser::parameterDeclarations() {
  ParameterDeclarationsContext *_localctx = _tracker.createInstance<ParameterDeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 74, CmmParser::RuleParameterDeclarations);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(393);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::VAR) {
      setState(392);
      match(CmmParser::VAR);
    }
    setState(395);
    typeIdentifier();
    setState(396);
    parameterIdentifierList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterIdentifierListContext ------------------------------------------------------------------

CmmParser::ParameterIdentifierListContext::ParameterIdentifierListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::ParameterIdentifierContext *> CmmParser::ParameterIdentifierListContext::parameterIdentifier() {
  return getRuleContexts<CmmParser::ParameterIdentifierContext>();
}

CmmParser::ParameterIdentifierContext* CmmParser::ParameterIdentifierListContext::parameterIdentifier(size_t i) {
  return getRuleContext<CmmParser::ParameterIdentifierContext>(i);
}


size_t CmmParser::ParameterIdentifierListContext::getRuleIndex() const {
  return CmmParser::RuleParameterIdentifierList;
}


antlrcpp::Any CmmParser::ParameterIdentifierListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParameterIdentifierList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ParameterIdentifierListContext* CmmParser::parameterIdentifierList() {
  ParameterIdentifierListContext *_localctx = _tracker.createInstance<ParameterIdentifierListContext>(_ctx, getState());
  enterRule(_localctx, 76, CmmParser::RuleParameterIdentifierList);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(398);
    parameterIdentifier();
    setState(403);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(399);
        match(CmmParser::T__2);
        setState(400);
        parameterIdentifier(); 
      }
      setState(405);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 24, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterIdentifierContext ------------------------------------------------------------------

CmmParser::ParameterIdentifierContext::ParameterIdentifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ParameterIdentifierContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::ParameterIdentifierContext::getRuleIndex() const {
  return CmmParser::RuleParameterIdentifier;
}


antlrcpp::Any CmmParser::ParameterIdentifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParameterIdentifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ParameterIdentifierContext* CmmParser::parameterIdentifier() {
  ParameterIdentifierContext *_localctx = _tracker.createInstance<ParameterIdentifierContext>(_ctx, getState());
  enterRule(_localctx, 78, CmmParser::RuleParameterIdentifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(406);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

CmmParser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::CompoundStatementContext* CmmParser::StatementContext::compoundStatement() {
  return getRuleContext<CmmParser::CompoundStatementContext>(0);
}

CmmParser::AssignmentStatementContext* CmmParser::StatementContext::assignmentStatement() {
  return getRuleContext<CmmParser::AssignmentStatementContext>(0);
}

CmmParser::IfStatementContext* CmmParser::StatementContext::ifStatement() {
  return getRuleContext<CmmParser::IfStatementContext>(0);
}

CmmParser::CaseStatementContext* CmmParser::StatementContext::caseStatement() {
  return getRuleContext<CmmParser::CaseStatementContext>(0);
}

CmmParser::RepeatStatementContext* CmmParser::StatementContext::repeatStatement() {
  return getRuleContext<CmmParser::RepeatStatementContext>(0);
}

CmmParser::WhileStatementContext* CmmParser::StatementContext::whileStatement() {
  return getRuleContext<CmmParser::WhileStatementContext>(0);
}

CmmParser::ForStatementContext* CmmParser::StatementContext::forStatement() {
  return getRuleContext<CmmParser::ForStatementContext>(0);
}

CmmParser::WriteStatementContext* CmmParser::StatementContext::writeStatement() {
  return getRuleContext<CmmParser::WriteStatementContext>(0);
}

CmmParser::WritelnStatementContext* CmmParser::StatementContext::writelnStatement() {
  return getRuleContext<CmmParser::WritelnStatementContext>(0);
}

CmmParser::ReadStatementContext* CmmParser::StatementContext::readStatement() {
  return getRuleContext<CmmParser::ReadStatementContext>(0);
}

CmmParser::ReadlnStatementContext* CmmParser::StatementContext::readlnStatement() {
  return getRuleContext<CmmParser::ReadlnStatementContext>(0);
}

CmmParser::ProcedureCallStatementContext* CmmParser::StatementContext::procedureCallStatement() {
  return getRuleContext<CmmParser::ProcedureCallStatementContext>(0);
}

CmmParser::ReturnStatementContext* CmmParser::StatementContext::returnStatement() {
  return getRuleContext<CmmParser::ReturnStatementContext>(0);
}

CmmParser::EmptyStatementContext* CmmParser::StatementContext::emptyStatement() {
  return getRuleContext<CmmParser::EmptyStatementContext>(0);
}


size_t CmmParser::StatementContext::getRuleIndex() const {
  return CmmParser::RuleStatement;
}


antlrcpp::Any CmmParser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::StatementContext* CmmParser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 80, CmmParser::RuleStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(422);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 25, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(408);
      compoundStatement();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(409);
      assignmentStatement();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(410);
      ifStatement();
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(411);
      caseStatement();
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(412);
      repeatStatement();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(413);
      whileStatement();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);
      setState(414);
      forStatement();
      break;
    }

    case 8: {
      enterOuterAlt(_localctx, 8);
      setState(415);
      writeStatement();
      break;
    }

    case 9: {
      enterOuterAlt(_localctx, 9);
      setState(416);
      writelnStatement();
      break;
    }

    case 10: {
      enterOuterAlt(_localctx, 10);
      setState(417);
      readStatement();
      break;
    }

    case 11: {
      enterOuterAlt(_localctx, 11);
      setState(418);
      readlnStatement();
      break;
    }

    case 12: {
      enterOuterAlt(_localctx, 12);
      setState(419);
      procedureCallStatement();
      break;
    }

    case 13: {
      enterOuterAlt(_localctx, 13);
      setState(420);
      returnStatement();
      break;
    }

    case 14: {
      enterOuterAlt(_localctx, 14);
      setState(421);
      emptyStatement();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompoundStatementContext ------------------------------------------------------------------

CmmParser::CompoundStatementContext::CompoundStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::CompoundStatementContext::BEGIN() {
  return getToken(CmmParser::BEGIN, 0);
}

CmmParser::StatementListContext* CmmParser::CompoundStatementContext::statementList() {
  return getRuleContext<CmmParser::StatementListContext>(0);
}

tree::TerminalNode* CmmParser::CompoundStatementContext::END() {
  return getToken(CmmParser::END, 0);
}


size_t CmmParser::CompoundStatementContext::getRuleIndex() const {
  return CmmParser::RuleCompoundStatement;
}


antlrcpp::Any CmmParser::CompoundStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCompoundStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CompoundStatementContext* CmmParser::compoundStatement() {
  CompoundStatementContext *_localctx = _tracker.createInstance<CompoundStatementContext>(_ctx, getState());
  enterRule(_localctx, 82, CmmParser::RuleCompoundStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(424);
    match(CmmParser::BEGIN);
    setState(425);
    statementList();
    setState(426);
    match(CmmParser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EmptyStatementContext ------------------------------------------------------------------

CmmParser::EmptyStatementContext::EmptyStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::EmptyStatementContext::getRuleIndex() const {
  return CmmParser::RuleEmptyStatement;
}


antlrcpp::Any CmmParser::EmptyStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitEmptyStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::EmptyStatementContext* CmmParser::emptyStatement() {
  EmptyStatementContext *_localctx = _tracker.createInstance<EmptyStatementContext>(_ctx, getState());
  enterRule(_localctx, 84, CmmParser::RuleEmptyStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);

   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementListContext ------------------------------------------------------------------

CmmParser::StatementListContext::StatementListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::StatementContext *> CmmParser::StatementListContext::statement() {
  return getRuleContexts<CmmParser::StatementContext>();
}

CmmParser::StatementContext* CmmParser::StatementListContext::statement(size_t i) {
  return getRuleContext<CmmParser::StatementContext>(i);
}


size_t CmmParser::StatementListContext::getRuleIndex() const {
  return CmmParser::RuleStatementList;
}


antlrcpp::Any CmmParser::StatementListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitStatementList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::StatementListContext* CmmParser::statementList() {
  StatementListContext *_localctx = _tracker.createInstance<StatementListContext>(_ctx, getState());
  enterRule(_localctx, 86, CmmParser::RuleStatementList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(430);
    statement();
    setState(435);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__0) {
      setState(431);
      match(CmmParser::T__0);
      setState(432);
      statement();
      setState(437);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentStatementContext ------------------------------------------------------------------

CmmParser::AssignmentStatementContext::AssignmentStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::AssignmentStatementContext::getRuleIndex() const {
  return CmmParser::RuleAssignmentStatement;
}

void CmmParser::AssignmentStatementContext::copyFrom(AssignmentStatementContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- NormalAssignmentContext ------------------------------------------------------------------

CmmParser::LhsContext* CmmParser::NormalAssignmentContext::lhs() {
  return getRuleContext<CmmParser::LhsContext>(0);
}

CmmParser::RhsContext* CmmParser::NormalAssignmentContext::rhs() {
  return getRuleContext<CmmParser::RhsContext>(0);
}

CmmParser::NormalAssignmentContext::NormalAssignmentContext(AssignmentStatementContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::NormalAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitNormalAssignment(this);
  else
    return visitor->visitChildren(this);
}
//----------------- DecrementAssignmentContext ------------------------------------------------------------------

CmmParser::VariableContext* CmmParser::DecrementAssignmentContext::variable() {
  return getRuleContext<CmmParser::VariableContext>(0);
}

CmmParser::DecrementAssignmentContext::DecrementAssignmentContext(AssignmentStatementContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::DecrementAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitDecrementAssignment(this);
  else
    return visitor->visitChildren(this);
}
//----------------- IncrementAssignmentContext ------------------------------------------------------------------

CmmParser::VariableContext* CmmParser::IncrementAssignmentContext::variable() {
  return getRuleContext<CmmParser::VariableContext>(0);
}

CmmParser::IncrementAssignmentContext::IncrementAssignmentContext(AssignmentStatementContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::IncrementAssignmentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitIncrementAssignment(this);
  else
    return visitor->visitChildren(this);
}
CmmParser::AssignmentStatementContext* CmmParser::assignmentStatement() {
  AssignmentStatementContext *_localctx = _tracker.createInstance<AssignmentStatementContext>(_ctx, getState());
  enterRule(_localctx, 88, CmmParser::RuleAssignmentStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(456);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 29, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<AssignmentStatementContext *>(_tracker.createInstance<CmmParser::NormalAssignmentContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(438);
      lhs();
      setState(439);
      match(CmmParser::T__4);
      setState(440);
      rhs();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<AssignmentStatementContext *>(_tracker.createInstance<CmmParser::IncrementAssignmentContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(447);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case CmmParser::T__10: {
          setState(442);
          match(CmmParser::T__10);
          setState(443);
          variable();
          break;
        }

        case CmmParser::IDENTIFIER: {
          setState(444);
          variable();
          setState(445);
          match(CmmParser::T__10);
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      break;
    }

    case 3: {
      _localctx = dynamic_cast<AssignmentStatementContext *>(_tracker.createInstance<CmmParser::DecrementAssignmentContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(454);
      _errHandler->sync(this);
      switch (_input->LA(1)) {
        case CmmParser::T__11: {
          setState(449);
          match(CmmParser::T__11);
          setState(450);
          variable();
          break;
        }

        case CmmParser::IDENTIFIER: {
          setState(451);
          variable();
          setState(452);
          match(CmmParser::T__11);
          break;
        }

      default:
        throw NoViableAltException(this);
      }
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LhsContext ------------------------------------------------------------------

CmmParser::LhsContext::LhsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::VariableContext* CmmParser::LhsContext::variable() {
  return getRuleContext<CmmParser::VariableContext>(0);
}


size_t CmmParser::LhsContext::getRuleIndex() const {
  return CmmParser::RuleLhs;
}


antlrcpp::Any CmmParser::LhsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitLhs(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::LhsContext* CmmParser::lhs() {
  LhsContext *_localctx = _tracker.createInstance<LhsContext>(_ctx, getState());
  enterRule(_localctx, 90, CmmParser::RuleLhs);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(458);
    variable();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RhsContext ------------------------------------------------------------------

CmmParser::RhsContext::RhsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ExpressionContext* CmmParser::RhsContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::RhsContext::getRuleIndex() const {
  return CmmParser::RuleRhs;
}


antlrcpp::Any CmmParser::RhsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRhs(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RhsContext* CmmParser::rhs() {
  RhsContext *_localctx = _tracker.createInstance<RhsContext>(_ctx, getState());
  enterRule(_localctx, 92, CmmParser::RuleRhs);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(460);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IfStatementContext ------------------------------------------------------------------

CmmParser::IfStatementContext::IfStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::IfStatementContext::IF() {
  return getToken(CmmParser::IF, 0);
}

CmmParser::ExpressionContext* CmmParser::IfStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}

CmmParser::TrueStatementContext* CmmParser::IfStatementContext::trueStatement() {
  return getRuleContext<CmmParser::TrueStatementContext>(0);
}

tree::TerminalNode* CmmParser::IfStatementContext::ELSE() {
  return getToken(CmmParser::ELSE, 0);
}

CmmParser::FalseStatementContext* CmmParser::IfStatementContext::falseStatement() {
  return getRuleContext<CmmParser::FalseStatementContext>(0);
}


size_t CmmParser::IfStatementContext::getRuleIndex() const {
  return CmmParser::RuleIfStatement;
}


antlrcpp::Any CmmParser::IfStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitIfStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::IfStatementContext* CmmParser::ifStatement() {
  IfStatementContext *_localctx = _tracker.createInstance<IfStatementContext>(_ctx, getState());
  enterRule(_localctx, 94, CmmParser::RuleIfStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(462);
    match(CmmParser::IF);
    setState(463);
    match(CmmParser::T__1);
    setState(464);
    expression();
    setState(465);
    match(CmmParser::T__3);
    setState(466);
    trueStatement();
    setState(469);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 30, _ctx)) {
    case 1: {
      setState(467);
      match(CmmParser::ELSE);
      setState(468);
      falseStatement();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TrueStatementContext ------------------------------------------------------------------

CmmParser::TrueStatementContext::TrueStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::StatementContext* CmmParser::TrueStatementContext::statement() {
  return getRuleContext<CmmParser::StatementContext>(0);
}


size_t CmmParser::TrueStatementContext::getRuleIndex() const {
  return CmmParser::RuleTrueStatement;
}


antlrcpp::Any CmmParser::TrueStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTrueStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TrueStatementContext* CmmParser::trueStatement() {
  TrueStatementContext *_localctx = _tracker.createInstance<TrueStatementContext>(_ctx, getState());
  enterRule(_localctx, 96, CmmParser::RuleTrueStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(471);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FalseStatementContext ------------------------------------------------------------------

CmmParser::FalseStatementContext::FalseStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::StatementContext* CmmParser::FalseStatementContext::statement() {
  return getRuleContext<CmmParser::StatementContext>(0);
}


size_t CmmParser::FalseStatementContext::getRuleIndex() const {
  return CmmParser::RuleFalseStatement;
}


antlrcpp::Any CmmParser::FalseStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFalseStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FalseStatementContext* CmmParser::falseStatement() {
  FalseStatementContext *_localctx = _tracker.createInstance<FalseStatementContext>(_ctx, getState());
  enterRule(_localctx, 98, CmmParser::RuleFalseStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(473);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReturnStatementContext ------------------------------------------------------------------

CmmParser::ReturnStatementContext::ReturnStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ReturnStatementContext::RETURN() {
  return getToken(CmmParser::RETURN, 0);
}

CmmParser::ExpressionContext* CmmParser::ReturnStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::ReturnStatementContext::getRuleIndex() const {
  return CmmParser::RuleReturnStatement;
}


antlrcpp::Any CmmParser::ReturnStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitReturnStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ReturnStatementContext* CmmParser::returnStatement() {
  ReturnStatementContext *_localctx = _tracker.createInstance<ReturnStatementContext>(_ctx, getState());
  enterRule(_localctx, 100, CmmParser::RuleReturnStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(475);
    match(CmmParser::RETURN);
    setState(477);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__1)
      | (1ULL << CmmParser::T__5)
      | (1ULL << CmmParser::T__6)
      | (1ULL << CmmParser::NOT)
      | (1ULL << CmmParser::IDENTIFIER)
      | (1ULL << CmmParser::INTEGER)
      | (1ULL << CmmParser::REAL))) != 0) || _la == CmmParser::CHARACTER

    || _la == CmmParser::STRING) {
      setState(476);
      expression();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseStatementContext ------------------------------------------------------------------

CmmParser::CaseStatementContext::CaseStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::CaseStatementContext::SWITCH() {
  return getToken(CmmParser::SWITCH, 0);
}

CmmParser::ExpressionContext* CmmParser::CaseStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}

tree::TerminalNode* CmmParser::CaseStatementContext::BEGIN() {
  return getToken(CmmParser::BEGIN, 0);
}

CmmParser::CaseBranchListContext* CmmParser::CaseStatementContext::caseBranchList() {
  return getRuleContext<CmmParser::CaseBranchListContext>(0);
}

tree::TerminalNode* CmmParser::CaseStatementContext::END() {
  return getToken(CmmParser::END, 0);
}


size_t CmmParser::CaseStatementContext::getRuleIndex() const {
  return CmmParser::RuleCaseStatement;
}


antlrcpp::Any CmmParser::CaseStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCaseStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CaseStatementContext* CmmParser::caseStatement() {
  CaseStatementContext *_localctx = _tracker.createInstance<CaseStatementContext>(_ctx, getState());
  enterRule(_localctx, 102, CmmParser::RuleCaseStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(479);
    match(CmmParser::SWITCH);
    setState(480);
    match(CmmParser::T__1);
    setState(481);
    expression();
    setState(482);
    match(CmmParser::T__3);
    setState(483);
    match(CmmParser::BEGIN);
    setState(484);
    caseBranchList();
    setState(485);
    match(CmmParser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseBranchListContext ------------------------------------------------------------------

CmmParser::CaseBranchListContext::CaseBranchListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::CaseBranchContext *> CmmParser::CaseBranchListContext::caseBranch() {
  return getRuleContexts<CmmParser::CaseBranchContext>();
}

CmmParser::CaseBranchContext* CmmParser::CaseBranchListContext::caseBranch(size_t i) {
  return getRuleContext<CmmParser::CaseBranchContext>(i);
}


size_t CmmParser::CaseBranchListContext::getRuleIndex() const {
  return CmmParser::RuleCaseBranchList;
}


antlrcpp::Any CmmParser::CaseBranchListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCaseBranchList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CaseBranchListContext* CmmParser::caseBranchList() {
  CaseBranchListContext *_localctx = _tracker.createInstance<CaseBranchListContext>(_ctx, getState());
  enterRule(_localctx, 104, CmmParser::RuleCaseBranchList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(487);
    caseBranch();
    setState(492);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__0) {
      setState(488);
      match(CmmParser::T__0);
      setState(489);
      caseBranch();
      setState(494);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseBranchContext ------------------------------------------------------------------

CmmParser::CaseBranchContext::CaseBranchContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::CaseConstantListContext* CmmParser::CaseBranchContext::caseConstantList() {
  return getRuleContext<CmmParser::CaseConstantListContext>(0);
}

CmmParser::StatementContext* CmmParser::CaseBranchContext::statement() {
  return getRuleContext<CmmParser::StatementContext>(0);
}


size_t CmmParser::CaseBranchContext::getRuleIndex() const {
  return CmmParser::RuleCaseBranch;
}


antlrcpp::Any CmmParser::CaseBranchContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCaseBranch(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CaseBranchContext* CmmParser::caseBranch() {
  CaseBranchContext *_localctx = _tracker.createInstance<CaseBranchContext>(_ctx, getState());
  enterRule(_localctx, 106, CmmParser::RuleCaseBranch);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(500);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::CASE: {
        enterOuterAlt(_localctx, 1);
        setState(495);
        caseConstantList();
        setState(496);
        match(CmmParser::T__12);
        setState(497);
        statement();
        break;
      }

      case CmmParser::T__0:
      case CmmParser::END: {
        enterOuterAlt(_localctx, 2);

        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseConstantListContext ------------------------------------------------------------------

CmmParser::CaseConstantListContext::CaseConstantListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> CmmParser::CaseConstantListContext::CASE() {
  return getTokens(CmmParser::CASE);
}

tree::TerminalNode* CmmParser::CaseConstantListContext::CASE(size_t i) {
  return getToken(CmmParser::CASE, i);
}

std::vector<CmmParser::CaseConstantContext *> CmmParser::CaseConstantListContext::caseConstant() {
  return getRuleContexts<CmmParser::CaseConstantContext>();
}

CmmParser::CaseConstantContext* CmmParser::CaseConstantListContext::caseConstant(size_t i) {
  return getRuleContext<CmmParser::CaseConstantContext>(i);
}


size_t CmmParser::CaseConstantListContext::getRuleIndex() const {
  return CmmParser::RuleCaseConstantList;
}


antlrcpp::Any CmmParser::CaseConstantListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCaseConstantList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CaseConstantListContext* CmmParser::caseConstantList() {
  CaseConstantListContext *_localctx = _tracker.createInstance<CaseConstantListContext>(_ctx, getState());
  enterRule(_localctx, 108, CmmParser::RuleCaseConstantList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(502);
    match(CmmParser::CASE);
    setState(503);
    caseConstant();
    setState(509);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(504);
      match(CmmParser::T__2);
      setState(505);
      match(CmmParser::CASE);
      setState(506);
      caseConstant();
      setState(511);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseConstantContext ------------------------------------------------------------------

CmmParser::CaseConstantContext::CaseConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ConstantContext* CmmParser::CaseConstantContext::constant() {
  return getRuleContext<CmmParser::ConstantContext>(0);
}


size_t CmmParser::CaseConstantContext::getRuleIndex() const {
  return CmmParser::RuleCaseConstant;
}


antlrcpp::Any CmmParser::CaseConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCaseConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CaseConstantContext* CmmParser::caseConstant() {
  CaseConstantContext *_localctx = _tracker.createInstance<CaseConstantContext>(_ctx, getState());
  enterRule(_localctx, 110, CmmParser::RuleCaseConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(512);
    constant();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RepeatStatementContext ------------------------------------------------------------------

CmmParser::RepeatStatementContext::RepeatStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::RepeatStatementContext::DO() {
  return getToken(CmmParser::DO, 0);
}

tree::TerminalNode* CmmParser::RepeatStatementContext::BEGIN() {
  return getToken(CmmParser::BEGIN, 0);
}

CmmParser::StatementListContext* CmmParser::RepeatStatementContext::statementList() {
  return getRuleContext<CmmParser::StatementListContext>(0);
}

tree::TerminalNode* CmmParser::RepeatStatementContext::END() {
  return getToken(CmmParser::END, 0);
}

tree::TerminalNode* CmmParser::RepeatStatementContext::WHILE() {
  return getToken(CmmParser::WHILE, 0);
}

CmmParser::ExpressionContext* CmmParser::RepeatStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::RepeatStatementContext::getRuleIndex() const {
  return CmmParser::RuleRepeatStatement;
}


antlrcpp::Any CmmParser::RepeatStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRepeatStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RepeatStatementContext* CmmParser::repeatStatement() {
  RepeatStatementContext *_localctx = _tracker.createInstance<RepeatStatementContext>(_ctx, getState());
  enterRule(_localctx, 112, CmmParser::RuleRepeatStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(514);
    match(CmmParser::DO);
    setState(515);
    match(CmmParser::BEGIN);
    setState(516);
    statementList();
    setState(517);
    match(CmmParser::END);
    setState(518);
    match(CmmParser::WHILE);
    setState(519);
    match(CmmParser::T__1);
    setState(520);
    expression();
    setState(521);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WhileStatementContext ------------------------------------------------------------------

CmmParser::WhileStatementContext::WhileStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::WhileStatementContext::WHILE() {
  return getToken(CmmParser::WHILE, 0);
}

CmmParser::ExpressionContext* CmmParser::WhileStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}

CmmParser::StatementContext* CmmParser::WhileStatementContext::statement() {
  return getRuleContext<CmmParser::StatementContext>(0);
}


size_t CmmParser::WhileStatementContext::getRuleIndex() const {
  return CmmParser::RuleWhileStatement;
}


antlrcpp::Any CmmParser::WhileStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitWhileStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::WhileStatementContext* CmmParser::whileStatement() {
  WhileStatementContext *_localctx = _tracker.createInstance<WhileStatementContext>(_ctx, getState());
  enterRule(_localctx, 114, CmmParser::RuleWhileStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(523);
    match(CmmParser::WHILE);
    setState(524);
    match(CmmParser::T__1);
    setState(525);
    expression();
    setState(526);
    match(CmmParser::T__3);
    setState(527);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ForStatementContext ------------------------------------------------------------------

CmmParser::ForStatementContext::ForStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ForStatementContext::FOR() {
  return getToken(CmmParser::FOR, 0);
}

std::vector<CmmParser::StatementContext *> CmmParser::ForStatementContext::statement() {
  return getRuleContexts<CmmParser::StatementContext>();
}

CmmParser::StatementContext* CmmParser::ForStatementContext::statement(size_t i) {
  return getRuleContext<CmmParser::StatementContext>(i);
}

CmmParser::ExpressionContext* CmmParser::ForStatementContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::ForStatementContext::getRuleIndex() const {
  return CmmParser::RuleForStatement;
}


antlrcpp::Any CmmParser::ForStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitForStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ForStatementContext* CmmParser::forStatement() {
  ForStatementContext *_localctx = _tracker.createInstance<ForStatementContext>(_ctx, getState());
  enterRule(_localctx, 116, CmmParser::RuleForStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(529);
    match(CmmParser::FOR);
    setState(530);
    match(CmmParser::T__1);
    setState(531);
    statement();
    setState(532);
    match(CmmParser::T__0);
    setState(533);
    expression();
    setState(534);
    match(CmmParser::T__0);
    setState(535);
    statement();
    setState(536);
    match(CmmParser::T__3);
    setState(537);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureCallStatementContext ------------------------------------------------------------------

CmmParser::ProcedureCallStatementContext::ProcedureCallStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ProcedureNameContext* CmmParser::ProcedureCallStatementContext::procedureName() {
  return getRuleContext<CmmParser::ProcedureNameContext>(0);
}

CmmParser::ArgumentListContext* CmmParser::ProcedureCallStatementContext::argumentList() {
  return getRuleContext<CmmParser::ArgumentListContext>(0);
}


size_t CmmParser::ProcedureCallStatementContext::getRuleIndex() const {
  return CmmParser::RuleProcedureCallStatement;
}


antlrcpp::Any CmmParser::ProcedureCallStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProcedureCallStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProcedureCallStatementContext* CmmParser::procedureCallStatement() {
  ProcedureCallStatementContext *_localctx = _tracker.createInstance<ProcedureCallStatementContext>(_ctx, getState());
  enterRule(_localctx, 118, CmmParser::RuleProcedureCallStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(539);
    procedureName();
    setState(540);
    match(CmmParser::T__1);
    setState(542);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__1)
      | (1ULL << CmmParser::T__5)
      | (1ULL << CmmParser::T__6)
      | (1ULL << CmmParser::NOT)
      | (1ULL << CmmParser::IDENTIFIER)
      | (1ULL << CmmParser::INTEGER)
      | (1ULL << CmmParser::REAL))) != 0) || _la == CmmParser::CHARACTER

    || _la == CmmParser::STRING) {
      setState(541);
      argumentList();
    }
    setState(544);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProcedureNameContext ------------------------------------------------------------------

CmmParser::ProcedureNameContext::ProcedureNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ProcedureNameContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::ProcedureNameContext::getRuleIndex() const {
  return CmmParser::RuleProcedureName;
}


antlrcpp::Any CmmParser::ProcedureNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitProcedureName(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ProcedureNameContext* CmmParser::procedureName() {
  ProcedureNameContext *_localctx = _tracker.createInstance<ProcedureNameContext>(_ctx, getState());
  enterRule(_localctx, 120, CmmParser::RuleProcedureName);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(546);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgumentListContext ------------------------------------------------------------------

CmmParser::ArgumentListContext::ArgumentListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::ArgumentContext *> CmmParser::ArgumentListContext::argument() {
  return getRuleContexts<CmmParser::ArgumentContext>();
}

CmmParser::ArgumentContext* CmmParser::ArgumentListContext::argument(size_t i) {
  return getRuleContext<CmmParser::ArgumentContext>(i);
}


size_t CmmParser::ArgumentListContext::getRuleIndex() const {
  return CmmParser::RuleArgumentList;
}


antlrcpp::Any CmmParser::ArgumentListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitArgumentList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ArgumentListContext* CmmParser::argumentList() {
  ArgumentListContext *_localctx = _tracker.createInstance<ArgumentListContext>(_ctx, getState());
  enterRule(_localctx, 122, CmmParser::RuleArgumentList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(548);
    argument();
    setState(553);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(549);
      match(CmmParser::T__2);
      setState(550);
      argument();
      setState(555);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArgumentContext ------------------------------------------------------------------

CmmParser::ArgumentContext::ArgumentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ExpressionContext* CmmParser::ArgumentContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::ArgumentContext::getRuleIndex() const {
  return CmmParser::RuleArgument;
}


antlrcpp::Any CmmParser::ArgumentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitArgument(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ArgumentContext* CmmParser::argument() {
  ArgumentContext *_localctx = _tracker.createInstance<ArgumentContext>(_ctx, getState());
  enterRule(_localctx, 124, CmmParser::RuleArgument);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(556);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteStatementContext ------------------------------------------------------------------

CmmParser::WriteStatementContext::WriteStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::WriteStatementContext::PRINTF() {
  return getToken(CmmParser::PRINTF, 0);
}

CmmParser::WriteArgumentsContext* CmmParser::WriteStatementContext::writeArguments() {
  return getRuleContext<CmmParser::WriteArgumentsContext>(0);
}


size_t CmmParser::WriteStatementContext::getRuleIndex() const {
  return CmmParser::RuleWriteStatement;
}


antlrcpp::Any CmmParser::WriteStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitWriteStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::WriteStatementContext* CmmParser::writeStatement() {
  WriteStatementContext *_localctx = _tracker.createInstance<WriteStatementContext>(_ctx, getState());
  enterRule(_localctx, 126, CmmParser::RuleWriteStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(558);
    match(CmmParser::PRINTF);
    setState(559);
    writeArguments();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WritelnStatementContext ------------------------------------------------------------------

CmmParser::WritelnStatementContext::WritelnStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::WritelnStatementContext::PRINTLN() {
  return getToken(CmmParser::PRINTLN, 0);
}

CmmParser::WriteArgumentsContext* CmmParser::WritelnStatementContext::writeArguments() {
  return getRuleContext<CmmParser::WriteArgumentsContext>(0);
}


size_t CmmParser::WritelnStatementContext::getRuleIndex() const {
  return CmmParser::RuleWritelnStatement;
}


antlrcpp::Any CmmParser::WritelnStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitWritelnStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::WritelnStatementContext* CmmParser::writelnStatement() {
  WritelnStatementContext *_localctx = _tracker.createInstance<WritelnStatementContext>(_ctx, getState());
  enterRule(_localctx, 128, CmmParser::RuleWritelnStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(561);
    match(CmmParser::PRINTLN);
    setState(563);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__1) {
      setState(562);
      writeArguments();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentsContext ------------------------------------------------------------------

CmmParser::WriteArgumentsContext::WriteArgumentsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::WriteArgumentContext *> CmmParser::WriteArgumentsContext::writeArgument() {
  return getRuleContexts<CmmParser::WriteArgumentContext>();
}

CmmParser::WriteArgumentContext* CmmParser::WriteArgumentsContext::writeArgument(size_t i) {
  return getRuleContext<CmmParser::WriteArgumentContext>(i);
}


size_t CmmParser::WriteArgumentsContext::getRuleIndex() const {
  return CmmParser::RuleWriteArguments;
}


antlrcpp::Any CmmParser::WriteArgumentsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitWriteArguments(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::WriteArgumentsContext* CmmParser::writeArguments() {
  WriteArgumentsContext *_localctx = _tracker.createInstance<WriteArgumentsContext>(_ctx, getState());
  enterRule(_localctx, 130, CmmParser::RuleWriteArguments);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(565);
    match(CmmParser::T__1);
    setState(566);
    writeArgument();
    setState(571);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(567);
      match(CmmParser::T__2);
      setState(568);
      writeArgument();
      setState(573);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(574);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentContext ------------------------------------------------------------------

CmmParser::WriteArgumentContext::WriteArgumentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ExpressionContext* CmmParser::WriteArgumentContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}

CmmParser::FieldWidthContext* CmmParser::WriteArgumentContext::fieldWidth() {
  return getRuleContext<CmmParser::FieldWidthContext>(0);
}


size_t CmmParser::WriteArgumentContext::getRuleIndex() const {
  return CmmParser::RuleWriteArgument;
}


antlrcpp::Any CmmParser::WriteArgumentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitWriteArgument(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::WriteArgumentContext* CmmParser::writeArgument() {
  WriteArgumentContext *_localctx = _tracker.createInstance<WriteArgumentContext>(_ctx, getState());
  enterRule(_localctx, 132, CmmParser::RuleWriteArgument);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(576);
    expression();
    setState(579);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__12) {
      setState(577);
      match(CmmParser::T__12);
      setState(578);
      fieldWidth();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FieldWidthContext ------------------------------------------------------------------

CmmParser::FieldWidthContext::FieldWidthContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::IntegerConstantContext* CmmParser::FieldWidthContext::integerConstant() {
  return getRuleContext<CmmParser::IntegerConstantContext>(0);
}

CmmParser::SignContext* CmmParser::FieldWidthContext::sign() {
  return getRuleContext<CmmParser::SignContext>(0);
}

CmmParser::DecimalPlacesContext* CmmParser::FieldWidthContext::decimalPlaces() {
  return getRuleContext<CmmParser::DecimalPlacesContext>(0);
}


size_t CmmParser::FieldWidthContext::getRuleIndex() const {
  return CmmParser::RuleFieldWidth;
}


antlrcpp::Any CmmParser::FieldWidthContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFieldWidth(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FieldWidthContext* CmmParser::fieldWidth() {
  FieldWidthContext *_localctx = _tracker.createInstance<FieldWidthContext>(_ctx, getState());
  enterRule(_localctx, 134, CmmParser::RuleFieldWidth);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(582);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__5

    || _la == CmmParser::T__6) {
      setState(581);
      sign();
    }
    setState(584);
    integerConstant();
    setState(587);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__12) {
      setState(585);
      match(CmmParser::T__12);
      setState(586);
      decimalPlaces();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DecimalPlacesContext ------------------------------------------------------------------

CmmParser::DecimalPlacesContext::DecimalPlacesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::IntegerConstantContext* CmmParser::DecimalPlacesContext::integerConstant() {
  return getRuleContext<CmmParser::IntegerConstantContext>(0);
}


size_t CmmParser::DecimalPlacesContext::getRuleIndex() const {
  return CmmParser::RuleDecimalPlaces;
}


antlrcpp::Any CmmParser::DecimalPlacesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitDecimalPlaces(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::DecimalPlacesContext* CmmParser::decimalPlaces() {
  DecimalPlacesContext *_localctx = _tracker.createInstance<DecimalPlacesContext>(_ctx, getState());
  enterRule(_localctx, 136, CmmParser::RuleDecimalPlaces);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(589);
    integerConstant();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReadStatementContext ------------------------------------------------------------------

CmmParser::ReadStatementContext::ReadStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ReadStatementContext::READ() {
  return getToken(CmmParser::READ, 0);
}

CmmParser::ReadArgumentsContext* CmmParser::ReadStatementContext::readArguments() {
  return getRuleContext<CmmParser::ReadArgumentsContext>(0);
}


size_t CmmParser::ReadStatementContext::getRuleIndex() const {
  return CmmParser::RuleReadStatement;
}


antlrcpp::Any CmmParser::ReadStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitReadStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ReadStatementContext* CmmParser::readStatement() {
  ReadStatementContext *_localctx = _tracker.createInstance<ReadStatementContext>(_ctx, getState());
  enterRule(_localctx, 138, CmmParser::RuleReadStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(591);
    match(CmmParser::READ);
    setState(592);
    readArguments();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReadlnStatementContext ------------------------------------------------------------------

CmmParser::ReadlnStatementContext::ReadlnStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::ReadlnStatementContext::READLN() {
  return getToken(CmmParser::READLN, 0);
}

CmmParser::ReadArgumentsContext* CmmParser::ReadlnStatementContext::readArguments() {
  return getRuleContext<CmmParser::ReadArgumentsContext>(0);
}


size_t CmmParser::ReadlnStatementContext::getRuleIndex() const {
  return CmmParser::RuleReadlnStatement;
}


antlrcpp::Any CmmParser::ReadlnStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitReadlnStatement(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ReadlnStatementContext* CmmParser::readlnStatement() {
  ReadlnStatementContext *_localctx = _tracker.createInstance<ReadlnStatementContext>(_ctx, getState());
  enterRule(_localctx, 140, CmmParser::RuleReadlnStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(594);
    match(CmmParser::READLN);
    setState(595);
    readArguments();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReadArgumentsContext ------------------------------------------------------------------

CmmParser::ReadArgumentsContext::ReadArgumentsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::VariableContext *> CmmParser::ReadArgumentsContext::variable() {
  return getRuleContexts<CmmParser::VariableContext>();
}

CmmParser::VariableContext* CmmParser::ReadArgumentsContext::variable(size_t i) {
  return getRuleContext<CmmParser::VariableContext>(i);
}


size_t CmmParser::ReadArgumentsContext::getRuleIndex() const {
  return CmmParser::RuleReadArguments;
}


antlrcpp::Any CmmParser::ReadArgumentsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitReadArguments(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ReadArgumentsContext* CmmParser::readArguments() {
  ReadArgumentsContext *_localctx = _tracker.createInstance<ReadArgumentsContext>(_ctx, getState());
  enterRule(_localctx, 142, CmmParser::RuleReadArguments);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(597);
    match(CmmParser::T__1);
    setState(598);
    variable();
    setState(603);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(599);
      match(CmmParser::T__2);
      setState(600);
      variable();
      setState(605);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(606);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

CmmParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::SimpleExpressionContext *> CmmParser::ExpressionContext::simpleExpression() {
  return getRuleContexts<CmmParser::SimpleExpressionContext>();
}

CmmParser::SimpleExpressionContext* CmmParser::ExpressionContext::simpleExpression(size_t i) {
  return getRuleContext<CmmParser::SimpleExpressionContext>(i);
}

CmmParser::RelOpContext* CmmParser::ExpressionContext::relOp() {
  return getRuleContext<CmmParser::RelOpContext>(0);
}


size_t CmmParser::ExpressionContext::getRuleIndex() const {
  return CmmParser::RuleExpression;
}


antlrcpp::Any CmmParser::ExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitExpression(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ExpressionContext* CmmParser::expression() {
  ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, getState());
  enterRule(_localctx, 144, CmmParser::RuleExpression);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(608);
    simpleExpression();
    setState(612);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__4)
      | (1ULL << CmmParser::T__14)
      | (1ULL << CmmParser::T__15)
      | (1ULL << CmmParser::T__16)
      | (1ULL << CmmParser::T__17)
      | (1ULL << CmmParser::T__18))) != 0)) {
      setState(609);
      relOp();
      setState(610);
      simpleExpression();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SimpleExpressionContext ------------------------------------------------------------------

CmmParser::SimpleExpressionContext::SimpleExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::TermContext *> CmmParser::SimpleExpressionContext::term() {
  return getRuleContexts<CmmParser::TermContext>();
}

CmmParser::TermContext* CmmParser::SimpleExpressionContext::term(size_t i) {
  return getRuleContext<CmmParser::TermContext>(i);
}

CmmParser::SignContext* CmmParser::SimpleExpressionContext::sign() {
  return getRuleContext<CmmParser::SignContext>(0);
}

std::vector<CmmParser::AddOpContext *> CmmParser::SimpleExpressionContext::addOp() {
  return getRuleContexts<CmmParser::AddOpContext>();
}

CmmParser::AddOpContext* CmmParser::SimpleExpressionContext::addOp(size_t i) {
  return getRuleContext<CmmParser::AddOpContext>(i);
}


size_t CmmParser::SimpleExpressionContext::getRuleIndex() const {
  return CmmParser::RuleSimpleExpression;
}


antlrcpp::Any CmmParser::SimpleExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitSimpleExpression(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::SimpleExpressionContext* CmmParser::simpleExpression() {
  SimpleExpressionContext *_localctx = _tracker.createInstance<SimpleExpressionContext>(_ctx, getState());
  enterRule(_localctx, 146, CmmParser::RuleSimpleExpression);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(615);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 44, _ctx)) {
    case 1: {
      setState(614);
      sign();
      break;
    }

    default:
      break;
    }
    setState(617);
    term();
    setState(623);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__5)
      | (1ULL << CmmParser::T__6)
      | (1ULL << CmmParser::OR))) != 0)) {
      setState(618);
      addOp();
      setState(619);
      term();
      setState(625);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TermContext ------------------------------------------------------------------

CmmParser::TermContext::TermContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::FactorContext *> CmmParser::TermContext::factor() {
  return getRuleContexts<CmmParser::FactorContext>();
}

CmmParser::FactorContext* CmmParser::TermContext::factor(size_t i) {
  return getRuleContext<CmmParser::FactorContext>(i);
}

std::vector<CmmParser::MulOpContext *> CmmParser::TermContext::mulOp() {
  return getRuleContexts<CmmParser::MulOpContext>();
}

CmmParser::MulOpContext* CmmParser::TermContext::mulOp(size_t i) {
  return getRuleContext<CmmParser::MulOpContext>(i);
}


size_t CmmParser::TermContext::getRuleIndex() const {
  return CmmParser::RuleTerm;
}


antlrcpp::Any CmmParser::TermContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitTerm(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::TermContext* CmmParser::term() {
  TermContext *_localctx = _tracker.createInstance<TermContext>(_ctx, getState());
  enterRule(_localctx, 148, CmmParser::RuleTerm);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(626);
    factor();
    setState(632);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__19)
      | (1ULL << CmmParser::T__20)
      | (1ULL << CmmParser::DIV)
      | (1ULL << CmmParser::MOD)
      | (1ULL << CmmParser::AND))) != 0)) {
      setState(627);
      mulOp();
      setState(628);
      factor();
      setState(634);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FactorContext ------------------------------------------------------------------

CmmParser::FactorContext::FactorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::FactorContext::getRuleIndex() const {
  return CmmParser::RuleFactor;
}

void CmmParser::FactorContext::copyFrom(FactorContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
  this->type = ctx->type;
}

//----------------- NumberFactorContext ------------------------------------------------------------------

CmmParser::NumberContext* CmmParser::NumberFactorContext::number() {
  return getRuleContext<CmmParser::NumberContext>(0);
}

CmmParser::NumberFactorContext::NumberFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::NumberFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitNumberFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- StringFactorContext ------------------------------------------------------------------

CmmParser::StringConstantContext* CmmParser::StringFactorContext::stringConstant() {
  return getRuleContext<CmmParser::StringConstantContext>(0);
}

CmmParser::StringFactorContext::StringFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::StringFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitStringFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CharacterFactorContext ------------------------------------------------------------------

CmmParser::CharacterConstantContext* CmmParser::CharacterFactorContext::characterConstant() {
  return getRuleContext<CmmParser::CharacterConstantContext>(0);
}

CmmParser::CharacterFactorContext::CharacterFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::CharacterFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCharacterFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- VariableFactorContext ------------------------------------------------------------------

CmmParser::VariableContext* CmmParser::VariableFactorContext::variable() {
  return getRuleContext<CmmParser::VariableContext>(0);
}

CmmParser::VariableFactorContext::VariableFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::VariableFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariableFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- FunctionCallFactorContext ------------------------------------------------------------------

CmmParser::FunctionCallContext* CmmParser::FunctionCallFactorContext::functionCall() {
  return getRuleContext<CmmParser::FunctionCallContext>(0);
}

CmmParser::FunctionCallFactorContext::FunctionCallFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::FunctionCallFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFunctionCallFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NotFactorContext ------------------------------------------------------------------

tree::TerminalNode* CmmParser::NotFactorContext::NOT() {
  return getToken(CmmParser::NOT, 0);
}

CmmParser::FactorContext* CmmParser::NotFactorContext::factor() {
  return getRuleContext<CmmParser::FactorContext>(0);
}

CmmParser::NotFactorContext::NotFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::NotFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitNotFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParenthesizedFactorContext ------------------------------------------------------------------

CmmParser::ExpressionContext* CmmParser::ParenthesizedFactorContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}

CmmParser::ParenthesizedFactorContext::ParenthesizedFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any CmmParser::ParenthesizedFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitParenthesizedFactor(this);
  else
    return visitor->visitChildren(this);
}
CmmParser::FactorContext* CmmParser::factor() {
  FactorContext *_localctx = _tracker.createInstance<FactorContext>(_ctx, getState());
  enterRule(_localctx, 150, CmmParser::RuleFactor);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(646);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 47, _ctx)) {
    case 1: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::VariableFactorContext>(_localctx));
      enterOuterAlt(_localctx, 1);
      setState(635);
      variable();
      break;
    }

    case 2: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::NumberFactorContext>(_localctx));
      enterOuterAlt(_localctx, 2);
      setState(636);
      number();
      break;
    }

    case 3: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::CharacterFactorContext>(_localctx));
      enterOuterAlt(_localctx, 3);
      setState(637);
      characterConstant();
      break;
    }

    case 4: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::StringFactorContext>(_localctx));
      enterOuterAlt(_localctx, 4);
      setState(638);
      stringConstant();
      break;
    }

    case 5: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::FunctionCallFactorContext>(_localctx));
      enterOuterAlt(_localctx, 5);
      setState(639);
      functionCall();
      break;
    }

    case 6: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::NotFactorContext>(_localctx));
      enterOuterAlt(_localctx, 6);
      setState(640);
      match(CmmParser::NOT);
      setState(641);
      factor();
      break;
    }

    case 7: {
      _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<CmmParser::ParenthesizedFactorContext>(_localctx));
      enterOuterAlt(_localctx, 7);
      setState(642);
      match(CmmParser::T__1);
      setState(643);
      expression();
      setState(644);
      match(CmmParser::T__3);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableContext ------------------------------------------------------------------

CmmParser::VariableContext::VariableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::VariableIdentifierContext* CmmParser::VariableContext::variableIdentifier() {
  return getRuleContext<CmmParser::VariableIdentifierContext>(0);
}

std::vector<CmmParser::ModifierContext *> CmmParser::VariableContext::modifier() {
  return getRuleContexts<CmmParser::ModifierContext>();
}

CmmParser::ModifierContext* CmmParser::VariableContext::modifier(size_t i) {
  return getRuleContext<CmmParser::ModifierContext>(i);
}


size_t CmmParser::VariableContext::getRuleIndex() const {
  return CmmParser::RuleVariable;
}


antlrcpp::Any CmmParser::VariableContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitVariable(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::VariableContext* CmmParser::variable() {
  VariableContext *_localctx = _tracker.createInstance<VariableContext>(_ctx, getState());
  enterRule(_localctx, 152, CmmParser::RuleVariable);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(648);
    variableIdentifier();
    setState(652);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__8

    || _la == CmmParser::T__13) {
      setState(649);
      modifier();
      setState(654);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ModifierContext ------------------------------------------------------------------

CmmParser::ModifierContext::ModifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::IndexListContext* CmmParser::ModifierContext::indexList() {
  return getRuleContext<CmmParser::IndexListContext>(0);
}

CmmParser::FieldContext* CmmParser::ModifierContext::field() {
  return getRuleContext<CmmParser::FieldContext>(0);
}


size_t CmmParser::ModifierContext::getRuleIndex() const {
  return CmmParser::RuleModifier;
}


antlrcpp::Any CmmParser::ModifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitModifier(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::ModifierContext* CmmParser::modifier() {
  ModifierContext *_localctx = _tracker.createInstance<ModifierContext>(_ctx, getState());
  enterRule(_localctx, 154, CmmParser::RuleModifier);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(661);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::T__8: {
        enterOuterAlt(_localctx, 1);
        setState(655);
        match(CmmParser::T__8);
        setState(656);
        indexList();
        setState(657);
        match(CmmParser::T__9);
        break;
      }

      case CmmParser::T__13: {
        enterOuterAlt(_localctx, 2);
        setState(659);
        match(CmmParser::T__13);
        setState(660);
        field();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IndexListContext ------------------------------------------------------------------

CmmParser::IndexListContext::IndexListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<CmmParser::IndexContext *> CmmParser::IndexListContext::index() {
  return getRuleContexts<CmmParser::IndexContext>();
}

CmmParser::IndexContext* CmmParser::IndexListContext::index(size_t i) {
  return getRuleContext<CmmParser::IndexContext>(i);
}


size_t CmmParser::IndexListContext::getRuleIndex() const {
  return CmmParser::RuleIndexList;
}


antlrcpp::Any CmmParser::IndexListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitIndexList(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::IndexListContext* CmmParser::indexList() {
  IndexListContext *_localctx = _tracker.createInstance<IndexListContext>(_ctx, getState());
  enterRule(_localctx, 156, CmmParser::RuleIndexList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(663);
    index();
    setState(668);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == CmmParser::T__2) {
      setState(664);
      match(CmmParser::T__2);
      setState(665);
      index();
      setState(670);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IndexContext ------------------------------------------------------------------

CmmParser::IndexContext::IndexContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::ExpressionContext* CmmParser::IndexContext::expression() {
  return getRuleContext<CmmParser::ExpressionContext>(0);
}


size_t CmmParser::IndexContext::getRuleIndex() const {
  return CmmParser::RuleIndex;
}


antlrcpp::Any CmmParser::IndexContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitIndex(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::IndexContext* CmmParser::index() {
  IndexContext *_localctx = _tracker.createInstance<IndexContext>(_ctx, getState());
  enterRule(_localctx, 158, CmmParser::RuleIndex);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(671);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FieldContext ------------------------------------------------------------------

CmmParser::FieldContext::FieldContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::FieldContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::FieldContext::getRuleIndex() const {
  return CmmParser::RuleField;
}


antlrcpp::Any CmmParser::FieldContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitField(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FieldContext* CmmParser::field() {
  FieldContext *_localctx = _tracker.createInstance<FieldContext>(_ctx, getState());
  enterRule(_localctx, 160, CmmParser::RuleField);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(673);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionCallContext ------------------------------------------------------------------

CmmParser::FunctionCallContext::FunctionCallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::FunctionNameContext* CmmParser::FunctionCallContext::functionName() {
  return getRuleContext<CmmParser::FunctionNameContext>(0);
}

CmmParser::ArgumentListContext* CmmParser::FunctionCallContext::argumentList() {
  return getRuleContext<CmmParser::ArgumentListContext>(0);
}


size_t CmmParser::FunctionCallContext::getRuleIndex() const {
  return CmmParser::RuleFunctionCall;
}


antlrcpp::Any CmmParser::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FunctionCallContext* CmmParser::functionCall() {
  FunctionCallContext *_localctx = _tracker.createInstance<FunctionCallContext>(_ctx, getState());
  enterRule(_localctx, 162, CmmParser::RuleFunctionCall);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(675);
    functionName();
    setState(676);
    match(CmmParser::T__1);
    setState(678);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__1)
      | (1ULL << CmmParser::T__5)
      | (1ULL << CmmParser::T__6)
      | (1ULL << CmmParser::NOT)
      | (1ULL << CmmParser::IDENTIFIER)
      | (1ULL << CmmParser::INTEGER)
      | (1ULL << CmmParser::REAL))) != 0) || _la == CmmParser::CHARACTER

    || _la == CmmParser::STRING) {
      setState(677);
      argumentList();
    }
    setState(680);
    match(CmmParser::T__3);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionNameContext ------------------------------------------------------------------

CmmParser::FunctionNameContext::FunctionNameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::FunctionNameContext::IDENTIFIER() {
  return getToken(CmmParser::IDENTIFIER, 0);
}


size_t CmmParser::FunctionNameContext::getRuleIndex() const {
  return CmmParser::RuleFunctionName;
}


antlrcpp::Any CmmParser::FunctionNameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitFunctionName(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::FunctionNameContext* CmmParser::functionName() {
  FunctionNameContext *_localctx = _tracker.createInstance<FunctionNameContext>(_ctx, getState());
  enterRule(_localctx, 164, CmmParser::RuleFunctionName);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(682);
    match(CmmParser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

CmmParser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::UnsignedNumberContext* CmmParser::NumberContext::unsignedNumber() {
  return getRuleContext<CmmParser::UnsignedNumberContext>(0);
}

CmmParser::SignContext* CmmParser::NumberContext::sign() {
  return getRuleContext<CmmParser::SignContext>(0);
}


size_t CmmParser::NumberContext::getRuleIndex() const {
  return CmmParser::RuleNumber;
}


antlrcpp::Any CmmParser::NumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitNumber(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::NumberContext* CmmParser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 166, CmmParser::RuleNumber);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(685);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == CmmParser::T__5

    || _la == CmmParser::T__6) {
      setState(684);
      sign();
    }
    setState(687);
    unsignedNumber();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnsignedNumberContext ------------------------------------------------------------------

CmmParser::UnsignedNumberContext::UnsignedNumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

CmmParser::IntegerConstantContext* CmmParser::UnsignedNumberContext::integerConstant() {
  return getRuleContext<CmmParser::IntegerConstantContext>(0);
}

CmmParser::RealConstantContext* CmmParser::UnsignedNumberContext::realConstant() {
  return getRuleContext<CmmParser::RealConstantContext>(0);
}


size_t CmmParser::UnsignedNumberContext::getRuleIndex() const {
  return CmmParser::RuleUnsignedNumber;
}


antlrcpp::Any CmmParser::UnsignedNumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitUnsignedNumber(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::UnsignedNumberContext* CmmParser::unsignedNumber() {
  UnsignedNumberContext *_localctx = _tracker.createInstance<UnsignedNumberContext>(_ctx, getState());
  enterRule(_localctx, 168, CmmParser::RuleUnsignedNumber);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(691);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case CmmParser::INTEGER: {
        enterOuterAlt(_localctx, 1);
        setState(689);
        integerConstant();
        break;
      }

      case CmmParser::REAL: {
        enterOuterAlt(_localctx, 2);
        setState(690);
        realConstant();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IntegerConstantContext ------------------------------------------------------------------

CmmParser::IntegerConstantContext::IntegerConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::IntegerConstantContext::INTEGER() {
  return getToken(CmmParser::INTEGER, 0);
}


size_t CmmParser::IntegerConstantContext::getRuleIndex() const {
  return CmmParser::RuleIntegerConstant;
}


antlrcpp::Any CmmParser::IntegerConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitIntegerConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::IntegerConstantContext* CmmParser::integerConstant() {
  IntegerConstantContext *_localctx = _tracker.createInstance<IntegerConstantContext>(_ctx, getState());
  enterRule(_localctx, 170, CmmParser::RuleIntegerConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(693);
    match(CmmParser::INTEGER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RealConstantContext ------------------------------------------------------------------

CmmParser::RealConstantContext::RealConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::RealConstantContext::REAL() {
  return getToken(CmmParser::REAL, 0);
}


size_t CmmParser::RealConstantContext::getRuleIndex() const {
  return CmmParser::RuleRealConstant;
}


antlrcpp::Any CmmParser::RealConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRealConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RealConstantContext* CmmParser::realConstant() {
  RealConstantContext *_localctx = _tracker.createInstance<RealConstantContext>(_ctx, getState());
  enterRule(_localctx, 172, CmmParser::RuleRealConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(695);
    match(CmmParser::REAL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CharacterConstantContext ------------------------------------------------------------------

CmmParser::CharacterConstantContext::CharacterConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::CharacterConstantContext::CHARACTER() {
  return getToken(CmmParser::CHARACTER, 0);
}


size_t CmmParser::CharacterConstantContext::getRuleIndex() const {
  return CmmParser::RuleCharacterConstant;
}


antlrcpp::Any CmmParser::CharacterConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitCharacterConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::CharacterConstantContext* CmmParser::characterConstant() {
  CharacterConstantContext *_localctx = _tracker.createInstance<CharacterConstantContext>(_ctx, getState());
  enterRule(_localctx, 174, CmmParser::RuleCharacterConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(697);
    match(CmmParser::CHARACTER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringConstantContext ------------------------------------------------------------------

CmmParser::StringConstantContext::StringConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::StringConstantContext::STRING() {
  return getToken(CmmParser::STRING, 0);
}


size_t CmmParser::StringConstantContext::getRuleIndex() const {
  return CmmParser::RuleStringConstant;
}


antlrcpp::Any CmmParser::StringConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitStringConstant(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::StringConstantContext* CmmParser::stringConstant() {
  StringConstantContext *_localctx = _tracker.createInstance<StringConstantContext>(_ctx, getState());
  enterRule(_localctx, 176, CmmParser::RuleStringConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(699);
    match(CmmParser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RelOpContext ------------------------------------------------------------------

CmmParser::RelOpContext::RelOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t CmmParser::RelOpContext::getRuleIndex() const {
  return CmmParser::RuleRelOp;
}


antlrcpp::Any CmmParser::RelOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitRelOp(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::RelOpContext* CmmParser::relOp() {
  RelOpContext *_localctx = _tracker.createInstance<RelOpContext>(_ctx, getState());
  enterRule(_localctx, 178, CmmParser::RuleRelOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(701);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__4)
      | (1ULL << CmmParser::T__14)
      | (1ULL << CmmParser::T__15)
      | (1ULL << CmmParser::T__16)
      | (1ULL << CmmParser::T__17)
      | (1ULL << CmmParser::T__18))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddOpContext ------------------------------------------------------------------

CmmParser::AddOpContext::AddOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::AddOpContext::OR() {
  return getToken(CmmParser::OR, 0);
}


size_t CmmParser::AddOpContext::getRuleIndex() const {
  return CmmParser::RuleAddOp;
}


antlrcpp::Any CmmParser::AddOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitAddOp(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::AddOpContext* CmmParser::addOp() {
  AddOpContext *_localctx = _tracker.createInstance<AddOpContext>(_ctx, getState());
  enterRule(_localctx, 180, CmmParser::RuleAddOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(703);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__5)
      | (1ULL << CmmParser::T__6)
      | (1ULL << CmmParser::OR))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MulOpContext ------------------------------------------------------------------

CmmParser::MulOpContext::MulOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* CmmParser::MulOpContext::DIV() {
  return getToken(CmmParser::DIV, 0);
}

tree::TerminalNode* CmmParser::MulOpContext::MOD() {
  return getToken(CmmParser::MOD, 0);
}

tree::TerminalNode* CmmParser::MulOpContext::AND() {
  return getToken(CmmParser::AND, 0);
}


size_t CmmParser::MulOpContext::getRuleIndex() const {
  return CmmParser::RuleMulOp;
}


antlrcpp::Any CmmParser::MulOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<CmmVisitor*>(visitor))
    return parserVisitor->visitMulOp(this);
  else
    return visitor->visitChildren(this);
}

CmmParser::MulOpContext* CmmParser::mulOp() {
  MulOpContext *_localctx = _tracker.createInstance<MulOpContext>(_ctx, getState());
  enterRule(_localctx, 182, CmmParser::RuleMulOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(705);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << CmmParser::T__19)
      | (1ULL << CmmParser::T__20)
      | (1ULL << CmmParser::DIV)
      | (1ULL << CmmParser::MOD)
      | (1ULL << CmmParser::AND))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> CmmParser::_decisionToDFA;
atn::PredictionContextCache CmmParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN CmmParser::_atn;
std::vector<uint16_t> CmmParser::_serializedATN;

std::vector<std::string> CmmParser::_ruleNames = {
  "program", "programHeader", "programParameters", "programIdentifier", 
  "block", "declarations", "constantsPart", "constantDefinitionsList", "constantDefinition", 
  "constantIdentifier", "constant", "sign", "typesPart", "typeDefinitionsList", 
  "typeDefinition", "typeIdentifier", "typeSpecification", "simpleType", 
  "enumerationType", "enumerationConstant", "subrangeType", "arrayType", 
  "arrayDimensionList", "recordType", "recordFields", "variablesPart", "variableDeclarationsList", 
  "variableDeclarations", "variableIdentifierList", "variableIdentifier", 
  "routinesPart", "routineDefinition", "procedureHead", "functionHead", 
  "routineIdentifier", "parameters", "parameterDeclarationsList", "parameterDeclarations", 
  "parameterIdentifierList", "parameterIdentifier", "statement", "compoundStatement", 
  "emptyStatement", "statementList", "assignmentStatement", "lhs", "rhs", 
  "ifStatement", "trueStatement", "falseStatement", "returnStatement", "caseStatement", 
  "caseBranchList", "caseBranch", "caseConstantList", "caseConstant", "repeatStatement", 
  "whileStatement", "forStatement", "procedureCallStatement", "procedureName", 
  "argumentList", "argument", "writeStatement", "writelnStatement", "writeArguments", 
  "writeArgument", "fieldWidth", "decimalPlaces", "readStatement", "readlnStatement", 
  "readArguments", "expression", "simpleExpression", "term", "factor", "variable", 
  "modifier", "indexList", "index", "field", "functionCall", "functionName", 
  "number", "unsignedNumber", "integerConstant", "realConstant", "characterConstant", 
  "stringConstant", "relOp", "addOp", "mulOp"
};

std::vector<std::string> CmmParser::_literalNames = {
  "", "';'", "'('", "','", "')'", "'='", "'-'", "'+'", "'..'", "'['", "']'", 
  "'++'", "'--'", "':'", "'.'", "'!='", "'<'", "'<='", "'>'", "'>='", "'*'", 
  "'/'", "", "", "", "", "", "", "", "", "", "'{'", "'}'", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "'''"
};

std::vector<std::string> CmmParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "PROGRAM", "PROJECT", "CONST", "TYPE", "TYPEDEF", "ARRAY", 
  "OF", "RECORD", "VAR", "BEGIN", "END", "DIV", "MOD", "AND", "OR", "NOT", 
  "IF", "THEN", "ELSE", "SWITCH", "CASE", "REPEAT", "UNTIL", "WHILE", "DO", 
  "FOR", "TO", "DOWNTO", "WRITE", "WRITELN", "PRINTF", "PRINTLN", "READ", 
  "READLN", "PROCEDURE", "FUNCTION", "VOID", "RETURN", "IDENTIFIER", "INTEGER", 
  "REAL", "NEWLINE", "WS", "QUOTE", "CHARACTER", "STRING", "COMMENT"
};

dfa::Vocabulary CmmParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> CmmParser::_tokenNames;

CmmParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x46, 0x2c6, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x4, 0x2e, 0x9, 0x2e, 0x4, 0x2f, 
    0x9, 0x2f, 0x4, 0x30, 0x9, 0x30, 0x4, 0x31, 0x9, 0x31, 0x4, 0x32, 0x9, 
    0x32, 0x4, 0x33, 0x9, 0x33, 0x4, 0x34, 0x9, 0x34, 0x4, 0x35, 0x9, 0x35, 
    0x4, 0x36, 0x9, 0x36, 0x4, 0x37, 0x9, 0x37, 0x4, 0x38, 0x9, 0x38, 0x4, 
    0x39, 0x9, 0x39, 0x4, 0x3a, 0x9, 0x3a, 0x4, 0x3b, 0x9, 0x3b, 0x4, 0x3c, 
    0x9, 0x3c, 0x4, 0x3d, 0x9, 0x3d, 0x4, 0x3e, 0x9, 0x3e, 0x4, 0x3f, 0x9, 
    0x3f, 0x4, 0x40, 0x9, 0x40, 0x4, 0x41, 0x9, 0x41, 0x4, 0x42, 0x9, 0x42, 
    0x4, 0x43, 0x9, 0x43, 0x4, 0x44, 0x9, 0x44, 0x4, 0x45, 0x9, 0x45, 0x4, 
    0x46, 0x9, 0x46, 0x4, 0x47, 0x9, 0x47, 0x4, 0x48, 0x9, 0x48, 0x4, 0x49, 
    0x9, 0x49, 0x4, 0x4a, 0x9, 0x4a, 0x4, 0x4b, 0x9, 0x4b, 0x4, 0x4c, 0x9, 
    0x4c, 0x4, 0x4d, 0x9, 0x4d, 0x4, 0x4e, 0x9, 0x4e, 0x4, 0x4f, 0x9, 0x4f, 
    0x4, 0x50, 0x9, 0x50, 0x4, 0x51, 0x9, 0x51, 0x4, 0x52, 0x9, 0x52, 0x4, 
    0x53, 0x9, 0x53, 0x4, 0x54, 0x9, 0x54, 0x4, 0x55, 0x9, 0x55, 0x4, 0x56, 
    0x9, 0x56, 0x4, 0x57, 0x9, 0x57, 0x4, 0x58, 0x9, 0x58, 0x4, 0x59, 0x9, 
    0x59, 0x4, 0x5a, 0x9, 0x5a, 0x4, 0x5b, 0x9, 0x5b, 0x4, 0x5c, 0x9, 0x5c, 
    0x4, 0x5d, 0x9, 0x5d, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 
    0x3, 0x3, 0x5, 0x3, 0xc1, 0xa, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x7, 0x4, 0xc9, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 
    0xcc, 0xb, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x5, 0x7, 0xd8, 0xa, 0x7, 
    0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x5, 0x7, 0xdd, 0xa, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x5, 0x7, 0xe2, 0xa, 0x7, 0x3, 0x7, 0x5, 0x7, 0xe5, 0xa, 
    0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x7, 
    0x9, 0xed, 0xa, 0x9, 0xc, 0x9, 0xe, 0x9, 0xf0, 0xb, 0x9, 0x3, 0xa, 0x3, 
    0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xc, 0x5, 0xc, 0xf9, 
    0xa, 0xc, 0x3, 0xc, 0x3, 0xc, 0x5, 0xc, 0xfd, 0xa, 0xc, 0x3, 0xc, 0x3, 
    0xc, 0x5, 0xc, 0x101, 0xa, 0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 
    0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x7, 0xf, 0x10a, 0xa, 0xf, 0xc, 0xf, 0xe, 
    0xf, 0x10d, 0xb, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 
    0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x5, 0x12, 
    0x119, 0xa, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 0x13, 0x11e, 
    0xa, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x7, 0x14, 0x124, 
    0xa, 0x14, 0xc, 0x14, 0xe, 0x14, 0x127, 0xb, 0x14, 0x3, 0x14, 0x3, 0x14, 
    0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 
    0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 
    0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x7, 0x18, 0x13b, 0xa, 0x18, 0xc, 0x18, 
    0xe, 0x18, 0x13e, 0xb, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x5, 0x19, 
    0x143, 0xa, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 
    0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x7, 0x1c, 0x14e, 0xa, 0x1c, 
    0xc, 0x1c, 0xe, 0x1c, 0x151, 0xb, 0x1c, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 
    0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1e, 0x7, 0x1e, 0x159, 0xa, 0x1e, 0xc, 0x1e, 
    0xe, 0x1e, 0x15c, 0xb, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x20, 0x3, 0x20, 
    0x7, 0x20, 0x162, 0xa, 0x20, 0xc, 0x20, 0xe, 0x20, 0x165, 0xb, 0x20, 
    0x3, 0x21, 0x3, 0x21, 0x5, 0x21, 0x169, 0xa, 0x21, 0x3, 0x21, 0x3, 0x21, 
    0x3, 0x21, 0x3, 0x21, 0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x5, 
    0x22, 0x173, 0xa, 0x22, 0x3, 0x22, 0x3, 0x22, 0x3, 0x23, 0x3, 0x23, 
    0x3, 0x23, 0x3, 0x23, 0x5, 0x23, 0x17b, 0xa, 0x23, 0x3, 0x23, 0x3, 0x23, 
    0x3, 0x24, 0x3, 0x24, 0x3, 0x25, 0x3, 0x25, 0x3, 0x26, 0x3, 0x26, 0x3, 
    0x26, 0x7, 0x26, 0x186, 0xa, 0x26, 0xc, 0x26, 0xe, 0x26, 0x189, 0xb, 
    0x26, 0x3, 0x27, 0x5, 0x27, 0x18c, 0xa, 0x27, 0x3, 0x27, 0x3, 0x27, 
    0x3, 0x27, 0x3, 0x28, 0x3, 0x28, 0x3, 0x28, 0x7, 0x28, 0x194, 0xa, 0x28, 
    0xc, 0x28, 0xe, 0x28, 0x197, 0xb, 0x28, 0x3, 0x29, 0x3, 0x29, 0x3, 0x2a, 
    0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 
    0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 0x3, 0x2a, 
    0x5, 0x2a, 0x1a9, 0xa, 0x2a, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 
    0x3, 0x2c, 0x3, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 0x7, 0x2d, 0x1b4, 
    0xa, 0x2d, 0xc, 0x2d, 0xe, 0x2d, 0x1b7, 0xb, 0x2d, 0x3, 0x2e, 0x3, 0x2e, 
    0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 
    0x2e, 0x5, 0x2e, 0x1c2, 0xa, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 0x3, 0x2e, 
    0x3, 0x2e, 0x3, 0x2e, 0x5, 0x2e, 0x1c9, 0xa, 0x2e, 0x5, 0x2e, 0x1cb, 
    0xa, 0x2e, 0x3, 0x2f, 0x3, 0x2f, 0x3, 0x30, 0x3, 0x30, 0x3, 0x31, 0x3, 
    0x31, 0x3, 0x31, 0x3, 0x31, 0x3, 0x31, 0x3, 0x31, 0x3, 0x31, 0x5, 0x31, 
    0x1d8, 0xa, 0x31, 0x3, 0x32, 0x3, 0x32, 0x3, 0x33, 0x3, 0x33, 0x3, 0x34, 
    0x3, 0x34, 0x5, 0x34, 0x1e0, 0xa, 0x34, 0x3, 0x35, 0x3, 0x35, 0x3, 0x35, 
    0x3, 0x35, 0x3, 0x35, 0x3, 0x35, 0x3, 0x35, 0x3, 0x35, 0x3, 0x36, 0x3, 
    0x36, 0x3, 0x36, 0x7, 0x36, 0x1ed, 0xa, 0x36, 0xc, 0x36, 0xe, 0x36, 
    0x1f0, 0xb, 0x36, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 0x3, 0x37, 
    0x5, 0x37, 0x1f7, 0xa, 0x37, 0x3, 0x38, 0x3, 0x38, 0x3, 0x38, 0x3, 0x38, 
    0x3, 0x38, 0x7, 0x38, 0x1fe, 0xa, 0x38, 0xc, 0x38, 0xe, 0x38, 0x201, 
    0xb, 0x38, 0x3, 0x39, 0x3, 0x39, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 
    0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3a, 0x3, 0x3b, 
    0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3b, 0x3, 0x3c, 0x3, 
    0x3c, 0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3c, 
    0x3, 0x3c, 0x3, 0x3c, 0x3, 0x3d, 0x3, 0x3d, 0x3, 0x3d, 0x5, 0x3d, 0x221, 
    0xa, 0x3d, 0x3, 0x3d, 0x3, 0x3d, 0x3, 0x3e, 0x3, 0x3e, 0x3, 0x3f, 0x3, 
    0x3f, 0x3, 0x3f, 0x7, 0x3f, 0x22a, 0xa, 0x3f, 0xc, 0x3f, 0xe, 0x3f, 
    0x22d, 0xb, 0x3f, 0x3, 0x40, 0x3, 0x40, 0x3, 0x41, 0x3, 0x41, 0x3, 0x41, 
    0x3, 0x42, 0x3, 0x42, 0x5, 0x42, 0x236, 0xa, 0x42, 0x3, 0x43, 0x3, 0x43, 
    0x3, 0x43, 0x3, 0x43, 0x7, 0x43, 0x23c, 0xa, 0x43, 0xc, 0x43, 0xe, 0x43, 
    0x23f, 0xb, 0x43, 0x3, 0x43, 0x3, 0x43, 0x3, 0x44, 0x3, 0x44, 0x3, 0x44, 
    0x5, 0x44, 0x246, 0xa, 0x44, 0x3, 0x45, 0x5, 0x45, 0x249, 0xa, 0x45, 
    0x3, 0x45, 0x3, 0x45, 0x3, 0x45, 0x5, 0x45, 0x24e, 0xa, 0x45, 0x3, 0x46, 
    0x3, 0x46, 0x3, 0x47, 0x3, 0x47, 0x3, 0x47, 0x3, 0x48, 0x3, 0x48, 0x3, 
    0x48, 0x3, 0x49, 0x3, 0x49, 0x3, 0x49, 0x3, 0x49, 0x7, 0x49, 0x25c, 
    0xa, 0x49, 0xc, 0x49, 0xe, 0x49, 0x25f, 0xb, 0x49, 0x3, 0x49, 0x3, 0x49, 
    0x3, 0x4a, 0x3, 0x4a, 0x3, 0x4a, 0x3, 0x4a, 0x5, 0x4a, 0x267, 0xa, 0x4a, 
    0x3, 0x4b, 0x5, 0x4b, 0x26a, 0xa, 0x4b, 0x3, 0x4b, 0x3, 0x4b, 0x3, 0x4b, 
    0x3, 0x4b, 0x7, 0x4b, 0x270, 0xa, 0x4b, 0xc, 0x4b, 0xe, 0x4b, 0x273, 
    0xb, 0x4b, 0x3, 0x4c, 0x3, 0x4c, 0x3, 0x4c, 0x3, 0x4c, 0x7, 0x4c, 0x279, 
    0xa, 0x4c, 0xc, 0x4c, 0xe, 0x4c, 0x27c, 0xb, 0x4c, 0x3, 0x4d, 0x3, 0x4d, 
    0x3, 0x4d, 0x3, 0x4d, 0x3, 0x4d, 0x3, 0x4d, 0x3, 0x4d, 0x3, 0x4d, 0x3, 
    0x4d, 0x3, 0x4d, 0x3, 0x4d, 0x5, 0x4d, 0x289, 0xa, 0x4d, 0x3, 0x4e, 
    0x3, 0x4e, 0x7, 0x4e, 0x28d, 0xa, 0x4e, 0xc, 0x4e, 0xe, 0x4e, 0x290, 
    0xb, 0x4e, 0x3, 0x4f, 0x3, 0x4f, 0x3, 0x4f, 0x3, 0x4f, 0x3, 0x4f, 0x3, 
    0x4f, 0x5, 0x4f, 0x298, 0xa, 0x4f, 0x3, 0x50, 0x3, 0x50, 0x3, 0x50, 
    0x7, 0x50, 0x29d, 0xa, 0x50, 0xc, 0x50, 0xe, 0x50, 0x2a0, 0xb, 0x50, 
    0x3, 0x51, 0x3, 0x51, 0x3, 0x52, 0x3, 0x52, 0x3, 0x53, 0x3, 0x53, 0x3, 
    0x53, 0x5, 0x53, 0x2a9, 0xa, 0x53, 0x3, 0x53, 0x3, 0x53, 0x3, 0x54, 
    0x3, 0x54, 0x3, 0x55, 0x5, 0x55, 0x2b0, 0xa, 0x55, 0x3, 0x55, 0x3, 0x55, 
    0x3, 0x56, 0x3, 0x56, 0x5, 0x56, 0x2b6, 0xa, 0x56, 0x3, 0x57, 0x3, 0x57, 
    0x3, 0x58, 0x3, 0x58, 0x3, 0x59, 0x3, 0x59, 0x3, 0x5a, 0x3, 0x5a, 0x3, 
    0x5b, 0x3, 0x5b, 0x3, 0x5c, 0x3, 0x5c, 0x3, 0x5d, 0x3, 0x5d, 0x3, 0x5d, 
    0x2, 0x2, 0x5e, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 
    0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 
    0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 
    0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x5a, 0x5c, 
    0x5e, 0x60, 0x62, 0x64, 0x66, 0x68, 0x6a, 0x6c, 0x6e, 0x70, 0x72, 0x74, 
    0x76, 0x78, 0x7a, 0x7c, 0x7e, 0x80, 0x82, 0x84, 0x86, 0x88, 0x8a, 0x8c, 
    0x8e, 0x90, 0x92, 0x94, 0x96, 0x98, 0x9a, 0x9c, 0x9e, 0xa0, 0xa2, 0xa4, 
    0xa6, 0xa8, 0xaa, 0xac, 0xae, 0xb0, 0xb2, 0xb4, 0xb6, 0xb8, 0x2, 0x6, 
    0x3, 0x2, 0x8, 0x9, 0x4, 0x2, 0x7, 0x7, 0x11, 0x15, 0x4, 0x2, 0x8, 0x9, 
    0x26, 0x26, 0x4, 0x2, 0x16, 0x17, 0x23, 0x25, 0x2, 0x2b4, 0x2, 0xba, 
    0x3, 0x2, 0x2, 0x2, 0x4, 0xbd, 0x3, 0x2, 0x2, 0x2, 0x6, 0xc4, 0x3, 0x2, 
    0x2, 0x2, 0x8, 0xcf, 0x3, 0x2, 0x2, 0x2, 0xa, 0xd1, 0x3, 0x2, 0x2, 0x2, 
    0xc, 0xd7, 0x3, 0x2, 0x2, 0x2, 0xe, 0xe6, 0x3, 0x2, 0x2, 0x2, 0x10, 
    0xe9, 0x3, 0x2, 0x2, 0x2, 0x12, 0xf1, 0x3, 0x2, 0x2, 0x2, 0x14, 0xf5, 
    0x3, 0x2, 0x2, 0x2, 0x16, 0x100, 0x3, 0x2, 0x2, 0x2, 0x18, 0x102, 0x3, 
    0x2, 0x2, 0x2, 0x1a, 0x104, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x106, 0x3, 0x2, 
    0x2, 0x2, 0x1e, 0x10e, 0x3, 0x2, 0x2, 0x2, 0x20, 0x113, 0x3, 0x2, 0x2, 
    0x2, 0x22, 0x118, 0x3, 0x2, 0x2, 0x2, 0x24, 0x11d, 0x3, 0x2, 0x2, 0x2, 
    0x26, 0x11f, 0x3, 0x2, 0x2, 0x2, 0x28, 0x12a, 0x3, 0x2, 0x2, 0x2, 0x2a, 
    0x12c, 0x3, 0x2, 0x2, 0x2, 0x2c, 0x130, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x137, 
    0x3, 0x2, 0x2, 0x2, 0x30, 0x13f, 0x3, 0x2, 0x2, 0x2, 0x32, 0x146, 0x3, 
    0x2, 0x2, 0x2, 0x34, 0x148, 0x3, 0x2, 0x2, 0x2, 0x36, 0x14a, 0x3, 0x2, 
    0x2, 0x2, 0x38, 0x152, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x155, 0x3, 0x2, 0x2, 
    0x2, 0x3c, 0x15d, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x15f, 0x3, 0x2, 0x2, 0x2, 
    0x40, 0x168, 0x3, 0x2, 0x2, 0x2, 0x42, 0x16e, 0x3, 0x2, 0x2, 0x2, 0x44, 
    0x176, 0x3, 0x2, 0x2, 0x2, 0x46, 0x17e, 0x3, 0x2, 0x2, 0x2, 0x48, 0x180, 
    0x3, 0x2, 0x2, 0x2, 0x4a, 0x182, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x18b, 0x3, 
    0x2, 0x2, 0x2, 0x4e, 0x190, 0x3, 0x2, 0x2, 0x2, 0x50, 0x198, 0x3, 0x2, 
    0x2, 0x2, 0x52, 0x1a8, 0x3, 0x2, 0x2, 0x2, 0x54, 0x1aa, 0x3, 0x2, 0x2, 
    0x2, 0x56, 0x1ae, 0x3, 0x2, 0x2, 0x2, 0x58, 0x1b0, 0x3, 0x2, 0x2, 0x2, 
    0x5a, 0x1ca, 0x3, 0x2, 0x2, 0x2, 0x5c, 0x1cc, 0x3, 0x2, 0x2, 0x2, 0x5e, 
    0x1ce, 0x3, 0x2, 0x2, 0x2, 0x60, 0x1d0, 0x3, 0x2, 0x2, 0x2, 0x62, 0x1d9, 
    0x3, 0x2, 0x2, 0x2, 0x64, 0x1db, 0x3, 0x2, 0x2, 0x2, 0x66, 0x1dd, 0x3, 
    0x2, 0x2, 0x2, 0x68, 0x1e1, 0x3, 0x2, 0x2, 0x2, 0x6a, 0x1e9, 0x3, 0x2, 
    0x2, 0x2, 0x6c, 0x1f6, 0x3, 0x2, 0x2, 0x2, 0x6e, 0x1f8, 0x3, 0x2, 0x2, 
    0x2, 0x70, 0x202, 0x3, 0x2, 0x2, 0x2, 0x72, 0x204, 0x3, 0x2, 0x2, 0x2, 
    0x74, 0x20d, 0x3, 0x2, 0x2, 0x2, 0x76, 0x213, 0x3, 0x2, 0x2, 0x2, 0x78, 
    0x21d, 0x3, 0x2, 0x2, 0x2, 0x7a, 0x224, 0x3, 0x2, 0x2, 0x2, 0x7c, 0x226, 
    0x3, 0x2, 0x2, 0x2, 0x7e, 0x22e, 0x3, 0x2, 0x2, 0x2, 0x80, 0x230, 0x3, 
    0x2, 0x2, 0x2, 0x82, 0x233, 0x3, 0x2, 0x2, 0x2, 0x84, 0x237, 0x3, 0x2, 
    0x2, 0x2, 0x86, 0x242, 0x3, 0x2, 0x2, 0x2, 0x88, 0x248, 0x3, 0x2, 0x2, 
    0x2, 0x8a, 0x24f, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x251, 0x3, 0x2, 0x2, 0x2, 
    0x8e, 0x254, 0x3, 0x2, 0x2, 0x2, 0x90, 0x257, 0x3, 0x2, 0x2, 0x2, 0x92, 
    0x262, 0x3, 0x2, 0x2, 0x2, 0x94, 0x269, 0x3, 0x2, 0x2, 0x2, 0x96, 0x274, 
    0x3, 0x2, 0x2, 0x2, 0x98, 0x288, 0x3, 0x2, 0x2, 0x2, 0x9a, 0x28a, 0x3, 
    0x2, 0x2, 0x2, 0x9c, 0x297, 0x3, 0x2, 0x2, 0x2, 0x9e, 0x299, 0x3, 0x2, 
    0x2, 0x2, 0xa0, 0x2a1, 0x3, 0x2, 0x2, 0x2, 0xa2, 0x2a3, 0x3, 0x2, 0x2, 
    0x2, 0xa4, 0x2a5, 0x3, 0x2, 0x2, 0x2, 0xa6, 0x2ac, 0x3, 0x2, 0x2, 0x2, 
    0xa8, 0x2af, 0x3, 0x2, 0x2, 0x2, 0xaa, 0x2b5, 0x3, 0x2, 0x2, 0x2, 0xac, 
    0x2b7, 0x3, 0x2, 0x2, 0x2, 0xae, 0x2b9, 0x3, 0x2, 0x2, 0x2, 0xb0, 0x2bb, 
    0x3, 0x2, 0x2, 0x2, 0xb2, 0x2bd, 0x3, 0x2, 0x2, 0x2, 0xb4, 0x2bf, 0x3, 
    0x2, 0x2, 0x2, 0xb6, 0x2c1, 0x3, 0x2, 0x2, 0x2, 0xb8, 0x2c3, 0x3, 0x2, 
    0x2, 0x2, 0xba, 0xbb, 0x5, 0x4, 0x3, 0x2, 0xbb, 0xbc, 0x5, 0xc, 0x7, 
    0x2, 0xbc, 0x3, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xbe, 0x7, 0x19, 0x2, 0x2, 
    0xbe, 0xc0, 0x5, 0x8, 0x5, 0x2, 0xbf, 0xc1, 0x5, 0x6, 0x4, 0x2, 0xc0, 
    0xbf, 0x3, 0x2, 0x2, 0x2, 0xc0, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xc1, 0xc2, 
    0x3, 0x2, 0x2, 0x2, 0xc2, 0xc3, 0x7, 0x3, 0x2, 0x2, 0xc3, 0x5, 0x3, 
    0x2, 0x2, 0x2, 0xc4, 0xc5, 0x7, 0x4, 0x2, 0x2, 0xc5, 0xca, 0x7, 0x3e, 
    0x2, 0x2, 0xc6, 0xc7, 0x7, 0x5, 0x2, 0x2, 0xc7, 0xc9, 0x7, 0x3e, 0x2, 
    0x2, 0xc8, 0xc6, 0x3, 0x2, 0x2, 0x2, 0xc9, 0xcc, 0x3, 0x2, 0x2, 0x2, 
    0xca, 0xc8, 0x3, 0x2, 0x2, 0x2, 0xca, 0xcb, 0x3, 0x2, 0x2, 0x2, 0xcb, 
    0xcd, 0x3, 0x2, 0x2, 0x2, 0xcc, 0xca, 0x3, 0x2, 0x2, 0x2, 0xcd, 0xce, 
    0x7, 0x6, 0x2, 0x2, 0xce, 0x7, 0x3, 0x2, 0x2, 0x2, 0xcf, 0xd0, 0x7, 
    0x3e, 0x2, 0x2, 0xd0, 0x9, 0x3, 0x2, 0x2, 0x2, 0xd1, 0xd2, 0x5, 0xc, 
    0x7, 0x2, 0xd2, 0xd3, 0x5, 0x58, 0x2d, 0x2, 0xd3, 0xb, 0x3, 0x2, 0x2, 
    0x2, 0xd4, 0xd5, 0x5, 0xe, 0x8, 0x2, 0xd5, 0xd6, 0x7, 0x3, 0x2, 0x2, 
    0xd6, 0xd8, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xd4, 0x3, 0x2, 0x2, 0x2, 0xd7, 
    0xd8, 0x3, 0x2, 0x2, 0x2, 0xd8, 0xdc, 0x3, 0x2, 0x2, 0x2, 0xd9, 0xda, 
    0x5, 0x1a, 0xe, 0x2, 0xda, 0xdb, 0x7, 0x3, 0x2, 0x2, 0xdb, 0xdd, 0x3, 
    0x2, 0x2, 0x2, 0xdc, 0xd9, 0x3, 0x2, 0x2, 0x2, 0xdc, 0xdd, 0x3, 0x2, 
    0x2, 0x2, 0xdd, 0xe1, 0x3, 0x2, 0x2, 0x2, 0xde, 0xdf, 0x5, 0x34, 0x1b, 
    0x2, 0xdf, 0xe0, 0x7, 0x3, 0x2, 0x2, 0xe0, 0xe2, 0x3, 0x2, 0x2, 0x2, 
    0xe1, 0xde, 0x3, 0x2, 0x2, 0x2, 0xe1, 0xe2, 0x3, 0x2, 0x2, 0x2, 0xe2, 
    0xe4, 0x3, 0x2, 0x2, 0x2, 0xe3, 0xe5, 0x5, 0x3e, 0x20, 0x2, 0xe4, 0xe3, 
    0x3, 0x2, 0x2, 0x2, 0xe4, 0xe5, 0x3, 0x2, 0x2, 0x2, 0xe5, 0xd, 0x3, 
    0x2, 0x2, 0x2, 0xe6, 0xe7, 0x7, 0x1a, 0x2, 0x2, 0xe7, 0xe8, 0x5, 0x10, 
    0x9, 0x2, 0xe8, 0xf, 0x3, 0x2, 0x2, 0x2, 0xe9, 0xee, 0x5, 0x12, 0xa, 
    0x2, 0xea, 0xeb, 0x7, 0x3, 0x2, 0x2, 0xeb, 0xed, 0x5, 0x12, 0xa, 0x2, 
    0xec, 0xea, 0x3, 0x2, 0x2, 0x2, 0xed, 0xf0, 0x3, 0x2, 0x2, 0x2, 0xee, 
    0xec, 0x3, 0x2, 0x2, 0x2, 0xee, 0xef, 0x3, 0x2, 0x2, 0x2, 0xef, 0x11, 
    0x3, 0x2, 0x2, 0x2, 0xf0, 0xee, 0x3, 0x2, 0x2, 0x2, 0xf1, 0xf2, 0x5, 
    0x14, 0xb, 0x2, 0xf2, 0xf3, 0x7, 0x7, 0x2, 0x2, 0xf3, 0xf4, 0x5, 0x16, 
    0xc, 0x2, 0xf4, 0x13, 0x3, 0x2, 0x2, 0x2, 0xf5, 0xf6, 0x7, 0x3e, 0x2, 
    0x2, 0xf6, 0x15, 0x3, 0x2, 0x2, 0x2, 0xf7, 0xf9, 0x5, 0x18, 0xd, 0x2, 
    0xf8, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xf9, 0x3, 0x2, 0x2, 0x2, 0xf9, 
    0xfc, 0x3, 0x2, 0x2, 0x2, 0xfa, 0xfd, 0x7, 0x3e, 0x2, 0x2, 0xfb, 0xfd, 
    0x5, 0xaa, 0x56, 0x2, 0xfc, 0xfa, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfb, 0x3, 
    0x2, 0x2, 0x2, 0xfd, 0x101, 0x3, 0x2, 0x2, 0x2, 0xfe, 0x101, 0x5, 0xb0, 
    0x59, 0x2, 0xff, 0x101, 0x5, 0xb2, 0x5a, 0x2, 0x100, 0xf8, 0x3, 0x2, 
    0x2, 0x2, 0x100, 0xfe, 0x3, 0x2, 0x2, 0x2, 0x100, 0xff, 0x3, 0x2, 0x2, 
    0x2, 0x101, 0x17, 0x3, 0x2, 0x2, 0x2, 0x102, 0x103, 0x9, 0x2, 0x2, 0x2, 
    0x103, 0x19, 0x3, 0x2, 0x2, 0x2, 0x104, 0x105, 0x5, 0x1c, 0xf, 0x2, 
    0x105, 0x1b, 0x3, 0x2, 0x2, 0x2, 0x106, 0x10b, 0x5, 0x1e, 0x10, 0x2, 
    0x107, 0x108, 0x7, 0x3, 0x2, 0x2, 0x108, 0x10a, 0x5, 0x1e, 0x10, 0x2, 
    0x109, 0x107, 0x3, 0x2, 0x2, 0x2, 0x10a, 0x10d, 0x3, 0x2, 0x2, 0x2, 
    0x10b, 0x109, 0x3, 0x2, 0x2, 0x2, 0x10b, 0x10c, 0x3, 0x2, 0x2, 0x2, 
    0x10c, 0x1d, 0x3, 0x2, 0x2, 0x2, 0x10d, 0x10b, 0x3, 0x2, 0x2, 0x2, 0x10e, 
    0x10f, 0x7, 0x1c, 0x2, 0x2, 0x10f, 0x110, 0x5, 0x20, 0x11, 0x2, 0x110, 
    0x111, 0x7, 0x7, 0x2, 0x2, 0x111, 0x112, 0x5, 0x22, 0x12, 0x2, 0x112, 
    0x1f, 0x3, 0x2, 0x2, 0x2, 0x113, 0x114, 0x7, 0x3e, 0x2, 0x2, 0x114, 
    0x21, 0x3, 0x2, 0x2, 0x2, 0x115, 0x119, 0x5, 0x24, 0x13, 0x2, 0x116, 
    0x119, 0x5, 0x2c, 0x17, 0x2, 0x117, 0x119, 0x5, 0x30, 0x19, 0x2, 0x118, 
    0x115, 0x3, 0x2, 0x2, 0x2, 0x118, 0x116, 0x3, 0x2, 0x2, 0x2, 0x118, 
    0x117, 0x3, 0x2, 0x2, 0x2, 0x119, 0x23, 0x3, 0x2, 0x2, 0x2, 0x11a, 0x11e, 
    0x5, 0x20, 0x11, 0x2, 0x11b, 0x11e, 0x5, 0x26, 0x14, 0x2, 0x11c, 0x11e, 
    0x5, 0x2a, 0x16, 0x2, 0x11d, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x11d, 0x11b, 
    0x3, 0x2, 0x2, 0x2, 0x11d, 0x11c, 0x3, 0x2, 0x2, 0x2, 0x11e, 0x25, 0x3, 
    0x2, 0x2, 0x2, 0x11f, 0x120, 0x7, 0x4, 0x2, 0x2, 0x120, 0x125, 0x5, 
    0x28, 0x15, 0x2, 0x121, 0x122, 0x7, 0x5, 0x2, 0x2, 0x122, 0x124, 0x5, 
    0x28, 0x15, 0x2, 0x123, 0x121, 0x3, 0x2, 0x2, 0x2, 0x124, 0x127, 0x3, 
    0x2, 0x2, 0x2, 0x125, 0x123, 0x3, 0x2, 0x2, 0x2, 0x125, 0x126, 0x3, 
    0x2, 0x2, 0x2, 0x126, 0x128, 0x3, 0x2, 0x2, 0x2, 0x127, 0x125, 0x3, 
    0x2, 0x2, 0x2, 0x128, 0x129, 0x7, 0x6, 0x2, 0x2, 0x129, 0x27, 0x3, 0x2, 
    0x2, 0x2, 0x12a, 0x12b, 0x5, 0x14, 0xb, 0x2, 0x12b, 0x29, 0x3, 0x2, 
    0x2, 0x2, 0x12c, 0x12d, 0x5, 0x16, 0xc, 0x2, 0x12d, 0x12e, 0x7, 0xa, 
    0x2, 0x2, 0x12e, 0x12f, 0x5, 0x16, 0xc, 0x2, 0x12f, 0x2b, 0x3, 0x2, 
    0x2, 0x2, 0x130, 0x131, 0x7, 0x1d, 0x2, 0x2, 0x131, 0x132, 0x7, 0xb, 
    0x2, 0x2, 0x132, 0x133, 0x5, 0x2e, 0x18, 0x2, 0x133, 0x134, 0x7, 0xc, 
    0x2, 0x2, 0x134, 0x135, 0x7, 0x1e, 0x2, 0x2, 0x135, 0x136, 0x5, 0x22, 
    0x12, 0x2, 0x136, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x137, 0x13c, 0x5, 0x24, 
    0x13, 0x2, 0x138, 0x139, 0x7, 0x5, 0x2, 0x2, 0x139, 0x13b, 0x5, 0x24, 
    0x13, 0x2, 0x13a, 0x138, 0x3, 0x2, 0x2, 0x2, 0x13b, 0x13e, 0x3, 0x2, 
    0x2, 0x2, 0x13c, 0x13a, 0x3, 0x2, 0x2, 0x2, 0x13c, 0x13d, 0x3, 0x2, 
    0x2, 0x2, 0x13d, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x13e, 0x13c, 0x3, 0x2, 0x2, 
    0x2, 0x13f, 0x140, 0x7, 0x1f, 0x2, 0x2, 0x140, 0x142, 0x5, 0x32, 0x1a, 
    0x2, 0x141, 0x143, 0x7, 0x3, 0x2, 0x2, 0x142, 0x141, 0x3, 0x2, 0x2, 
    0x2, 0x142, 0x143, 0x3, 0x2, 0x2, 0x2, 0x143, 0x144, 0x3, 0x2, 0x2, 
    0x2, 0x144, 0x145, 0x7, 0x22, 0x2, 0x2, 0x145, 0x31, 0x3, 0x2, 0x2, 
    0x2, 0x146, 0x147, 0x5, 0x36, 0x1c, 0x2, 0x147, 0x33, 0x3, 0x2, 0x2, 
    0x2, 0x148, 0x149, 0x5, 0x36, 0x1c, 0x2, 0x149, 0x35, 0x3, 0x2, 0x2, 
    0x2, 0x14a, 0x14f, 0x5, 0x38, 0x1d, 0x2, 0x14b, 0x14c, 0x7, 0x3, 0x2, 
    0x2, 0x14c, 0x14e, 0x5, 0x38, 0x1d, 0x2, 0x14d, 0x14b, 0x3, 0x2, 0x2, 
    0x2, 0x14e, 0x151, 0x3, 0x2, 0x2, 0x2, 0x14f, 0x14d, 0x3, 0x2, 0x2, 
    0x2, 0x14f, 0x150, 0x3, 0x2, 0x2, 0x2, 0x150, 0x37, 0x3, 0x2, 0x2, 0x2, 
    0x151, 0x14f, 0x3, 0x2, 0x2, 0x2, 0x152, 0x153, 0x5, 0x22, 0x12, 0x2, 
    0x153, 0x154, 0x5, 0x3a, 0x1e, 0x2, 0x154, 0x39, 0x3, 0x2, 0x2, 0x2, 
    0x155, 0x15a, 0x5, 0x3c, 0x1f, 0x2, 0x156, 0x157, 0x7, 0x5, 0x2, 0x2, 
    0x157, 0x159, 0x5, 0x3c, 0x1f, 0x2, 0x158, 0x156, 0x3, 0x2, 0x2, 0x2, 
    0x159, 0x15c, 0x3, 0x2, 0x2, 0x2, 0x15a, 0x158, 0x3, 0x2, 0x2, 0x2, 
    0x15a, 0x15b, 0x3, 0x2, 0x2, 0x2, 0x15b, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x15c, 
    0x15a, 0x3, 0x2, 0x2, 0x2, 0x15d, 0x15e, 0x7, 0x3e, 0x2, 0x2, 0x15e, 
    0x3d, 0x3, 0x2, 0x2, 0x2, 0x15f, 0x163, 0x5, 0x40, 0x21, 0x2, 0x160, 
    0x162, 0x5, 0x40, 0x21, 0x2, 0x161, 0x160, 0x3, 0x2, 0x2, 0x2, 0x162, 
    0x165, 0x3, 0x2, 0x2, 0x2, 0x163, 0x161, 0x3, 0x2, 0x2, 0x2, 0x163, 
    0x164, 0x3, 0x2, 0x2, 0x2, 0x164, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x165, 0x163, 
    0x3, 0x2, 0x2, 0x2, 0x166, 0x169, 0x5, 0x42, 0x22, 0x2, 0x167, 0x169, 
    0x5, 0x44, 0x23, 0x2, 0x168, 0x166, 0x3, 0x2, 0x2, 0x2, 0x168, 0x167, 
    0x3, 0x2, 0x2, 0x2, 0x169, 0x16a, 0x3, 0x2, 0x2, 0x2, 0x16a, 0x16b, 
    0x7, 0x21, 0x2, 0x2, 0x16b, 0x16c, 0x5, 0xa, 0x6, 0x2, 0x16c, 0x16d, 
    0x7, 0x22, 0x2, 0x2, 0x16d, 0x41, 0x3, 0x2, 0x2, 0x2, 0x16e, 0x16f, 
    0x7, 0x3c, 0x2, 0x2, 0x16f, 0x170, 0x5, 0x46, 0x24, 0x2, 0x170, 0x172, 
    0x7, 0x4, 0x2, 0x2, 0x171, 0x173, 0x5, 0x48, 0x25, 0x2, 0x172, 0x171, 
    0x3, 0x2, 0x2, 0x2, 0x172, 0x173, 0x3, 0x2, 0x2, 0x2, 0x173, 0x174, 
    0x3, 0x2, 0x2, 0x2, 0x174, 0x175, 0x7, 0x6, 0x2, 0x2, 0x175, 0x43, 0x3, 
    0x2, 0x2, 0x2, 0x176, 0x177, 0x5, 0x20, 0x11, 0x2, 0x177, 0x178, 0x5, 
    0x46, 0x24, 0x2, 0x178, 0x17a, 0x7, 0x4, 0x2, 0x2, 0x179, 0x17b, 0x5, 
    0x48, 0x25, 0x2, 0x17a, 0x179, 0x3, 0x2, 0x2, 0x2, 0x17a, 0x17b, 0x3, 
    0x2, 0x2, 0x2, 0x17b, 0x17c, 0x3, 0x2, 0x2, 0x2, 0x17c, 0x17d, 0x7, 
    0x6, 0x2, 0x2, 0x17d, 0x45, 0x3, 0x2, 0x2, 0x2, 0x17e, 0x17f, 0x7, 0x3e, 
    0x2, 0x2, 0x17f, 0x47, 0x3, 0x2, 0x2, 0x2, 0x180, 0x181, 0x5, 0x4a, 
    0x26, 0x2, 0x181, 0x49, 0x3, 0x2, 0x2, 0x2, 0x182, 0x187, 0x5, 0x4c, 
    0x27, 0x2, 0x183, 0x184, 0x7, 0x5, 0x2, 0x2, 0x184, 0x186, 0x5, 0x4c, 
    0x27, 0x2, 0x185, 0x183, 0x3, 0x2, 0x2, 0x2, 0x186, 0x189, 0x3, 0x2, 
    0x2, 0x2, 0x187, 0x185, 0x3, 0x2, 0x2, 0x2, 0x187, 0x188, 0x3, 0x2, 
    0x2, 0x2, 0x188, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x189, 0x187, 0x3, 0x2, 0x2, 
    0x2, 0x18a, 0x18c, 0x7, 0x20, 0x2, 0x2, 0x18b, 0x18a, 0x3, 0x2, 0x2, 
    0x2, 0x18b, 0x18c, 0x3, 0x2, 0x2, 0x2, 0x18c, 0x18d, 0x3, 0x2, 0x2, 
    0x2, 0x18d, 0x18e, 0x5, 0x20, 0x11, 0x2, 0x18e, 0x18f, 0x5, 0x4e, 0x28, 
    0x2, 0x18f, 0x4d, 0x3, 0x2, 0x2, 0x2, 0x190, 0x195, 0x5, 0x50, 0x29, 
    0x2, 0x191, 0x192, 0x7, 0x5, 0x2, 0x2, 0x192, 0x194, 0x5, 0x50, 0x29, 
    0x2, 0x193, 0x191, 0x3, 0x2, 0x2, 0x2, 0x194, 0x197, 0x3, 0x2, 0x2, 
    0x2, 0x195, 0x193, 0x3, 0x2, 0x2, 0x2, 0x195, 0x196, 0x3, 0x2, 0x2, 
    0x2, 0x196, 0x4f, 0x3, 0x2, 0x2, 0x2, 0x197, 0x195, 0x3, 0x2, 0x2, 0x2, 
    0x198, 0x199, 0x7, 0x3e, 0x2, 0x2, 0x199, 0x51, 0x3, 0x2, 0x2, 0x2, 
    0x19a, 0x1a9, 0x5, 0x54, 0x2b, 0x2, 0x19b, 0x1a9, 0x5, 0x5a, 0x2e, 0x2, 
    0x19c, 0x1a9, 0x5, 0x60, 0x31, 0x2, 0x19d, 0x1a9, 0x5, 0x68, 0x35, 0x2, 
    0x19e, 0x1a9, 0x5, 0x72, 0x3a, 0x2, 0x19f, 0x1a9, 0x5, 0x74, 0x3b, 0x2, 
    0x1a0, 0x1a9, 0x5, 0x76, 0x3c, 0x2, 0x1a1, 0x1a9, 0x5, 0x80, 0x41, 0x2, 
    0x1a2, 0x1a9, 0x5, 0x82, 0x42, 0x2, 0x1a3, 0x1a9, 0x5, 0x8c, 0x47, 0x2, 
    0x1a4, 0x1a9, 0x5, 0x8e, 0x48, 0x2, 0x1a5, 0x1a9, 0x5, 0x78, 0x3d, 0x2, 
    0x1a6, 0x1a9, 0x5, 0x66, 0x34, 0x2, 0x1a7, 0x1a9, 0x5, 0x56, 0x2c, 0x2, 
    0x1a8, 0x19a, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x19b, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x19c, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x19d, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x19e, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x19f, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x1a0, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x1a1, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x1a2, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x1a3, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x1a4, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x1a5, 0x3, 0x2, 0x2, 0x2, 
    0x1a8, 0x1a6, 0x3, 0x2, 0x2, 0x2, 0x1a8, 0x1a7, 0x3, 0x2, 0x2, 0x2, 
    0x1a9, 0x53, 0x3, 0x2, 0x2, 0x2, 0x1aa, 0x1ab, 0x7, 0x21, 0x2, 0x2, 
    0x1ab, 0x1ac, 0x5, 0x58, 0x2d, 0x2, 0x1ac, 0x1ad, 0x7, 0x22, 0x2, 0x2, 
    0x1ad, 0x55, 0x3, 0x2, 0x2, 0x2, 0x1ae, 0x1af, 0x3, 0x2, 0x2, 0x2, 0x1af, 
    0x57, 0x3, 0x2, 0x2, 0x2, 0x1b0, 0x1b5, 0x5, 0x52, 0x2a, 0x2, 0x1b1, 
    0x1b2, 0x7, 0x3, 0x2, 0x2, 0x1b2, 0x1b4, 0x5, 0x52, 0x2a, 0x2, 0x1b3, 
    0x1b1, 0x3, 0x2, 0x2, 0x2, 0x1b4, 0x1b7, 0x3, 0x2, 0x2, 0x2, 0x1b5, 
    0x1b3, 0x3, 0x2, 0x2, 0x2, 0x1b5, 0x1b6, 0x3, 0x2, 0x2, 0x2, 0x1b6, 
    0x59, 0x3, 0x2, 0x2, 0x2, 0x1b7, 0x1b5, 0x3, 0x2, 0x2, 0x2, 0x1b8, 0x1b9, 
    0x5, 0x5c, 0x2f, 0x2, 0x1b9, 0x1ba, 0x7, 0x7, 0x2, 0x2, 0x1ba, 0x1bb, 
    0x5, 0x5e, 0x30, 0x2, 0x1bb, 0x1cb, 0x3, 0x2, 0x2, 0x2, 0x1bc, 0x1bd, 
    0x7, 0xd, 0x2, 0x2, 0x1bd, 0x1c2, 0x5, 0x9a, 0x4e, 0x2, 0x1be, 0x1bf, 
    0x5, 0x9a, 0x4e, 0x2, 0x1bf, 0x1c0, 0x7, 0xd, 0x2, 0x2, 0x1c0, 0x1c2, 
    0x3, 0x2, 0x2, 0x2, 0x1c1, 0x1bc, 0x3, 0x2, 0x2, 0x2, 0x1c1, 0x1be, 
    0x3, 0x2, 0x2, 0x2, 0x1c2, 0x1cb, 0x3, 0x2, 0x2, 0x2, 0x1c3, 0x1c4, 
    0x7, 0xe, 0x2, 0x2, 0x1c4, 0x1c9, 0x5, 0x9a, 0x4e, 0x2, 0x1c5, 0x1c6, 
    0x5, 0x9a, 0x4e, 0x2, 0x1c6, 0x1c7, 0x7, 0xe, 0x2, 0x2, 0x1c7, 0x1c9, 
    0x3, 0x2, 0x2, 0x2, 0x1c8, 0x1c3, 0x3, 0x2, 0x2, 0x2, 0x1c8, 0x1c5, 
    0x3, 0x2, 0x2, 0x2, 0x1c9, 0x1cb, 0x3, 0x2, 0x2, 0x2, 0x1ca, 0x1b8, 
    0x3, 0x2, 0x2, 0x2, 0x1ca, 0x1c1, 0x3, 0x2, 0x2, 0x2, 0x1ca, 0x1c8, 
    0x3, 0x2, 0x2, 0x2, 0x1cb, 0x5b, 0x3, 0x2, 0x2, 0x2, 0x1cc, 0x1cd, 0x5, 
    0x9a, 0x4e, 0x2, 0x1cd, 0x5d, 0x3, 0x2, 0x2, 0x2, 0x1ce, 0x1cf, 0x5, 
    0x92, 0x4a, 0x2, 0x1cf, 0x5f, 0x3, 0x2, 0x2, 0x2, 0x1d0, 0x1d1, 0x7, 
    0x28, 0x2, 0x2, 0x1d1, 0x1d2, 0x7, 0x4, 0x2, 0x2, 0x1d2, 0x1d3, 0x5, 
    0x92, 0x4a, 0x2, 0x1d3, 0x1d4, 0x7, 0x6, 0x2, 0x2, 0x1d4, 0x1d7, 0x5, 
    0x62, 0x32, 0x2, 0x1d5, 0x1d6, 0x7, 0x2a, 0x2, 0x2, 0x1d6, 0x1d8, 0x5, 
    0x64, 0x33, 0x2, 0x1d7, 0x1d5, 0x3, 0x2, 0x2, 0x2, 0x1d7, 0x1d8, 0x3, 
    0x2, 0x2, 0x2, 0x1d8, 0x61, 0x3, 0x2, 0x2, 0x2, 0x1d9, 0x1da, 0x5, 0x52, 
    0x2a, 0x2, 0x1da, 0x63, 0x3, 0x2, 0x2, 0x2, 0x1db, 0x1dc, 0x5, 0x52, 
    0x2a, 0x2, 0x1dc, 0x65, 0x3, 0x2, 0x2, 0x2, 0x1dd, 0x1df, 0x7, 0x3d, 
    0x2, 0x2, 0x1de, 0x1e0, 0x5, 0x92, 0x4a, 0x2, 0x1df, 0x1de, 0x3, 0x2, 
    0x2, 0x2, 0x1df, 0x1e0, 0x3, 0x2, 0x2, 0x2, 0x1e0, 0x67, 0x3, 0x2, 0x2, 
    0x2, 0x1e1, 0x1e2, 0x7, 0x2b, 0x2, 0x2, 0x1e2, 0x1e3, 0x7, 0x4, 0x2, 
    0x2, 0x1e3, 0x1e4, 0x5, 0x92, 0x4a, 0x2, 0x1e4, 0x1e5, 0x7, 0x6, 0x2, 
    0x2, 0x1e5, 0x1e6, 0x7, 0x21, 0x2, 0x2, 0x1e6, 0x1e7, 0x5, 0x6a, 0x36, 
    0x2, 0x1e7, 0x1e8, 0x7, 0x22, 0x2, 0x2, 0x1e8, 0x69, 0x3, 0x2, 0x2, 
    0x2, 0x1e9, 0x1ee, 0x5, 0x6c, 0x37, 0x2, 0x1ea, 0x1eb, 0x7, 0x3, 0x2, 
    0x2, 0x1eb, 0x1ed, 0x5, 0x6c, 0x37, 0x2, 0x1ec, 0x1ea, 0x3, 0x2, 0x2, 
    0x2, 0x1ed, 0x1f0, 0x3, 0x2, 0x2, 0x2, 0x1ee, 0x1ec, 0x3, 0x2, 0x2, 
    0x2, 0x1ee, 0x1ef, 0x3, 0x2, 0x2, 0x2, 0x1ef, 0x6b, 0x3, 0x2, 0x2, 0x2, 
    0x1f0, 0x1ee, 0x3, 0x2, 0x2, 0x2, 0x1f1, 0x1f2, 0x5, 0x6e, 0x38, 0x2, 
    0x1f2, 0x1f3, 0x7, 0xf, 0x2, 0x2, 0x1f3, 0x1f4, 0x5, 0x52, 0x2a, 0x2, 
    0x1f4, 0x1f7, 0x3, 0x2, 0x2, 0x2, 0x1f5, 0x1f7, 0x3, 0x2, 0x2, 0x2, 
    0x1f6, 0x1f1, 0x3, 0x2, 0x2, 0x2, 0x1f6, 0x1f5, 0x3, 0x2, 0x2, 0x2, 
    0x1f7, 0x6d, 0x3, 0x2, 0x2, 0x2, 0x1f8, 0x1f9, 0x7, 0x2c, 0x2, 0x2, 
    0x1f9, 0x1ff, 0x5, 0x70, 0x39, 0x2, 0x1fa, 0x1fb, 0x7, 0x5, 0x2, 0x2, 
    0x1fb, 0x1fc, 0x7, 0x2c, 0x2, 0x2, 0x1fc, 0x1fe, 0x5, 0x70, 0x39, 0x2, 
    0x1fd, 0x1fa, 0x3, 0x2, 0x2, 0x2, 0x1fe, 0x201, 0x3, 0x2, 0x2, 0x2, 
    0x1ff, 0x1fd, 0x3, 0x2, 0x2, 0x2, 0x1ff, 0x200, 0x3, 0x2, 0x2, 0x2, 
    0x200, 0x6f, 0x3, 0x2, 0x2, 0x2, 0x201, 0x1ff, 0x3, 0x2, 0x2, 0x2, 0x202, 
    0x203, 0x5, 0x16, 0xc, 0x2, 0x203, 0x71, 0x3, 0x2, 0x2, 0x2, 0x204, 
    0x205, 0x7, 0x30, 0x2, 0x2, 0x205, 0x206, 0x7, 0x21, 0x2, 0x2, 0x206, 
    0x207, 0x5, 0x58, 0x2d, 0x2, 0x207, 0x208, 0x7, 0x22, 0x2, 0x2, 0x208, 
    0x209, 0x7, 0x2f, 0x2, 0x2, 0x209, 0x20a, 0x7, 0x4, 0x2, 0x2, 0x20a, 
    0x20b, 0x5, 0x92, 0x4a, 0x2, 0x20b, 0x20c, 0x7, 0x6, 0x2, 0x2, 0x20c, 
    0x73, 0x3, 0x2, 0x2, 0x2, 0x20d, 0x20e, 0x7, 0x2f, 0x2, 0x2, 0x20e, 
    0x20f, 0x7, 0x4, 0x2, 0x2, 0x20f, 0x210, 0x5, 0x92, 0x4a, 0x2, 0x210, 
    0x211, 0x7, 0x6, 0x2, 0x2, 0x211, 0x212, 0x5, 0x52, 0x2a, 0x2, 0x212, 
    0x75, 0x3, 0x2, 0x2, 0x2, 0x213, 0x214, 0x7, 0x31, 0x2, 0x2, 0x214, 
    0x215, 0x7, 0x4, 0x2, 0x2, 0x215, 0x216, 0x5, 0x52, 0x2a, 0x2, 0x216, 
    0x217, 0x7, 0x3, 0x2, 0x2, 0x217, 0x218, 0x5, 0x92, 0x4a, 0x2, 0x218, 
    0x219, 0x7, 0x3, 0x2, 0x2, 0x219, 0x21a, 0x5, 0x52, 0x2a, 0x2, 0x21a, 
    0x21b, 0x7, 0x6, 0x2, 0x2, 0x21b, 0x21c, 0x5, 0x52, 0x2a, 0x2, 0x21c, 
    0x77, 0x3, 0x2, 0x2, 0x2, 0x21d, 0x21e, 0x5, 0x7a, 0x3e, 0x2, 0x21e, 
    0x220, 0x7, 0x4, 0x2, 0x2, 0x21f, 0x221, 0x5, 0x7c, 0x3f, 0x2, 0x220, 
    0x21f, 0x3, 0x2, 0x2, 0x2, 0x220, 0x221, 0x3, 0x2, 0x2, 0x2, 0x221, 
    0x222, 0x3, 0x2, 0x2, 0x2, 0x222, 0x223, 0x7, 0x6, 0x2, 0x2, 0x223, 
    0x79, 0x3, 0x2, 0x2, 0x2, 0x224, 0x225, 0x7, 0x3e, 0x2, 0x2, 0x225, 
    0x7b, 0x3, 0x2, 0x2, 0x2, 0x226, 0x22b, 0x5, 0x7e, 0x40, 0x2, 0x227, 
    0x228, 0x7, 0x5, 0x2, 0x2, 0x228, 0x22a, 0x5, 0x7e, 0x40, 0x2, 0x229, 
    0x227, 0x3, 0x2, 0x2, 0x2, 0x22a, 0x22d, 0x3, 0x2, 0x2, 0x2, 0x22b, 
    0x229, 0x3, 0x2, 0x2, 0x2, 0x22b, 0x22c, 0x3, 0x2, 0x2, 0x2, 0x22c, 
    0x7d, 0x3, 0x2, 0x2, 0x2, 0x22d, 0x22b, 0x3, 0x2, 0x2, 0x2, 0x22e, 0x22f, 
    0x5, 0x92, 0x4a, 0x2, 0x22f, 0x7f, 0x3, 0x2, 0x2, 0x2, 0x230, 0x231, 
    0x7, 0x36, 0x2, 0x2, 0x231, 0x232, 0x5, 0x84, 0x43, 0x2, 0x232, 0x81, 
    0x3, 0x2, 0x2, 0x2, 0x233, 0x235, 0x7, 0x37, 0x2, 0x2, 0x234, 0x236, 
    0x5, 0x84, 0x43, 0x2, 0x235, 0x234, 0x3, 0x2, 0x2, 0x2, 0x235, 0x236, 
    0x3, 0x2, 0x2, 0x2, 0x236, 0x83, 0x3, 0x2, 0x2, 0x2, 0x237, 0x238, 0x7, 
    0x4, 0x2, 0x2, 0x238, 0x23d, 0x5, 0x86, 0x44, 0x2, 0x239, 0x23a, 0x7, 
    0x5, 0x2, 0x2, 0x23a, 0x23c, 0x5, 0x86, 0x44, 0x2, 0x23b, 0x239, 0x3, 
    0x2, 0x2, 0x2, 0x23c, 0x23f, 0x3, 0x2, 0x2, 0x2, 0x23d, 0x23b, 0x3, 
    0x2, 0x2, 0x2, 0x23d, 0x23e, 0x3, 0x2, 0x2, 0x2, 0x23e, 0x240, 0x3, 
    0x2, 0x2, 0x2, 0x23f, 0x23d, 0x3, 0x2, 0x2, 0x2, 0x240, 0x241, 0x7, 
    0x6, 0x2, 0x2, 0x241, 0x85, 0x3, 0x2, 0x2, 0x2, 0x242, 0x245, 0x5, 0x92, 
    0x4a, 0x2, 0x243, 0x244, 0x7, 0xf, 0x2, 0x2, 0x244, 0x246, 0x5, 0x88, 
    0x45, 0x2, 0x245, 0x243, 0x3, 0x2, 0x2, 0x2, 0x245, 0x246, 0x3, 0x2, 
    0x2, 0x2, 0x246, 0x87, 0x3, 0x2, 0x2, 0x2, 0x247, 0x249, 0x5, 0x18, 
    0xd, 0x2, 0x248, 0x247, 0x3, 0x2, 0x2, 0x2, 0x248, 0x249, 0x3, 0x2, 
    0x2, 0x2, 0x249, 0x24a, 0x3, 0x2, 0x2, 0x2, 0x24a, 0x24d, 0x5, 0xac, 
    0x57, 0x2, 0x24b, 0x24c, 0x7, 0xf, 0x2, 0x2, 0x24c, 0x24e, 0x5, 0x8a, 
    0x46, 0x2, 0x24d, 0x24b, 0x3, 0x2, 0x2, 0x2, 0x24d, 0x24e, 0x3, 0x2, 
    0x2, 0x2, 0x24e, 0x89, 0x3, 0x2, 0x2, 0x2, 0x24f, 0x250, 0x5, 0xac, 
    0x57, 0x2, 0x250, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x251, 0x252, 0x7, 0x38, 
    0x2, 0x2, 0x252, 0x253, 0x5, 0x90, 0x49, 0x2, 0x253, 0x8d, 0x3, 0x2, 
    0x2, 0x2, 0x254, 0x255, 0x7, 0x39, 0x2, 0x2, 0x255, 0x256, 0x5, 0x90, 
    0x49, 0x2, 0x256, 0x8f, 0x3, 0x2, 0x2, 0x2, 0x257, 0x258, 0x7, 0x4, 
    0x2, 0x2, 0x258, 0x25d, 0x5, 0x9a, 0x4e, 0x2, 0x259, 0x25a, 0x7, 0x5, 
    0x2, 0x2, 0x25a, 0x25c, 0x5, 0x9a, 0x4e, 0x2, 0x25b, 0x259, 0x3, 0x2, 
    0x2, 0x2, 0x25c, 0x25f, 0x3, 0x2, 0x2, 0x2, 0x25d, 0x25b, 0x3, 0x2, 
    0x2, 0x2, 0x25d, 0x25e, 0x3, 0x2, 0x2, 0x2, 0x25e, 0x260, 0x3, 0x2, 
    0x2, 0x2, 0x25f, 0x25d, 0x3, 0x2, 0x2, 0x2, 0x260, 0x261, 0x7, 0x6, 
    0x2, 0x2, 0x261, 0x91, 0x3, 0x2, 0x2, 0x2, 0x262, 0x266, 0x5, 0x94, 
    0x4b, 0x2, 0x263, 0x264, 0x5, 0xb4, 0x5b, 0x2, 0x264, 0x265, 0x5, 0x94, 
    0x4b, 0x2, 0x265, 0x267, 0x3, 0x2, 0x2, 0x2, 0x266, 0x263, 0x3, 0x2, 
    0x2, 0x2, 0x266, 0x267, 0x3, 0x2, 0x2, 0x2, 0x267, 0x93, 0x3, 0x2, 0x2, 
    0x2, 0x268, 0x26a, 0x5, 0x18, 0xd, 0x2, 0x269, 0x268, 0x3, 0x2, 0x2, 
    0x2, 0x269, 0x26a, 0x3, 0x2, 0x2, 0x2, 0x26a, 0x26b, 0x3, 0x2, 0x2, 
    0x2, 0x26b, 0x271, 0x5, 0x96, 0x4c, 0x2, 0x26c, 0x26d, 0x5, 0xb6, 0x5c, 
    0x2, 0x26d, 0x26e, 0x5, 0x96, 0x4c, 0x2, 0x26e, 0x270, 0x3, 0x2, 0x2, 
    0x2, 0x26f, 0x26c, 0x3, 0x2, 0x2, 0x2, 0x270, 0x273, 0x3, 0x2, 0x2, 
    0x2, 0x271, 0x26f, 0x3, 0x2, 0x2, 0x2, 0x271, 0x272, 0x3, 0x2, 0x2, 
    0x2, 0x272, 0x95, 0x3, 0x2, 0x2, 0x2, 0x273, 0x271, 0x3, 0x2, 0x2, 0x2, 
    0x274, 0x27a, 0x5, 0x98, 0x4d, 0x2, 0x275, 0x276, 0x5, 0xb8, 0x5d, 0x2, 
    0x276, 0x277, 0x5, 0x98, 0x4d, 0x2, 0x277, 0x279, 0x3, 0x2, 0x2, 0x2, 
    0x278, 0x275, 0x3, 0x2, 0x2, 0x2, 0x279, 0x27c, 0x3, 0x2, 0x2, 0x2, 
    0x27a, 0x278, 0x3, 0x2, 0x2, 0x2, 0x27a, 0x27b, 0x3, 0x2, 0x2, 0x2, 
    0x27b, 0x97, 0x3, 0x2, 0x2, 0x2, 0x27c, 0x27a, 0x3, 0x2, 0x2, 0x2, 0x27d, 
    0x289, 0x5, 0x9a, 0x4e, 0x2, 0x27e, 0x289, 0x5, 0xa8, 0x55, 0x2, 0x27f, 
    0x289, 0x5, 0xb0, 0x59, 0x2, 0x280, 0x289, 0x5, 0xb2, 0x5a, 0x2, 0x281, 
    0x289, 0x5, 0xa4, 0x53, 0x2, 0x282, 0x283, 0x7, 0x27, 0x2, 0x2, 0x283, 
    0x289, 0x5, 0x98, 0x4d, 0x2, 0x284, 0x285, 0x7, 0x4, 0x2, 0x2, 0x285, 
    0x286, 0x5, 0x92, 0x4a, 0x2, 0x286, 0x287, 0x7, 0x6, 0x2, 0x2, 0x287, 
    0x289, 0x3, 0x2, 0x2, 0x2, 0x288, 0x27d, 0x3, 0x2, 0x2, 0x2, 0x288, 
    0x27e, 0x3, 0x2, 0x2, 0x2, 0x288, 0x27f, 0x3, 0x2, 0x2, 0x2, 0x288, 
    0x280, 0x3, 0x2, 0x2, 0x2, 0x288, 0x281, 0x3, 0x2, 0x2, 0x2, 0x288, 
    0x282, 0x3, 0x2, 0x2, 0x2, 0x288, 0x284, 0x3, 0x2, 0x2, 0x2, 0x289, 
    0x99, 0x3, 0x2, 0x2, 0x2, 0x28a, 0x28e, 0x5, 0x3c, 0x1f, 0x2, 0x28b, 
    0x28d, 0x5, 0x9c, 0x4f, 0x2, 0x28c, 0x28b, 0x3, 0x2, 0x2, 0x2, 0x28d, 
    0x290, 0x3, 0x2, 0x2, 0x2, 0x28e, 0x28c, 0x3, 0x2, 0x2, 0x2, 0x28e, 
    0x28f, 0x3, 0x2, 0x2, 0x2, 0x28f, 0x9b, 0x3, 0x2, 0x2, 0x2, 0x290, 0x28e, 
    0x3, 0x2, 0x2, 0x2, 0x291, 0x292, 0x7, 0xb, 0x2, 0x2, 0x292, 0x293, 
    0x5, 0x9e, 0x50, 0x2, 0x293, 0x294, 0x7, 0xc, 0x2, 0x2, 0x294, 0x298, 
    0x3, 0x2, 0x2, 0x2, 0x295, 0x296, 0x7, 0x10, 0x2, 0x2, 0x296, 0x298, 
    0x5, 0xa2, 0x52, 0x2, 0x297, 0x291, 0x3, 0x2, 0x2, 0x2, 0x297, 0x295, 
    0x3, 0x2, 0x2, 0x2, 0x298, 0x9d, 0x3, 0x2, 0x2, 0x2, 0x299, 0x29e, 0x5, 
    0xa0, 0x51, 0x2, 0x29a, 0x29b, 0x7, 0x5, 0x2, 0x2, 0x29b, 0x29d, 0x5, 
    0xa0, 0x51, 0x2, 0x29c, 0x29a, 0x3, 0x2, 0x2, 0x2, 0x29d, 0x2a0, 0x3, 
    0x2, 0x2, 0x2, 0x29e, 0x29c, 0x3, 0x2, 0x2, 0x2, 0x29e, 0x29f, 0x3, 
    0x2, 0x2, 0x2, 0x29f, 0x9f, 0x3, 0x2, 0x2, 0x2, 0x2a0, 0x29e, 0x3, 0x2, 
    0x2, 0x2, 0x2a1, 0x2a2, 0x5, 0x92, 0x4a, 0x2, 0x2a2, 0xa1, 0x3, 0x2, 
    0x2, 0x2, 0x2a3, 0x2a4, 0x7, 0x3e, 0x2, 0x2, 0x2a4, 0xa3, 0x3, 0x2, 
    0x2, 0x2, 0x2a5, 0x2a6, 0x5, 0xa6, 0x54, 0x2, 0x2a6, 0x2a8, 0x7, 0x4, 
    0x2, 0x2, 0x2a7, 0x2a9, 0x5, 0x7c, 0x3f, 0x2, 0x2a8, 0x2a7, 0x3, 0x2, 
    0x2, 0x2, 0x2a8, 0x2a9, 0x3, 0x2, 0x2, 0x2, 0x2a9, 0x2aa, 0x3, 0x2, 
    0x2, 0x2, 0x2aa, 0x2ab, 0x7, 0x6, 0x2, 0x2, 0x2ab, 0xa5, 0x3, 0x2, 0x2, 
    0x2, 0x2ac, 0x2ad, 0x7, 0x3e, 0x2, 0x2, 0x2ad, 0xa7, 0x3, 0x2, 0x2, 
    0x2, 0x2ae, 0x2b0, 0x5, 0x18, 0xd, 0x2, 0x2af, 0x2ae, 0x3, 0x2, 0x2, 
    0x2, 0x2af, 0x2b0, 0x3, 0x2, 0x2, 0x2, 0x2b0, 0x2b1, 0x3, 0x2, 0x2, 
    0x2, 0x2b1, 0x2b2, 0x5, 0xaa, 0x56, 0x2, 0x2b2, 0xa9, 0x3, 0x2, 0x2, 
    0x2, 0x2b3, 0x2b6, 0x5, 0xac, 0x57, 0x2, 0x2b4, 0x2b6, 0x5, 0xae, 0x58, 
    0x2, 0x2b5, 0x2b3, 0x3, 0x2, 0x2, 0x2, 0x2b5, 0x2b4, 0x3, 0x2, 0x2, 
    0x2, 0x2b6, 0xab, 0x3, 0x2, 0x2, 0x2, 0x2b7, 0x2b8, 0x7, 0x3f, 0x2, 
    0x2, 0x2b8, 0xad, 0x3, 0x2, 0x2, 0x2, 0x2b9, 0x2ba, 0x7, 0x40, 0x2, 
    0x2, 0x2ba, 0xaf, 0x3, 0x2, 0x2, 0x2, 0x2bb, 0x2bc, 0x7, 0x44, 0x2, 
    0x2, 0x2bc, 0xb1, 0x3, 0x2, 0x2, 0x2, 0x2bd, 0x2be, 0x7, 0x45, 0x2, 
    0x2, 0x2be, 0xb3, 0x3, 0x2, 0x2, 0x2, 0x2bf, 0x2c0, 0x9, 0x3, 0x2, 0x2, 
    0x2c0, 0xb5, 0x3, 0x2, 0x2, 0x2, 0x2c1, 0x2c2, 0x9, 0x4, 0x2, 0x2, 0x2c2, 
    0xb7, 0x3, 0x2, 0x2, 0x2, 0x2c3, 0x2c4, 0x9, 0x5, 0x2, 0x2, 0x2c4, 0xb9, 
    0x3, 0x2, 0x2, 0x2, 0x38, 0xc0, 0xca, 0xd7, 0xdc, 0xe1, 0xe4, 0xee, 
    0xf8, 0xfc, 0x100, 0x10b, 0x118, 0x11d, 0x125, 0x13c, 0x142, 0x14f, 
    0x15a, 0x163, 0x168, 0x172, 0x17a, 0x187, 0x18b, 0x195, 0x1a8, 0x1b5, 
    0x1c1, 0x1c8, 0x1ca, 0x1d7, 0x1df, 0x1ee, 0x1f6, 0x1ff, 0x220, 0x22b, 
    0x235, 0x23d, 0x245, 0x248, 0x24d, 0x25d, 0x266, 0x269, 0x271, 0x27a, 
    0x288, 0x28e, 0x297, 0x29e, 0x2a8, 0x2af, 0x2b5, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

CmmParser::Initializer CmmParser::_init;
