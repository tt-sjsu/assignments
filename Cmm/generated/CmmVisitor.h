
    #include <map>
    #include "intermediate/symtab/Symtab.h"
    #include "intermediate/type/Typespec.h"
    using namespace intermediate::symtab;
    using namespace intermediate::type;


// Generated from Cmm.g4 by ANTLR 4.9.1

#pragma once


#include "antlr4-runtime.h"
#include "CmmParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by CmmParser.
 */
class  CmmVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by CmmParser.
   */
    virtual antlrcpp::Any visitProgram(CmmParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitProgramHeader(CmmParser::ProgramHeaderContext *context) = 0;

    virtual antlrcpp::Any visitProgramParameters(CmmParser::ProgramParametersContext *context) = 0;

    virtual antlrcpp::Any visitProgramIdentifier(CmmParser::ProgramIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitBlock(CmmParser::BlockContext *context) = 0;

    virtual antlrcpp::Any visitDeclarations(CmmParser::DeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitConstantsPart(CmmParser::ConstantsPartContext *context) = 0;

    virtual antlrcpp::Any visitConstantDefinitionsList(CmmParser::ConstantDefinitionsListContext *context) = 0;

    virtual antlrcpp::Any visitConstantDefinition(CmmParser::ConstantDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitConstantIdentifier(CmmParser::ConstantIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitConstant(CmmParser::ConstantContext *context) = 0;

    virtual antlrcpp::Any visitSign(CmmParser::SignContext *context) = 0;

    virtual antlrcpp::Any visitTypesPart(CmmParser::TypesPartContext *context) = 0;

    virtual antlrcpp::Any visitTypeDefinitionsList(CmmParser::TypeDefinitionsListContext *context) = 0;

    virtual antlrcpp::Any visitTypeDefinition(CmmParser::TypeDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitTypeIdentifier(CmmParser::TypeIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitSimpleTypespec(CmmParser::SimpleTypespecContext *context) = 0;

    virtual antlrcpp::Any visitArrayTypespec(CmmParser::ArrayTypespecContext *context) = 0;

    virtual antlrcpp::Any visitRecordTypespec(CmmParser::RecordTypespecContext *context) = 0;

    virtual antlrcpp::Any visitTypeIdentifierTypespec(CmmParser::TypeIdentifierTypespecContext *context) = 0;

    virtual antlrcpp::Any visitEnumerationTypespec(CmmParser::EnumerationTypespecContext *context) = 0;

    virtual antlrcpp::Any visitSubrangeTypespec(CmmParser::SubrangeTypespecContext *context) = 0;

    virtual antlrcpp::Any visitEnumerationType(CmmParser::EnumerationTypeContext *context) = 0;

    virtual antlrcpp::Any visitEnumerationConstant(CmmParser::EnumerationConstantContext *context) = 0;

    virtual antlrcpp::Any visitSubrangeType(CmmParser::SubrangeTypeContext *context) = 0;

    virtual antlrcpp::Any visitArrayType(CmmParser::ArrayTypeContext *context) = 0;

    virtual antlrcpp::Any visitArrayDimensionList(CmmParser::ArrayDimensionListContext *context) = 0;

    virtual antlrcpp::Any visitRecordType(CmmParser::RecordTypeContext *context) = 0;

    virtual antlrcpp::Any visitRecordFields(CmmParser::RecordFieldsContext *context) = 0;

    virtual antlrcpp::Any visitVariablesPart(CmmParser::VariablesPartContext *context) = 0;

    virtual antlrcpp::Any visitVariableDeclarationsList(CmmParser::VariableDeclarationsListContext *context) = 0;

    virtual antlrcpp::Any visitVariableDeclarations(CmmParser::VariableDeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitVariableIdentifierList(CmmParser::VariableIdentifierListContext *context) = 0;

    virtual antlrcpp::Any visitVariableIdentifier(CmmParser::VariableIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitRoutinesPart(CmmParser::RoutinesPartContext *context) = 0;

    virtual antlrcpp::Any visitRoutineDefinition(CmmParser::RoutineDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitProcedureHead(CmmParser::ProcedureHeadContext *context) = 0;

    virtual antlrcpp::Any visitFunctionHead(CmmParser::FunctionHeadContext *context) = 0;

    virtual antlrcpp::Any visitRoutineIdentifier(CmmParser::RoutineIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitParameters(CmmParser::ParametersContext *context) = 0;

    virtual antlrcpp::Any visitParameterDeclarationsList(CmmParser::ParameterDeclarationsListContext *context) = 0;

    virtual antlrcpp::Any visitParameterDeclarations(CmmParser::ParameterDeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitParameterIdentifierList(CmmParser::ParameterIdentifierListContext *context) = 0;

    virtual antlrcpp::Any visitParameterIdentifier(CmmParser::ParameterIdentifierContext *context) = 0;

    virtual antlrcpp::Any visitStatement(CmmParser::StatementContext *context) = 0;

    virtual antlrcpp::Any visitCompoundStatement(CmmParser::CompoundStatementContext *context) = 0;

    virtual antlrcpp::Any visitEmptyStatement(CmmParser::EmptyStatementContext *context) = 0;

    virtual antlrcpp::Any visitStatementList(CmmParser::StatementListContext *context) = 0;

    virtual antlrcpp::Any visitNormalAssignment(CmmParser::NormalAssignmentContext *context) = 0;

    virtual antlrcpp::Any visitIncrementAssignment(CmmParser::IncrementAssignmentContext *context) = 0;

    virtual antlrcpp::Any visitDecrementAssignment(CmmParser::DecrementAssignmentContext *context) = 0;

    virtual antlrcpp::Any visitLhs(CmmParser::LhsContext *context) = 0;

    virtual antlrcpp::Any visitRhs(CmmParser::RhsContext *context) = 0;

    virtual antlrcpp::Any visitIfStatement(CmmParser::IfStatementContext *context) = 0;

    virtual antlrcpp::Any visitTrueStatement(CmmParser::TrueStatementContext *context) = 0;

    virtual antlrcpp::Any visitFalseStatement(CmmParser::FalseStatementContext *context) = 0;

    virtual antlrcpp::Any visitReturnStatement(CmmParser::ReturnStatementContext *context) = 0;

    virtual antlrcpp::Any visitCaseStatement(CmmParser::CaseStatementContext *context) = 0;

    virtual antlrcpp::Any visitCaseBranchList(CmmParser::CaseBranchListContext *context) = 0;

    virtual antlrcpp::Any visitCaseBranch(CmmParser::CaseBranchContext *context) = 0;

    virtual antlrcpp::Any visitCaseConstantList(CmmParser::CaseConstantListContext *context) = 0;

    virtual antlrcpp::Any visitCaseConstant(CmmParser::CaseConstantContext *context) = 0;

    virtual antlrcpp::Any visitRepeatStatement(CmmParser::RepeatStatementContext *context) = 0;

    virtual antlrcpp::Any visitWhileStatement(CmmParser::WhileStatementContext *context) = 0;

    virtual antlrcpp::Any visitForStatement(CmmParser::ForStatementContext *context) = 0;

    virtual antlrcpp::Any visitProcedureCallStatement(CmmParser::ProcedureCallStatementContext *context) = 0;

    virtual antlrcpp::Any visitProcedureName(CmmParser::ProcedureNameContext *context) = 0;

    virtual antlrcpp::Any visitArgumentList(CmmParser::ArgumentListContext *context) = 0;

    virtual antlrcpp::Any visitArgument(CmmParser::ArgumentContext *context) = 0;

    virtual antlrcpp::Any visitWriteStatement(CmmParser::WriteStatementContext *context) = 0;

    virtual antlrcpp::Any visitWritelnStatement(CmmParser::WritelnStatementContext *context) = 0;

    virtual antlrcpp::Any visitWriteArguments(CmmParser::WriteArgumentsContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgument(CmmParser::WriteArgumentContext *context) = 0;

    virtual antlrcpp::Any visitFieldWidth(CmmParser::FieldWidthContext *context) = 0;

    virtual antlrcpp::Any visitDecimalPlaces(CmmParser::DecimalPlacesContext *context) = 0;

    virtual antlrcpp::Any visitReadStatement(CmmParser::ReadStatementContext *context) = 0;

    virtual antlrcpp::Any visitReadlnStatement(CmmParser::ReadlnStatementContext *context) = 0;

    virtual antlrcpp::Any visitReadArguments(CmmParser::ReadArgumentsContext *context) = 0;

    virtual antlrcpp::Any visitExpression(CmmParser::ExpressionContext *context) = 0;

    virtual antlrcpp::Any visitSimpleExpression(CmmParser::SimpleExpressionContext *context) = 0;

    virtual antlrcpp::Any visitTerm(CmmParser::TermContext *context) = 0;

    virtual antlrcpp::Any visitVariableFactor(CmmParser::VariableFactorContext *context) = 0;

    virtual antlrcpp::Any visitNumberFactor(CmmParser::NumberFactorContext *context) = 0;

    virtual antlrcpp::Any visitCharacterFactor(CmmParser::CharacterFactorContext *context) = 0;

    virtual antlrcpp::Any visitStringFactor(CmmParser::StringFactorContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCallFactor(CmmParser::FunctionCallFactorContext *context) = 0;

    virtual antlrcpp::Any visitNotFactor(CmmParser::NotFactorContext *context) = 0;

    virtual antlrcpp::Any visitParenthesizedFactor(CmmParser::ParenthesizedFactorContext *context) = 0;

    virtual antlrcpp::Any visitVariable(CmmParser::VariableContext *context) = 0;

    virtual antlrcpp::Any visitModifier(CmmParser::ModifierContext *context) = 0;

    virtual antlrcpp::Any visitIndexList(CmmParser::IndexListContext *context) = 0;

    virtual antlrcpp::Any visitIndex(CmmParser::IndexContext *context) = 0;

    virtual antlrcpp::Any visitField(CmmParser::FieldContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCall(CmmParser::FunctionCallContext *context) = 0;

    virtual antlrcpp::Any visitFunctionName(CmmParser::FunctionNameContext *context) = 0;

    virtual antlrcpp::Any visitNumber(CmmParser::NumberContext *context) = 0;

    virtual antlrcpp::Any visitUnsignedNumber(CmmParser::UnsignedNumberContext *context) = 0;

    virtual antlrcpp::Any visitIntegerConstant(CmmParser::IntegerConstantContext *context) = 0;

    virtual antlrcpp::Any visitRealConstant(CmmParser::RealConstantContext *context) = 0;

    virtual antlrcpp::Any visitCharacterConstant(CmmParser::CharacterConstantContext *context) = 0;

    virtual antlrcpp::Any visitStringConstant(CmmParser::StringConstantContext *context) = 0;

    virtual antlrcpp::Any visitRelOp(CmmParser::RelOpContext *context) = 0;

    virtual antlrcpp::Any visitAddOp(CmmParser::AddOpContext *context) = 0;

    virtual antlrcpp::Any visitMulOp(CmmParser::MulOpContext *context) = 0;


};

