

## To set up environment
```bash
export CLASSPATH="${PWD}/lib/antlr-4.9.1-complete.jar:$CLASSPATH"
alias antlr4="java -jar ${PWD}/lib/antlr-4.9.1-complete.jar"
alias grun='java org.antlr.v4.gui.TestRig'
```
## Create parser library from antlr4
```bash
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o generated Cmm.g4
```
## Compile

### Compile the project with g++
```bash
g++ -std=c++11 -I${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/antlr4-runtime \
 -L${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/lib -I${PWD} \
 -I${PWD}/generated -lantlr4-runtime ${PWD}/generated/*.cpp \
 backend/compiler/*.cpp backend/converter/*.cpp backend/interpreter/*.cpp  \
 frontend/*.cpp intermediate/symtab/*.cpp intermediate/type/*.cpp intermediate/util/*.cpp \
 -o Cmm Cmm.cpp
```

### Compile the project with Cmake
```bash
cmake build .
make
```

## Run sample program
```bash
./Cmm -compile inputs/TestIf.cmm
jasmin TestIf.j
java TestIf
```
### Generate outputs
```bash
mkdir -p outputs/jasmin  outputs/object outputs/runtime
pushd .; cd outputs/jasmin; ls -ltr ../../inputs/*.cmm  | awk -F" " {'print $9'} | xargs -n 1 -I {} ../../Cmm -compile {}; popd
pushd .; cd outputs/jasmin; ls -ltr *.j  | awk -F" " {'print $9'} | xargs -n 1 -I {} jasmin {} -d ../object; popd
pushd .; cd outputs/object; ls -ltr *.class  | awk -F" " {'print $9'}  | awk -F"." {'print $1'} | xargs -n 1 -I {} bash -c "java {} > ../runtime/{}.out"; popd
```


### Generate sample outputs
```bash
./Cmm -compile inputs/HelloWorld.cmm
jasmin HelloWorld.j
java HelloWorld

./Cmm -compile inputs/TestDoWhile.cmm
jasmin TestDoWhile.j
java TestDoWhile

./Cmm -compile inputs/TestFor.cmm
jasmin TestFor.j
java TestFor

./Cmm -compile inputs/TestIf.cmm
jasmin TestIf.j
java TestIf

./Cmm -compile inputs/TestProcedure.cmm
jasmin TestProcedure.j
java TestProcedure

./Cmm -compile inputs/TestSwitch.cmm
jasmin TestSwitch.j
java TestSwitch

./Cmm -compile inputs/TestWhile.cmm
jasmin TestWhile.j
java TestWhile

./Cmm -compile inputs/Newton3.cmm
jasmin Newton3.j
java Newton3
```