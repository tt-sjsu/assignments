#ifndef SEMANTICS_H_
#define SEMANTICS_H_

#include <map>

#include "CmmBaseVisitor.h"
#include "antlr4-runtime.h"

#include "intermediate/symtab/SymtabStack.h"
#include "intermediate/symtab/SymtabEntry.h"
#include "intermediate/symtab/Predefined.h"
#include "intermediate/type/Typespec.h"
#include "intermediate/util/BackendMode.h"
#include "SemanticErrorHandler.h"

namespace frontend {

using namespace std;
using namespace intermediate::symtab;
using namespace intermediate::type;

class Semantics : public CmmBaseVisitor
{
private:
    BackendMode mode;
    SymtabStack *symtabStack;
    SymtabEntry *programId;
    SemanticErrorHandler error;

    map<string, Typespec *> *typeTable;

    /**
     * Return the number of values in a datatype.
     * @param type the datatype.
     * @return the number of values.
     */
    int typeCount(Typespec *type);

    /**
     * Determine whether or not an expression is a variable only.
     * @param exprCtx the ExpressionContext.
     * @return true if it's an expression only, else false.
     */
    bool expressionIsVariable(CmmParser::ExpressionContext *exprCtx);

    /**
     * Perform semantic operations on procedure and function call arguments.
     * @param listCtx the ArgumentListContext.
     * @param parameters the vector of parameters to fill.
     */
    void checkCallArguments(CmmParser::ArgumentListContext *listCtx,
                            vector<SymtabEntry *> *parms);

    /**
     * Determine the datatype of a variable that can have modifiers.
     * @param varCtx the VariableContext.
     * @param varType the variable's datatype without the modifiers.
     * @return the datatype with any modifiers.
     */
    Typespec *variableDatatype(CmmParser::VariableContext *varCtx,
                               Typespec *varType);

    /**
     * Create a new record type.
     * @param recordTypeSpecCtx the RecordTypespecContext.
     * @param recordTypeName the name of the record type.
     * @return the symbol table entry of the record type identifier.
     */
    SymtabEntry *createRecordType(
                        CmmParser::RecordTypespecContext *recordTypeSpecCtx,
                        string recordTypeName);

    /**
     * Create the fully qualified type pathname of a record type.
     * @param recordType the record type.
     * @return the pathname.
     */
    string createRecordTypePath(Typespec *recordType);

    /**
     * Create the symbol table for a record type.
     * @param ctx the RecordFieldsContext,
     * @param ownerId the symbol table entry of the owner's identifier.
     * @return the symbol table.
     */
    Symtab *createRecordSymtab(
                CmmParser::RecordFieldsContext *ctx, SymtabEntry *ownerId);

public:
    Semantics(BackendMode mode) : mode(mode), programId(nullptr)
    {
        // Create and initialize the symbol table stack.
        symtabStack = new SymtabStack();
        Predefined::initialize(symtabStack);

        typeTable = new map<string, Typespec *>();
        (*typeTable)["integer"] = Predefined::integerType;
        (*typeTable)["real"]    = Predefined::realType;
        (*typeTable)["boolean"] = Predefined::booleanType;
        (*typeTable)["char"]    = Predefined::charType;
        (*typeTable)["string"]  = Predefined::stringType;
    }

    /**
     * Get the symbol table entry of the program identifier.
     * @return the entry.
     */
    SymtabEntry *getProgramId() { return programId; }

    /**
     * Get the count of semantic errors.
     * @return the count.
     */
    int getErrorCount() const { return error.getCount(); }

    /**
     * Return the default value for a given datatype.
     * @param type the datatype.
     */
    static Object defaultValue(Typespec *type);

    Object visitProgram(CmmParser::ProgramContext *ctx) override;
    Object visitProgramHeader(CmmParser::ProgramHeaderContext *ctx) override;
    Object visitConstantDefinition(CmmParser::ConstantDefinitionContext *ctx) override;
    Object visitConstant(CmmParser::ConstantContext *ctx) override;
    Object visitTypeDefinition(CmmParser::TypeDefinitionContext *ctx) override;
    Object visitRecordTypespec(CmmParser::RecordTypespecContext *ctx) override;
    Object visitSimpleTypespec(CmmParser::SimpleTypespecContext *ctx) override;
    Object visitTypeIdentifierTypespec(CmmParser::TypeIdentifierTypespecContext *ctx) override;
    Object visitTypeIdentifier(CmmParser::TypeIdentifierContext *ctx) override;
    Object visitEnumerationTypespec(CmmParser::EnumerationTypespecContext *ctx) override;
    Object visitSubrangeTypespec(CmmParser::SubrangeTypespecContext *ctx) override;
    Object visitArrayTypespec(CmmParser::ArrayTypespecContext *ctx) override;
    Object visitVariableDeclarations(CmmParser::VariableDeclarationsContext *ctx) override;
    Object visitRoutineDefinition(CmmParser::RoutineDefinitionContext *ctx) override;
    Object visitParameterDeclarationsList(CmmParser::ParameterDeclarationsListContext *ctx) override;
    Object visitParameterDeclarations(CmmParser::ParameterDeclarationsContext *ctx) override;
    Object visitNormalAssignment(CmmParser::NormalAssignmentContext *ctx) override;
    Object visitLhs(CmmParser::LhsContext *ctx) override;
    Object visitIfStatement(CmmParser::IfStatementContext *ctx) override;
    Object visitCaseStatement(CmmParser::CaseStatementContext *ctx) override;
    Object visitRepeatStatement(CmmParser::RepeatStatementContext *ctx) override;
    Object visitWhileStatement(CmmParser::WhileStatementContext *ctx) override;
    Object visitForStatement(CmmParser::ForStatementContext *ctx) override;
    Object visitProcedureCallStatement(CmmParser::ProcedureCallStatementContext *ctx) override;
    Object visitFunctionCallFactor(CmmParser::FunctionCallFactorContext *ctx) override;
    Object visitExpression(CmmParser::ExpressionContext *ctx) override;
    Object visitSimpleExpression(CmmParser::SimpleExpressionContext *ctx) override;
    Object visitTerm(CmmParser::TermContext *ctx) override;
    Object visitVariableFactor(CmmParser::VariableFactorContext *ctx) override;
    Object visitVariable(CmmParser::VariableContext *ctx) override;
    Object visitVariableIdentifier(CmmParser::VariableIdentifierContext *ctx) override;
    Object visitNumberFactor(CmmParser::NumberFactorContext *ctx) override;
    Object visitCharacterFactor(CmmParser::CharacterFactorContext *ctx) override;
    Object visitStringFactor(CmmParser::StringFactorContext *ctx) override;
    Object visitNotFactor(CmmParser::NotFactorContext *ctx) override;
    Object visitParenthesizedFactor(CmmParser::ParenthesizedFactorContext *ctx) override;
};

} // namespace frontend

#endif /* SEMANTICS_H_ */
