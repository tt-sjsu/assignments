.class public TestFor
.super java/lang/Object

.field private static _sysin Ljava/util/Scanner;
.field private static ch C
.field private static i I
.field private static j I

;
; Runtime input scanner
;
.method static <clinit>()V

	new	java/util/Scanner
	dup
	getstatic	java/lang/System/in Ljava/io/InputStream;
	invokespecial	java/util/Scanner/<init>(Ljava/io/InputStream;)V
	putstatic	TestFor/_sysin Ljava/util/Scanner;
	return

.limit locals 0
.limit stack 3
.end method

;
; Main class constructor
;
.method public <init>()V
.var 0 is this LTestFor;

	aload_0
	invokespecial	java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

;
; PROCEDURE proc
;
.method private static proc(I)V

.var 1 is i I
.var 0 is limit I
;
; 008 for(i=1;i<=limit;i++)println('i = ',i)
;
L001:
;
; 008 i=1
;
	iconst_1
	istore_1
L004:
	iload_1
	iload_0
	if_icmple	L005
	iconst_0
	goto	L006
L005:
	iconst_1
L006:
	ifeq	L002
;
; 008 println('i = ',i)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_1
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 008 i++
;
	iload_1
	iconst_1
	iadd
	istore_1
	goto	L004
L002:
;
; 009 println
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	invokevirtual	java/io/PrintStream.println()V

	return

.limit locals 2
.limit stack 6
.end method

;
; PROCEDURE _main
;
.method private static _main()V

;
; 014 proc(5)
;
	iconst_5
	invokestatic	TestFor/proc(I)V
;
; 016 for(i=1;i<=3;i++){for(j=4;j>=1;j--){println('i = ',i,', j = ',j);}}
;
L007:
;
; 016 i=1
;
	iconst_1
	putstatic	TestFor/i I
L010:
	getstatic	TestFor/i I
	iconst_3
	if_icmple	L011
	iconst_0
	goto	L012
L011:
	iconst_1
L012:
	ifeq	L008
;
; 017 for(j=4;j>=1;j--){println('i = ',i,', j = ',j);}
;
L013:
;
; 017 j=4
;
	iconst_4
	putstatic	TestFor/j I
L016:
	getstatic	TestFor/j I
	iconst_1
	if_icmpge	L017
	iconst_0
	goto	L018
L017:
	iconst_1
L018:
	ifeq	L014
;
; 018 println('i = ',i,', j = ',j)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, j = %d\n"
	iconst_2
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestFor/i I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	getstatic	TestFor/j I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 017 j--
;
	getstatic	TestFor/j I
	iconst_1
	isub
	putstatic	TestFor/j I
	goto	L016
L014:
;
; 016 i++
;
	getstatic	TestFor/i I
	iconst_1
	iadd
	putstatic	TestFor/i I
	goto	L010
L008:
;
; 022 println
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	invokevirtual	java/io/PrintStream.println()V
;
; 024 for(ch='a';ch<='z';ch++)printf(ch)
;
L019:
;
; 024 ch='a'
;
	bipush	97
	putstatic	TestFor/ch C
L022:
	getstatic	TestFor/ch C
	bipush	122
	if_icmple	L023
	iconst_0
	goto	L024
L023:
	iconst_1
L024:
	ifeq	L020
;
; 024 printf(ch)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"%c"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestFor/ch C
	invokestatic	java/lang/Character/valueOf(C)Ljava/lang/Character;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 024 ch++
;
	getstatic	TestFor/ch C
	iconst_1
	iadd
	putstatic	TestFor/ch C
	goto	L022
L020:
;
; 025 println
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	invokevirtual	java/io/PrintStream.println()V
;
; 027 for(ch='Z';ch>='A';ch--)printf(ch)
;
L025:
;
; 027 ch='Z'
;
	bipush	90
	putstatic	TestFor/ch C
L028:
	getstatic	TestFor/ch C
	bipush	65
	if_icmpge	L029
	iconst_0
	goto	L030
L029:
	iconst_1
L030:
	ifeq	L026
;
; 027 printf(ch)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"%c"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestFor/ch C
	invokestatic	java/lang/Character/valueOf(C)Ljava/lang/Character;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 027 ch--
;
	getstatic	TestFor/ch C
	iconst_1
	isub
	putstatic	TestFor/ch C
	goto	L028
L026:
;
; 028 println
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	invokevirtual	java/io/PrintStream.println()V

	return

.limit locals 0
.limit stack 7
.end method

;
; MAIN
;
.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String;
.var 1 is _start Ljava/time/Instant;
.var 2 is _end Ljava/time/Instant;
.var 3 is _elapsed J

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_1
	invokestatic	TestFor/_main()V

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_2
	aload_1
	aload_2
	invokestatic	java/time/Duration/between(Ljava/time/temporal/Temporal;Ljava/time/temporal/Temporal;)Ljava/time/Duration;
	invokevirtual	java/time/Duration/toMillis()J
	lstore_3
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"\n[%,d milliseconds execution time.]\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	lload_3
	invokestatic	java/lang/Long/valueOf(J)Ljava/lang/Long;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop

	return

.limit locals 6
.limit stack 8
.end method
