.class public Newton3
.super java/lang/Object

.field private static _sysin Ljava/util/Scanner;
.field private static number I

;
; Runtime input scanner
;
.method static <clinit>()V

	new	java/util/Scanner
	dup
	getstatic	java/lang/System/in Ljava/io/InputStream;
	invokespecial	java/util/Scanner/<init>(Ljava/io/InputStream;)V
	putstatic	Newton3/_sysin Ljava/util/Scanner;
	return

.limit locals 0
.limit stack 3
.end method

;
; Main class constructor
;
.method public <init>()V
.var 0 is this LNewton3;

	aload_0
	invokespecial	java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

;
; FUNCTION root
;
.method private static root(F)F

.var 3 is diff F
.var 2 is prev F
.var 1 is r F
.var 4 is root F
.var 0 is x F
;
; 007 r=1
;
	iconst_1
	i2f
	fstore_1
;
; 008 prev=0
;
	iconst_0
	i2f
	fstore_2
;
; 010 do{r=(x/r+r)/2;diff=r-prev;if(diff<0)diff=-diff;prev=r;}while(diff<1.0 ...
;
L001:
;
; 011 r=(x/r+r)/2
;
	fload_0
	fload_1
	fdiv
	fload_1
	fadd
	iconst_2
	i2f
	fdiv
	fstore_1
;
; 012 diff=r-prev
;
	fload_1
	fload_2
	fsub
	fstore_3
;
; 013 if(diff<0)diff=-diff
;
L003:
	fload_3
	iconst_0
	i2f
	fcmpg
	iflt	L006
	iconst_0
	goto	L007
L006:
	iconst_1
L007:
	ifne	L005
	goto	L004
L005:
;
; 013 diff=-diff
;
	fload_3
	fneg
	fstore_3
L004:
;
; 014 prev=r
;
	fload_1
	fstore_2
	fload_3
	ldc	1.01e-10
	fcmpg
	iflt	L008
	iconst_0
	goto	L009
L008:
	iconst_1
L009:
	ifne	L002
	goto	L001
L002:
;
; 017 returnr
;
	fload_1
	freturn

	fload	4
	freturn

.limit locals 5
.limit stack 2
.end method

;
; PROCEDURE print1
;
.method private static print1(IF)V

.var 0 is n I
.var 1 is root F
;
; 022 println('The square root of ',n:4,' is ',root:8:4)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"The square root of %4d is %8.4f\n"
	iconst_2
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_0
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	fload_1
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop

	return

.limit locals 2
.limit stack 6
.end method

;
; PROCEDURE _main
;
.method private static _main()V

;
; 026 for(number=1;number<=25;number++){print1(number,root(number));}
;
L010:
;
; 026 number=1
;
	iconst_1
	putstatic	Newton3/number I
L013:
	getstatic	Newton3/number I
	bipush	25
	if_icmple	L014
	iconst_0
	goto	L015
L014:
	iconst_1
L015:
	ifeq	L011
;
; 027 print1(number,root(number))
;
	getstatic	Newton3/number I
	getstatic	Newton3/number I
	i2f
	invokestatic	Newton3/root(F)F
	invokestatic	Newton3/print1(IF)V
;
; 026 number++
;
	getstatic	Newton3/number I
	iconst_1
	iadd
	putstatic	Newton3/number I
	goto	L013
L011:

	return

.limit locals 0
.limit stack 4
.end method

;
; MAIN
;
.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String;
.var 1 is _start Ljava/time/Instant;
.var 2 is _end Ljava/time/Instant;
.var 3 is _elapsed J

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_1
	invokestatic	Newton3/_main()V

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_2
	aload_1
	aload_2
	invokestatic	java/time/Duration/between(Ljava/time/temporal/Temporal;Ljava/time/temporal/Temporal;)Ljava/time/Duration;
	invokevirtual	java/time/Duration/toMillis()J
	lstore_3
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"\n[%,d milliseconds execution time.]\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	lload_3
	invokestatic	java/lang/Long/valueOf(J)Ljava/lang/Long;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop

	return

.limit locals 6
.limit stack 8
.end method
