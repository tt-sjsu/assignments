.class public TestCase
.super java/lang/Object

.field private static _sysin Ljava/util/Scanner;
.field private static ch C
.field private static even I
.field private static i I
.field private static j I
.field private static odd I
.field private static prime I
.field private static str C

;
; Runtime input scanner
;
.method static <clinit>()V

	new	java/util/Scanner
	dup
	getstatic	java/lang/System/in Ljava/io/InputStream;
	invokespecial	java/util/Scanner/<init>(Ljava/io/InputStream;)V
	putstatic	TestCase/_sysin Ljava/util/Scanner;
	return

.limit locals 0
.limit stack 3
.end method

;
; Main class constructor
;
.method public <init>()V
.var 0 is this LTestCase;

	aload_0
	invokespecial	java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

;
; PROCEDURE _main
;
.method private static _main()V

;
; 008 i=3
;
	iconst_3
	putstatic	TestCase/i I
;
; 008 ch='b'
;
	bipush	98
	putstatic	TestCase/ch C
;
; 009 even=-990
;
	sipush	990
	ineg
	putstatic	TestCase/even I
;
; 009 odd=-999
;
	sipush	999
	ineg
	putstatic	TestCase/odd I
;
; 009 prime=0
;
	iconst_0
	putstatic	TestCase/prime I
;
; 011 switch(i+1){case1:j=i;case-8:j=8*i;case5,case7,case4:j=574*i;}
;
L001:
	getstatic	TestCase/i I
	iconst_1
	iadd
	lookupswitch
	  -8: L004
	  1: L003
	  4: L005
	  5: L005
	  7: L005
	  default: L002
L003:
;
; 012 j=i
;
	getstatic	TestCase/i I
	putstatic	TestCase/j I
	goto	L002
L004:
;
; 013 j=8*i
;
	bipush	8
	getstatic	TestCase/i I
	imul
	putstatic	TestCase/j I
	goto	L002
L005:
;
; 016 j=574*i
;
	sipush	574
	getstatic	TestCase/i I
	imul
	putstatic	TestCase/j I
	goto	L002
L002:
;
; 019 println('j = ',j)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"j = %d\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestCase/j I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 021 switch(ch){case'c',case'b':str='p';case'a':str='q'}
;
L006:
	getstatic	TestCase/ch C
	lookupswitch
	  97: L009
	  98: L008
	  99: L008
	  default: L007
L008:
;
; 023 str='p'
;
	bipush	112
	putstatic	TestCase/str C
	goto	L007
L009:
;
; 024 str='q'
;
	bipush	113
	putstatic	TestCase/str C
	goto	L007
L007:
;
; 027 println('str = ''',str,'''')
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"str = '%c'\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestCase/str C
	invokestatic	java/lang/Character/valueOf(C)Ljava/lang/Character;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 029 for(i=-5;i<=15;i=i+1){switch(i){case2:prime=i;case-4,case-2,case0,case ...
;
L010:
;
; 029 i=-5
;
	iconst_5
	ineg
	putstatic	TestCase/i I
L013:
	getstatic	TestCase/i I
	bipush	15
	if_icmple	L014
	iconst_0
	goto	L015
L014:
	iconst_1
L015:
	ifeq	L011
;
; 030 switch(i){case2:prime=i;case-4,case-2,case0,case4,case6,case8,case10,c ...
;
L016:
	getstatic	TestCase/i I
	lookupswitch
	  -4: L019
	  -3: L020
	  -2: L019
	  -1: L020
	  0: L019
	  1: L020
	  2: L018
	  3: L020
	  4: L019
	  5: L020
	  6: L019
	  7: L020
	  8: L019
	  9: L020
	  10: L019
	  11: L020
	  12: L019
	  default: L017
L018:
;
; 031 prime=i
;
	getstatic	TestCase/i I
	putstatic	TestCase/prime I
	goto	L017
L019:
;
; 032 even=i
;
	getstatic	TestCase/i I
	putstatic	TestCase/even I
	goto	L017
L020:
;
; 033 switch(i){case-3,case-1,case1,case9:odd=i;case2,case3,case5,case7,case ...
;
L021:
	getstatic	TestCase/i I
	lookupswitch
	  -3: L023
	  -1: L023
	  1: L023
	  2: L024
	  3: L024
	  5: L024
	  7: L024
	  9: L023
	  11: L024
	  default: L022
L023:
;
; 034 odd=i
;
	getstatic	TestCase/i I
	putstatic	TestCase/odd I
	goto	L022
L024:
;
; 035 prime=i
;
	getstatic	TestCase/i I
	putstatic	TestCase/prime I
	goto	L022
L022:
	goto	L017
L017:
;
; 039 println('i = ',i,', even = ',even,', odd = ',odd,', prime = ',prime)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, even = %d, odd = %d, prime = %d\n"
	iconst_4
	anewarray	java/lang/Object
	dup
	iconst_0
	getstatic	TestCase/i I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	getstatic	TestCase/even I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_2
	getstatic	TestCase/odd I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_3
	getstatic	TestCase/prime I
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 029 i=i+1
;
	getstatic	TestCase/i I
	iconst_1
	iadd
	putstatic	TestCase/i I
	goto	L013
L011:

	return

.limit locals 0
.limit stack 6
.end method

;
; MAIN
;
.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String;
.var 1 is _start Ljava/time/Instant;
.var 2 is _end Ljava/time/Instant;
.var 3 is _elapsed J

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_1
	invokestatic	TestCase/_main()V

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_2
	aload_1
	aload_2
	invokestatic	java/time/Duration/between(Ljava/time/temporal/Temporal;Ljava/time/temporal/Temporal;)Ljava/time/Duration;
	invokevirtual	java/time/Duration/toMillis()J
	lstore_3
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"\n[%,d milliseconds execution time.]\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	lload_3
	invokestatic	java/lang/Long/valueOf(J)Ljava/lang/Long;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop

	return

.limit locals 6
.limit stack 8
.end method
