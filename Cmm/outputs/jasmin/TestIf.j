.class public TestIf
.super java/lang/Object

.field private static _sysin Ljava/util/Scanner;

;
; Runtime input scanner
;
.method static <clinit>()V

	new	java/util/Scanner
	dup
	getstatic	java/lang/System/in Ljava/io/InputStream;
	invokespecial	java/util/Scanner/<init>(Ljava/io/InputStream;)V
	putstatic	TestIf/_sysin Ljava/util/Scanner;
	return

.limit locals 0
.limit stack 3
.end method

;
; Main class constructor
;
.method public <init>()V
.var 0 is this LTestIf;

	aload_0
	invokespecial	java/lang/Object/<init>()V
	return

.limit locals 1
.limit stack 1
.end method

;
; PROCEDURE _main
;
.method private static _main()V

.var 0 is i I
.var 1 is j I
.var 2 is k I
.var 5 is p Z
.var 6 is q Z
.var 7 is str Ljava/lang/String;
.var 8 is txt Ljava/lang/String;
.var 3 is x F
.var 4 is y F
;
; 009 i=1
;
	iconst_1
	istore_0
;
; 010 j=2
;
	iconst_2
	istore_1
;
; 012 if(i=j)x=3.14elsex=-5
;
L001:
	iload_0
	iload_1
	if_icmpeq	L004
	iconst_0
	goto	L005
L004:
	iconst_1
L005:
	ifne	L003
;
; 013 x=-5
;
	iconst_5
	ineg
	i2f
	fstore_3
	goto	L002
L003:
;
; 012 x=3.14
;
	ldc	3.14
	fstore_3
L002:
;
; 015 if(i!=j)y=3.14elsey=-5
;
L006:
	iload_0
	iload_1
	if_icmpne	L009
	iconst_0
	goto	L010
L009:
	iconst_1
L010:
	ifne	L008
;
; 016 y=-5
;
	iconst_5
	ineg
	i2f
	fstore	4
	goto	L007
L008:
;
; 015 y=3.14
;
	ldc	3.14
	fstore	4
L007:
;
; 018 println('i = ',i,', j = ',j,', x = ',x:5:2,', y = ',y:5:2)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, j = %d, x = %5.2f, y = %5.2f\n"
	iconst_4
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_0
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	iload_1
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_2
	fload_3
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	dup
	iconst_3
	fload	4
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 020 if(i=j){x=-7}else{x=8;}
;
L011:
	iload_0
	iload_1
	if_icmpeq	L014
	iconst_0
	goto	L015
L014:
	iconst_1
L015:
	ifne	L013
;
; 024 x=8
;
	bipush	8
	i2f
	fstore_3
	goto	L012
L013:
;
; 021 x=-7
;
	bipush	7
	ineg
	i2f
	fstore_3
L012:
;
; 027 if(i!=j){y=14}else{y=-2;}
;
L016:
	iload_0
	iload_1
	if_icmpne	L019
	iconst_0
	goto	L020
L019:
	iconst_1
L020:
	ifne	L018
;
; 031 y=-2
;
	iconst_2
	ineg
	i2f
	fstore	4
	goto	L017
L018:
;
; 028 y=14
;
	bipush	14
	i2f
	fstore	4
L017:
;
; 034 println('i = ',i,', j = ',j,', x = ',x:5:2,', y = ',y:5:2)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, j = %d, x = %5.2f, y = %5.2f\n"
	iconst_4
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_0
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	iload_1
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_2
	fload_3
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	dup
	iconst_3
	fload	4
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 036 p=i=j
;
	iload_0
	iload_1
	if_icmpeq	L021
	iconst_0
	goto	L022
L021:
	iconst_1
L022:
	istore	5
;
; 037 if(p)x=55.55else{x=77.77;y=88.88;}
;
L023:
	iload	5
	ifne	L025
;
; 039 x=77.77
;
	ldc	77.77
	fstore_3
;
; 040 y=88.88
;
	ldc	88.88
	fstore	4
	goto	L024
L025:
;
; 037 x=55.55
;
	ldc	55.55
	fstore_3
L024:
;
; 043 println('i = ',i,', j = ',j,', x = ',x:5:2,', y = ',y:5:2)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, j = %d, x = %5.2f, y = %5.2f\n"
	iconst_4
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_0
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	iload_1
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_2
	fload_3
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	dup
	iconst_3
	fload	4
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 045 q=i<=j
;
	iload_0
	iload_1
	if_icmple	L026
	iconst_0
	goto	L027
L026:
	iconst_1
L027:
	istore	6
;
; 046 k=10
;
	bipush	10
	istore_2
;
; 048 if(p)i=33elseif(!q)i=44elseif(i=j)i=55elsei=6
;
L028:
	iload	5
	ifne	L030
;
; 048 if(!q)i=44elseif(i=j)i=55elsei=6
;
L031:
	iload	6
	iconst_1
	ixor
	ifne	L033
;
; 048 if(i=j)i=55elsei=6
;
L034:
	iload_0
	iload_1
	if_icmpeq	L037
	iconst_0
	goto	L038
L037:
	iconst_1
L038:
	ifne	L036
;
; 048 i=6
;
	bipush	6
	istore_0
	goto	L035
L036:
;
; 048 i=55
;
	bipush	55
	istore_0
L035:
	goto	L032
L033:
;
; 048 i=44
;
	bipush	44
	istore_0
L032:
	goto	L029
L030:
;
; 048 i=33
;
	bipush	33
	istore_0
L029:
;
; 049 if(!p)if(q)j=9elsej=-9
;
L039:
	iload	5
	iconst_1
	ixor
	ifne	L041
	goto	L040
L041:
;
; 049 if(q)j=9elsej=-9
;
L042:
	iload	6
	ifne	L044
;
; 049 j=-9
;
	bipush	9
	ineg
	istore_1
	goto	L043
L044:
;
; 049 j=9
;
	bipush	9
	istore_1
L043:
L040:
;
; 050 if(p)if(q)k=11elsek=12
;
L045:
	iload	5
	ifne	L047
	goto	L046
L047:
;
; 050 if(q)k=11elsek=12
;
L048:
	iload	6
	ifne	L050
;
; 050 k=12
;
	bipush	12
	istore_2
	goto	L049
L050:
;
; 050 k=11
;
	bipush	11
	istore_2
L049:
L046:
;
; 052 println('i = ',i,', j = ',j,', x = ',x:5:2,', y = ',y:5:2)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"i = %d, j = %d, x = %5.2f, y = %5.2f\n"
	iconst_4
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_0
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_1
	iload_1
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	dup
	iconst_2
	fload_3
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	dup
	iconst_3
	fload	4
	invokestatic	java/lang/Float/valueOf(F)Ljava/lang/Float;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 053 println('k = ',k)
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"k = %d\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	iload_2
	invokestatic	java/lang/Integer/valueOf(I)Ljava/lang/Integer;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop
;
; 055 println
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	invokevirtual	java/io/PrintStream.println()V
;
; 056 if(!pandqand(i!=j)and(x<y))printf('Good-bye')
;
L051:
	iload	5
	iconst_1
	ixor
	iload	6
	iand
	iload_0
	iload_1
	if_icmpne	L054
	iconst_0
	goto	L055
L054:
	iconst_1
L055:
	iand
	fload_3
	fload	4
	fcmpg
	iflt	L056
	iconst_0
	goto	L057
L056:
	iconst_1
L057:
	iand
	ifne	L053
	goto	L052
L053:
;
; 056 printf('Good-bye')
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"Good-bye"
	invokevirtual	java/io/PrintStream/print(Ljava/lang/String;)V
L052:
;
; 057 if(!p)if(q)if(i!=j)if(x<y)println(', world!')
;
L058:
	iload	5
	iconst_1
	ixor
	ifne	L060
	goto	L059
L060:
;
; 057 if(q)if(i!=j)if(x<y)println(', world!')
;
L061:
	iload	6
	ifne	L063
	goto	L062
L063:
;
; 057 if(i!=j)if(x<y)println(', world!')
;
L064:
	iload_0
	iload_1
	if_icmpne	L067
	iconst_0
	goto	L068
L067:
	iconst_1
L068:
	ifne	L066
	goto	L065
L066:
;
; 057 if(x<y)println(', world!')
;
L069:
	fload_3
	fload	4
	fcmpg
	iflt	L072
	iconst_0
	goto	L073
L072:
	iconst_1
L073:
	ifne	L071
	goto	L070
L071:
;
; 057 println(', world!')
;
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	", world!\n"
	invokevirtual	java/io/PrintStream/print(Ljava/lang/String;)V
L070:
L065:
L062:
L059:

	return

.limit locals 9
.limit stack 6
.end method

;
; MAIN
;
.method public static main([Ljava/lang/String;)V
.var 0 is args [Ljava/lang/String;
.var 1 is _start Ljava/time/Instant;
.var 2 is _end Ljava/time/Instant;
.var 3 is _elapsed J

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_1
	invokestatic	TestIf/_main()V

	invokestatic	java/time/Instant/now()Ljava/time/Instant;
	astore_2
	aload_1
	aload_2
	invokestatic	java/time/Duration/between(Ljava/time/temporal/Temporal;Ljava/time/temporal/Temporal;)Ljava/time/Duration;
	invokevirtual	java/time/Duration/toMillis()J
	lstore_3
	getstatic	java/lang/System/out Ljava/io/PrintStream;
	ldc	"\n[%,d milliseconds execution time.]\n"
	iconst_1
	anewarray	java/lang/Object
	dup
	iconst_0
	lload_3
	invokestatic	java/lang/Long/valueOf(J)Ljava/lang/Long;
	aastore
	invokevirtual	java/io/PrintStream/printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;
	pop

	return

.limit locals 6
.limit stack 8
.end method
