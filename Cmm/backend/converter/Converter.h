#ifndef CONVERTER_H_
#define CONVERTER_H_

#include <map>
#include <vector>

#include "CmmBaseVisitor.h"
#include "antlr4-runtime.h"

#include "intermediate/symtab/SymtabStack.h"
#include "intermediate/symtab/SymtabEntry.h"
#include "intermediate/type/Typespec.h"
#include "CodeGenerator.h"

namespace backend { namespace converter {

using namespace std;
using namespace intermediate::symtab;
using namespace intermediate::type;

class Converter : public CmmBaseVisitor
{
private:
    CodeGenerator code;
    bool programVariables;
    bool recordFields;
    string currentSeparator;

public:
    Converter()
        : programVariables(true), recordFields(false),
          currentSeparator("")
    {
        typeNameTable["integer"] = "int";
        typeNameTable["real"]    = "double";
        typeNameTable["boolean"] = "bool";
        typeNameTable["char"]    = "char";
        typeNameTable["string"]  = "string";
    }

    /**
     * Get the name of the object (Java) file.
     * @return the name.
     */
    string getObjectFileName() const { return code.getObjectFileName(); }

    Object visitProgram(CmmParser::ProgramContext *ctx) override;
    Object visitProgramHeader(CmmParser::ProgramHeaderContext *ctx) override;
    Object visitConstantDefinition(CmmParser::ConstantDefinitionContext *ctx) override;
    Object visitTypeDefinition(CmmParser::TypeDefinitionContext *ctx) override;
    Object visitEnumerationTypespec(CmmParser::EnumerationTypespecContext *ctx) override;
    Object visitRecordTypespec(CmmParser::RecordTypespecContext *ctx) override;
    Object visitVariableDeclarations(CmmParser::VariableDeclarationsContext *ctx) override;
    Object visitTypeIdentifier(CmmParser::TypeIdentifierContext *ctx) override;
    Object visitVariableIdentifierList(CmmParser::VariableIdentifierListContext *ctx) override;
    Object visitRoutineDefinition(CmmParser::RoutineDefinitionContext *ctx) override;
    Object visitParameters(CmmParser::ParametersContext *ctx) override;
    Object visitParameterDeclarations(CmmParser::ParameterDeclarationsContext *ctx) override;
    Object visitStatementList(CmmParser::StatementListContext *ctx) override;
    Object visitCompoundStatement(CmmParser::CompoundStatementContext *ctx) override;
//    Object visitAssignmentStatement(CmmParser::AssignmentStatementContext *ctx) override;
    Object visitIfStatement(CmmParser::IfStatementContext *ctx) override;
    Object visitCaseStatement(CmmParser::CaseStatementContext *ctx) override;
    Object visitCaseBranch(CmmParser::CaseBranchContext *ctx) override;
    Object visitRepeatStatement(CmmParser::RepeatStatementContext *ctx) override;
    Object visitWhileStatement(CmmParser::WhileStatementContext *ctx) override;
    Object visitForStatement(CmmParser::ForStatementContext *ctx) override;
    Object visitProcedureCallStatement(CmmParser::ProcedureCallStatementContext *ctx) override;
    Object visitArgumentList(CmmParser::ArgumentListContext *ctx) override;
    Object visitExpression(CmmParser::ExpressionContext *ctx) override;
    Object visitSimpleExpression(CmmParser::SimpleExpressionContext *ctx) override;
    Object visitTerm(CmmParser::TermContext *ctx) override;
    Object visitVariableFactor(CmmParser::VariableFactorContext *ctx) override;
    Object visitVariable(CmmParser::VariableContext *ctx) override;
    Object visitNumberFactor(CmmParser::NumberFactorContext *ctx) override;
    Object visitCharacterFactor(CmmParser::CharacterFactorContext *ctx) override;
    Object visitStringFactor(CmmParser::StringFactorContext *ctx) override;
    Object visitFunctionCallFactor(CmmParser::FunctionCallFactorContext *context) override;
    Object visitNotFactor(CmmParser::NotFactorContext *ctx) override;
    Object visitParenthesizedFactor(CmmParser::ParenthesizedFactorContext *ctx) override;
    Object visitWriteStatement(CmmParser::WriteStatementContext *ctx) override;
    Object visitWritelnStatement(CmmParser::WritelnStatementContext *ctx) override;
    Object visitReadStatement(CmmParser::ReadStatementContext *ctx) override;
    Object visitReadlnStatement(CmmParser::ReadlnStatementContext *ctx) override;
    Object visitReadArguments(CmmParser::ReadArgumentsContext *ctx) override;

private:
    // Map a Cmm datatype name to the C++ datatype name.
    map<string, string> typeNameTable;

    /**
     * Map a Cmm type name to an equivalent C++ type name.
     */
    string mapTypeName(string CmmTypeName);

    /**
     * Convert a Cmm type name to the equivalent C++ type.
     * @param CmmType the datatype name.
     * @return the C++ type name.
     */
    string typeName(Typespec*CmmType);

    /**
     * Emit a variable declaration with allocation for an array or record.
     * @param type the datatype of the variable.
     * @param varId the symbol table entry of the variable.
     */
    void emitVariableDeclaration(Typespec *type, SymtabEntry *varId);

    /**
     * Emit code to allocate data for structured (array or record) variables.
     * @param lhsPrefix the prefix for the target variable name.
     * @param symtab the symbol table containing the variable names.
     */
    void emitAllocateStructuredVariables(string lhsPrefix, Symtab *symtab);

    /**
     * Emit code to allocate structured (array or record) data.
     * @param lhsPrefix the prefix for the target variable name.
     * @param variableId the symbol table entry of the target variable.
     */
    void emitAllocateStructuredData(string lhsPrefix, SymtabEntry *variableId);

    /**
     * Emit a string of bracketed dimension sizes for the array datatype.
     * @param type the array datatype.
     */
    void emitArrayDimensions(Typespec *type);

    /**
     * Emit code to allocate an array element.
     * @param lhsPrefix the prefix for the target variable name.
     * @param variableName the name of the target variable.
     * @param elmtType the element's datatype.
     */
    void emitNewArrayElement(string lhsPrefix, string variableName,
                             Typespec *elmtType);

    /**
     * Emit code to allocate a new record.
     * @param lhsPrefix the prefix for the target variable name.
     * @param variableName the name of the target variable.
     * @param recordType the record's datatype.
     */
    void emitNewRecord(string lhsPrefix, string variableName,
                       Typespec recordType);

    /**
     * Emit code to allocate a record's fields.
     * @param lhsPrefix the prefix for the target variable name.
     * @param recordType the record's datatype.
     */
    void emitNewRecordFields(string lhsPrefix, Typespec *recordType);

    /**
     * Emit a record type definition for an unnamed record.
     * @param symtab the symbol table that can contain unnamed records.
     */
    void emitUnnamedRecordDefinitions(Symtab *symtab);

    /**
     * Emit the record fields of a record.
     * @param symtab the symbol table of the unnamed record.
     */
    void emitRecordFields(Symtab *symtab);

    /**
     * Determine the datatype of a variable that can have modifiers.
     * @param varCtx the VariableContext.
     * @param varType the variable's datatype without the modifiers.
     * @return the datatype with any modifiers.
     */
    Typespec *variableDatatype(CmmParser::VariableContext *varCtx,
                               Typespec *varType);

    /**
     * Create the printf format string.
     * @param ctx the WriteArgumentsContext.
     * @return the format string.
     */
    string createWriteFormat(CmmParser::WriteArgumentsContext *ctx);

    /**
     * Create the string of write arguments.
     * @param ctx the WriteArgumentsContext.
     * @return the string of arguments.
     */
    string createWriteArguments(CmmParser::WriteArgumentsContext *ctx);
};

}} // namespace backend::converter

#endif /* CONVERTER_H_ */
