#ifndef EXECUTOR_H_
#define EXECUTOR_H_

#include "CmmBaseVisitor.h"
#include "antlr4-runtime.h"

#include "intermediate/symtab/SymtabStack.h"
#include "intermediate/symtab/SymtabEntry.h"
#include "intermediate/type/Typespec.h"
#include "RuntimeStack.h"
#include "RuntimeErrorHandler.h"

namespace backend { namespace interpreter {

using namespace std;
using namespace intermediate::symtab;
using namespace intermediate::type;

/**
 * Execute Cmm programs.
 */
class Executor : public CmmBaseVisitor
{
private:
    int executionCount;         // count of executed statements
    SymtabEntry *programId;     // program identifier's symbol table entry
    RuntimeStack runtimeStack;  // runtime stack
    RuntimeErrorHandler error;  // runtime error handler

public:
    Executor(SymtabEntry *programId) : executionCount(0), programId(programId) {}

    Object visitProgram(CmmParser::ProgramContext *ctx) override;
    Object visitStatement(CmmParser::StatementContext *ctx) override;
    Object visitNormalAssignment(CmmParser::NormalAssignmentContext *ctx) override;
    Object visitIncrementAssignment(CmmParser::IncrementAssignmentContext *ctx) override;
    Object visitDecrementAssignment(CmmParser::DecrementAssignmentContext *ctx) override;
    Object visitIfStatement(CmmParser::IfStatementContext *ctx) override;
    Object visitCaseStatement(CmmParser::CaseStatementContext *ctx) override;
    Object visitRepeatStatement(CmmParser::RepeatStatementContext *ctx) override;
    Object visitWhileStatement(CmmParser::WhileStatementContext *ctx) override;
    Object visitForStatement(CmmParser::ForStatementContext *ctx) override;
    Object visitProcedureCallStatement(CmmParser::ProcedureCallStatementContext *ctx) override;
    Object visitExpression(CmmParser::ExpressionContext *ctx) override;
    Object visitSimpleExpression(CmmParser::SimpleExpressionContext *ctx) override;
    Object visitTerm(CmmParser::TermContext *ctx) override;
    Object visitVariableFactor(CmmParser::VariableFactorContext *ctx) override;
    Object visitVariable(CmmParser::VariableContext *ctx) override;
    Object visitNumberFactor(CmmParser::NumberFactorContext *ctx) override;
    Object visitCharacterFactor(CmmParser::CharacterFactorContext *ctx) override;
    Object visitStringFactor(CmmParser::StringFactorContext *ctx) override;
    Object visitFunctionCallFactor(CmmParser::FunctionCallFactorContext *context) override;
    Object visitNotFactor(CmmParser::NotFactorContext *ctx) override;
    Object visitParenthesizedFactor(CmmParser::ParenthesizedFactorContext *ctx) override;
    Object visitWritelnStatement(CmmParser::WritelnStatementContext *ctx) override;
    Object visitWriteArguments(CmmParser::WriteArgumentsContext *ctx) override;
    Object visitReadlnStatement(CmmParser::ReadlnStatementContext *ctx) override;
    Object visitReadArguments(CmmParser::ReadArgumentsContext *ctx) override;
    Object visitReturnStatement(CmmParser::ReturnStatementContext *ctx) override;
    Object visitStatementList(CmmParser::StatementListContext *ctx) override;
private:
    /**
     * Assign a value to a target variable's memory cell.
     * @param varCtx the VariableContext of the target.
     * @param value the value to assign.
     * @param valueType the datatype of the value.
     * @return the target variable's memory cell.
     */
    Cell *assignValue(CmmParser::VariableContext *varCtx,
                      const Object& value, Typespec *valueType);

    /**
     * Assign a value to a target variable's memory cell.
     * @param targetCell the target variable's memory cell.
     * @param targetType the datatype of the target variable.
     * @param value the value to assign.
     * @param valueType the datatype of the value.
     */
    void assignValue(Cell *targetCell, Typespec *targetType,
                     const Object& value, Typespec *valueType);

    /**
     * Create the jump table for a CASE statement.
     * @param branchListCtx the CaseBranchListContext.
     * @return the jump table.
     */
    map<int, CmmParser::StatementContext*> *createJumpTable(
                            CmmParser::CaseBranchListContext *branchListCtx);

    /**
     * Execute procedure and function call arguments.
     * @param argListCtx the ArgumentListContext
     * @param parameters the routine's parameters.
     * @param frame the routine's stack frame.
     */
    void executeCallArguments(CmmParser::ArgumentListContext *argListCtx,
                              vector<SymtabEntry*> *parameters,
                              StackFrame *frame);
};

}}  // namespace backend::interpreter

#endif /* EXECUTOR_H_ */
