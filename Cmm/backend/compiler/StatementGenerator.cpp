#include <string>
#include <vector>
#include <map>

#include "CmmBaseVisitor.h"
#include "antlr4-runtime.h"

#include "intermediate/symtab/Predefined.h"
#include "Compiler.h"
#include "StatementGenerator.h"

namespace backend { namespace compiler {

using namespace std;
using namespace intermediate;

void StatementGenerator::emitAssignment(CmmParser::NormalAssignmentContext *ctx)
{
    CmmParser::VariableContext *varCtx  = ctx->lhs()->variable();
    CmmParser::ExpressionContext *exprCtx = ctx->rhs()->expression();
    handleAssignment(varCtx, exprCtx);
}

void StatementGenerator::handleAssignment(CmmParser::VariableContext *varCtx, CmmParser::ExpressionContext *exprCtx)
{
//    CmmParser::VariableContext *varCtx  = ctx->lhs()->variable();
//    CmmParser::ExpressionContext *exprCtx = ctx->rhs()->expression();
    SymtabEntry *varId = varCtx->entry;
    Typespec *varType  = varCtx->type;
    Typespec *exprType = exprCtx->type;

    // The last modifier, if any, is the variable's last subscript or field.
    int modifierCount = varCtx->modifier().size();
    CmmParser::ModifierContext *lastModCtx = modifierCount == 0
                                                ? nullptr : varCtx->modifier()[modifierCount - 1];

    // The target variable has subscripts and/or fields.
    if (modifierCount > 0)
    {
        lastModCtx = varCtx->modifier()[modifierCount - 1];
        compiler->visit(varCtx);
    }

    // Emit code to evaluate the expression.
    compiler->visit(exprCtx);

    // float variable := integer constant
    if (   (varType == Predefined::realType)
           && (exprType->baseType() == Predefined::integerType)) emit(I2F);

    // Emit code to store the expression value into the target variable.
    // The target variable has no subscripts or fields.
    if (lastModCtx == nullptr) emitStoreValue(varId, varId->getType());

        // The target variable is a field.
    else if (lastModCtx->field() != nullptr)
    {
        emitStoreValue(lastModCtx->field()->entry, lastModCtx->field()->type);
    }

        // The target variable is an array element.
    else
    {
        emitStoreValue(nullptr, varType);
    }
}

void StatementGenerator::emitVariableUpdate(CmmParser::VariableContext *varCtx, int value) {
    SymtabEntry *varId = varCtx->entry;
    Typespec *varType  = varCtx->type;
    emitLoadValue(varId);
    if (value == 1) {
        emit(Instruction::ICONST_1);
        emit(Instruction::IADD);
    }
    else if (value == -1) {
        emit(Instruction::ICONST_1);
        emit(Instruction::ISUB);
    }
    emitStoreValue(varId, varType);
}

void StatementGenerator::emitIf(CmmParser::IfStatementContext *ctx)
{
    /***** Complete this member function. *****/
    /*
     * ifStatement    : IF expression THEN trueStatement ( ELSE falseStatement )? ;
        trueStatement  : statement ;
        falseStatement : statement ;
     */
    Label *ifTopLabel  = new Label();
    Label *ifExitLabel = new Label();
    Label *ifTrueLabel = new Label();

    emitLabel(ifTopLabel);

    compiler->visit(ctx->expression());

    emit(IFNE, ifTrueLabel); // TRUE
    // FALSE STATEMENT
    if (ctx->falseStatement()) {
        compiler->visit(ctx->falseStatement());
    }
    emit(GOTO, ifExitLabel);

    // TRUE STATEMENT
    emitLabel(ifTrueLabel);
    compiler->visit(ctx->trueStatement());

    emitLabel(ifExitLabel);
}

bool sortCaseBranch(CaseBranchValue a, CaseBranchValue b) {
    return a.caseBranch < b.caseBranch;
}

void StatementGenerator::emitCase(CmmParser::CaseStatementContext *ctx)
{
    /***** Complete this member function. *****/
    /*
    CASE expression OF caseBranchList END ;
    caseBranchList   : caseBranch ( ';' caseBranch )* ;
    caseBranch       : caseConstantList ':' statement | ;
    caseConstantList : caseConstant ( ',' caseConstant )* ;
     */
    Label *caseTopLabel  = new Label();
    Label *caseExitLabel  = new Label();


    emitLabel(caseTopLabel);
    compiler->visit(ctx->expression());
    emit(LOOKUPSWITCH);
    vector<Label*> caseBranchLabels;
    vector<CaseBranchValue> caseBranchValue;

    for(int i = 0; i < ctx->caseBranchList()->caseBranch().size(); i++) {
        if (!ctx->caseBranchList()->caseBranch().at(i)->getText().length()) {
            continue;
        }
        Label *caseBranchLabel = new Label();
        caseBranchLabels.push_back(caseBranchLabel);
        auto caseConstants = ctx->caseBranchList()->caseBranch().at(i)->caseConstantList()->caseConstant();
        for (int j = 0; j < caseConstants.size(); j++) {
            caseBranchValue.push_back(CaseBranchValue{.caseBranch=caseConstants.at(j)->value, .label=caseBranchLabel});
        }
    }
    sort(caseBranchValue.begin(), caseBranchValue.end(), sortCaseBranch);

    for (auto it = begin (caseBranchValue); it != end (caseBranchValue); ++it) {
        emitLabel(it->caseBranch, it->label);
    }


    emitLabel("default", caseExitLabel);

    for(int i = 0; i < ctx->caseBranchList()->caseBranch().size(); i++) {
        if (!ctx->caseBranchList()->caseBranch().at(i)->getText().length()) {
            continue;
        }
        Label *caseBranchLabel = caseBranchLabels.at(i);
        emitLabel(caseBranchLabel);
        compiler->visit(ctx->caseBranchList()->caseBranch().at(i)->statement());
        emit(GOTO, caseExitLabel);
    }
    emitLabel(caseExitLabel);
}

void StatementGenerator::emitRepeat(CmmParser::RepeatStatementContext *ctx)
{
    //repeatStatement : REPEAT statementList UNTIL expression ;
    Label *loopTopLabel  = new Label();
    Label *loopExitLabel = new Label();

    emitLabel(loopTopLabel);

    compiler->visit(ctx->statementList());
    compiler->visit(ctx->expression());
    emit(IFNE, loopExitLabel);
    emit(GOTO, loopTopLabel);

    emitLabel(loopExitLabel);
}

void StatementGenerator::emitWhile(CmmParser::WhileStatementContext *ctx)
{
    /***** Complete this member function. *****/
   //whileStatement  : WHILE expression DO statement
    Label *whileTopLabel  = new Label();
    Label *whileExitLabel = new Label();

    emitLabel(whileTopLabel);

    compiler->visit(ctx->expression());


    emit(IFEQ, whileExitLabel); // IFEQ=false, IFNE=true -> jump to statement
    compiler->visit(ctx->statement());
    emit(GOTO, whileTopLabel);

    emitLabel(whileExitLabel);
}

void StatementGenerator::emitFor(CmmParser::ForStatementContext *ctx)
{
//    /***** Complete this member function. *****/
//  /*
//    forStatement : FOR variable ':=' expression
//                    ( TO | DOWNTO ) expression DO statement ;
//   */
    Label *forTopLabel  = new Label();
    Label *forExitLabel = new Label();
    Label *forBeforeStatementLabel = new Label();

    Label *checkLabel  = new Label();
    emitLabel(forTopLabel);
    // Initial statement
    auto statements = ctx->statement();
    compiler->visit(ctx->statement().at(0));

    // Check condition
    emitLabel(checkLabel);
    compiler->visit(ctx->expression());
    emit(IFEQ, forExitLabel);
    compiler->visit(ctx->statement().at(2));
    compiler->visit(ctx->statement().at(1));
    emit(GOTO, checkLabel);
    emitLabel(forExitLabel);

}

void StatementGenerator::emitProcedureCall(CmmParser::ProcedureCallStatementContext *ctx)
{
    /***** Complete this member function. *****/
    SymtabEntry *routineId = ctx->procedureName()->entry;
    emitCall(routineId, ctx->argumentList());
}

void StatementGenerator::emitFunctionCall(CmmParser::FunctionCallContext *ctx)
{
    /***** Complete this member function. *****/
    SymtabEntry *routineId = ctx->functionName()->entry;
    emitCall(routineId, ctx->argumentList());
}

void StatementGenerator::emitCall(SymtabEntry *routineId,
                                  CmmParser::ArgumentListContext *argListCtx)
{
    /***** Complete this member function. *****/
    /***** Complete this member function. *****/
    string routineName = routineId->getName();
    if (routineName == "main") {
        routineName = "_main";
    }
    vector<SymtabEntry *> *parmIds = routineId->getRoutineParameters();

    if (argListCtx) {
        auto argurements = argListCtx->argument();
        for (int i = 0; i < argurements.size(); i++) {
            compiler->visit(argurements.at(i)->expression());
            if (typeDescriptor(parmIds->at(i)) == "F" && typeDescriptor(argurements.at(i)->expression()->type) == "I") {
                emit(I2F);
            }
            else if (typeDescriptor(parmIds->at(i)) == "I" && typeDescriptor(argurements.at(i)->expression()->type) == "F") {
                emit(F2I);
            }
        }
    }

    string procedureCall = programName + "/" + routineName + "(";
    // Parameter and return type descriptors.
    if (parmIds != nullptr)
    {
        for (SymtabEntry *parmId : *parmIds)
        {
            procedureCall += typeDescriptor(parmId);
        }
    }
    procedureCall += ")";

    procedureCall += routineId->getType() ? typeDescriptor(routineId->getType()) : "V";

    emit(Instruction::INVOKESTATIC, procedureCall);
}

void StatementGenerator::emitWrite(CmmParser::WriteStatementContext *ctx)
{
    emitWrite(ctx->writeArguments(), false);
}

void StatementGenerator::emitWriteln(CmmParser::WritelnStatementContext *ctx)
{
    emitWrite(ctx->writeArguments(), true);
}

void StatementGenerator::emitWrite(CmmParser::WriteArgumentsContext *argsCtx,
                      bool needLF)
{
    emit(GETSTATIC, "java/lang/System/out", "Ljava/io/PrintStream;");

    // WRITELN with no arguments.
    if (argsCtx == nullptr)
    {
        emit(INVOKEVIRTUAL, "java/io/PrintStream.println()V");
        localStack->decrease(1);
    }

    // Generate code for the arguments.
    else
    {
        string format;
        int exprCount = createWriteFormat(argsCtx, format, needLF);

        // Load the format string.
        emit(LDC, format);

        // Emit the arguments array.
       if (exprCount > 0)
        {
            emitArgumentsArray(argsCtx, exprCount);

            emit(INVOKEVIRTUAL,
                        string("java/io/PrintStream/printf(Ljava/lang/String;")
                      + string("[Ljava/lang/Object;)")
                      + string("Ljava/io/PrintStream;"));
            localStack->decrease(2);
            emit(POP);
        }
        else
        {
            emit(INVOKEVIRTUAL,
                 "java/io/PrintStream/print(Ljava/lang/String;)V");
            localStack->decrease(2);
        }
    }
}

int StatementGenerator::createWriteFormat(
                                CmmParser::WriteArgumentsContext *argsCtx,
                                string& format, bool needLF)
{
    int exprCount = 0;
    format += "\"";

    // Loop over the write arguments.
    for (CmmParser::WriteArgumentContext *argCtx : argsCtx->writeArgument())
    {
        Typespec *type = argCtx->expression()->type;
        string argText = argCtx->getText();

        // Append any literal strings.
        if (argText[0] == '\'') format += convertString(argText, true);

        // For any other expressions, append a field specifier.
        else
        {
            exprCount++;
            format.append("%");

            CmmParser::FieldWidthContext *fwCtx = argCtx->fieldWidth();
            if (fwCtx != nullptr)
            {
                string sign = (   (fwCtx->sign() != nullptr)
                               && (fwCtx->sign()->getText() == "-")) ? "-" : "";
                format += sign + fwCtx->integerConstant()->getText();

                CmmParser::DecimalPlacesContext *dpCtx =
                                                        fwCtx->decimalPlaces();
                if (dpCtx != nullptr)
                {
                    format += "." + dpCtx->integerConstant()->getText();
                }
            }

            string typeFlag = type == Predefined::integerType ? "d"
                            : type == Predefined::realType    ? "f"
                            : type == Predefined::booleanType ? "b"
                            : type == Predefined::charType    ? "c"
                            :                                  "s";
            format += typeFlag;
        }
    }

    format += needLF ? "\\n\"" : "\"";

    return exprCount;
}

void StatementGenerator::emitArgumentsArray(
                    CmmParser::WriteArgumentsContext *argsCtx, int exprCount)
{
    // Create the arguments array.
    emitLoadConstant(exprCount);
    emit(ANEWARRAY, "java/lang/Object");

    int index = 0;

    // Loop over the write arguments to fill the arguments array.
    for (CmmParser::WriteArgumentContext *argCtx :
                                                argsCtx->writeArgument())
    {
        string argText = argCtx->getText();
        CmmParser::ExpressionContext *exprCtx = argCtx->expression();
        Typespec *type = exprCtx->type->baseType();

        // Skip string constants, which were made part of
        // the format string.
        if (argText[0] != '\'')
        {
            emit(DUP);
            emitLoadConstant(index++);

            compiler->visit(exprCtx);

            Form form = type->getForm();
            if (    ((form == SCALAR) || (form == ENUMERATION))
                 && (type != Predefined::stringType))
            {
                emit(INVOKESTATIC, valueOfSignature(type));
            }

            // Store the value into the array.
            emit(AASTORE);
        }
    }
}

void StatementGenerator::emitRead(CmmParser::ReadStatementContext *ctx)
{
    emitRead(ctx->readArguments(), false);
}

void StatementGenerator::emitReadln(CmmParser::ReadlnStatementContext *ctx)
{
    emitRead(ctx->readArguments(), true);
}

void StatementGenerator::emitRead(CmmParser::ReadArgumentsContext *argsCtx,
                                  bool needSkip)
{
    int size = argsCtx->variable().size();

    // Loop over read arguments.
    for (int i = 0; i < size; i++)
    {
        CmmParser::VariableContext *varCtx = argsCtx->variable()[i];
        Typespec *varType = varCtx->type;

        if (varType == Predefined::integerType)
        {
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/nextInt()I");
            emitStoreValue(varCtx->entry, nullptr);
        }
        else if (varType == Predefined::realType)
        {
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/nextFloat()F");
            emitStoreValue(varCtx->entry, nullptr);
        }
        else if (varType == Predefined::booleanType)
        {
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/nextBoolean()Z");
            emitStoreValue(varCtx->entry, nullptr);
        }
        else if (varType == Predefined::charType)
        {
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(LDC, "\"\"");
            emit(INVOKEVIRTUAL,
                 string("java/util/Scanner/useDelimiter(Ljava/lang/String;)") +
                 string("Ljava/util/Scanner;"));
            emit(POP);
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/next()Ljava/lang/String;");
            emit(ICONST_0);
            emit(INVOKEVIRTUAL, "java/lang/String/charAt(I)C");
            emitStoreValue(varCtx->entry, nullptr);

            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/reset()Ljava/util/Scanner;");

        }
        else  // string
        {
            emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
            emit(INVOKEVIRTUAL, "java/util/Scanner/next()Ljava/lang/String;");
            emitStoreValue(varCtx->entry, nullptr);
        }
    }

    // READLN: Skip the rest of the input line.
    if (needSkip)
    {
        emit(GETSTATIC, programName + "/_sysin Ljava/util/Scanner;");
        emit(INVOKEVIRTUAL, "java/util/Scanner/nextLine()Ljava/lang/String;");
        emit(POP);
    }
}

}} // namespace backend::compiler
