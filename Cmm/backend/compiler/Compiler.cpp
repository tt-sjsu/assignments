#include "intermediate/symtab/Predefined.h"
#include "Compiler.h"

namespace backend { namespace compiler {

Object Compiler::visitProgram(CmmParser::ProgramContext *ctx)
{
    createNewGenerators(code);
    programCode->emitProgram(ctx);
    return nullptr;
}

Object Compiler::visitRoutineDefinition(
                                CmmParser::RoutineDefinitionContext *ctx)
{
    createNewGenerators(programCode);
    programCode->emitRoutine(ctx);
    return nullptr;
}

Object Compiler::visitReturnStatement(CmmParser::ReturnStatementContext *ctx) {
    visit(ctx->expression());
    return nullptr;
}

Object Compiler::visitStatement(CmmParser::StatementContext *ctx)
{
    if (   (ctx->compoundStatement() == nullptr)
        && (ctx->emptyStatement() == nullptr))
    {
        statementCode->emitComment(ctx);
    }

    return visitChildren(ctx);
}

Object Compiler::visitNormalAssignment(CmmParser::NormalAssignmentContext *ctx)
{
    statementCode->emitAssignment(ctx);
    return nullptr;
}

Object Compiler::visitIncrementAssignment(CmmParser::IncrementAssignmentContext *ctx) {
    statementCode->emitVariableUpdate(ctx->variable(), 1);
    return nullptr;
}

Object Compiler::visitDecrementAssignment(CmmParser::DecrementAssignmentContext *ctx) {
    statementCode->emitVariableUpdate(ctx->variable(), -1);
    return nullptr;
}

Object Compiler::visitIfStatement(CmmParser::IfStatementContext *ctx)
{
    statementCode->emitIf(ctx);
    return nullptr;
}

Object Compiler::visitCaseStatement(CmmParser::CaseStatementContext *ctx)
{
    statementCode->emitCase(ctx);
    return nullptr;
}

Object Compiler::visitRepeatStatement(CmmParser::RepeatStatementContext *ctx)
{
    statementCode->emitRepeat(ctx);
    return nullptr;
}

Object Compiler::visitWhileStatement(CmmParser::WhileStatementContext *ctx)
{
    statementCode->emitWhile(ctx);
    return nullptr;
}

Object Compiler::visitForStatement(CmmParser::ForStatementContext *ctx)
{
    statementCode->emitFor(ctx);
    return nullptr;
}

Object Compiler::visitProcedureCallStatement(
                            CmmParser::ProcedureCallStatementContext *ctx)
{
    statementCode->emitProcedureCall(ctx);
    return nullptr;
}

Object Compiler::visitExpression(CmmParser::ExpressionContext *ctx)
{
    expressionCode->emitExpression(ctx);
    return nullptr;
}

Object Compiler::visitVariableFactor(CmmParser::VariableFactorContext *ctx)
{
    expressionCode->emitLoadValue(ctx->variable());
    return nullptr;
}

Object Compiler::visitVariable(CmmParser::VariableContext *ctx)
{
    expressionCode->emitLoadVariable(ctx);
    return nullptr;
}

Object Compiler::visitNumberFactor(CmmParser::NumberFactorContext *ctx)
{
    if (ctx->type == Predefined::integerType)
    {
        expressionCode->emitLoadIntegerConstant(ctx->number());
    }
    else
    {
        expressionCode->emitLoadRealConstant(ctx->number());
    }

    return nullptr;
}

Object Compiler::visitCharacterFactor(CmmParser::CharacterFactorContext *ctx)
{
    char ch = ctx->getText()[1];
    expressionCode->emitLoadConstant(ch);

    return nullptr;
}

Object Compiler::visitStringFactor(CmmParser::StringFactorContext *ctx)
{
    string jasminString = convertString(ctx->getText(), true);
    expressionCode->emitLoadConstant(jasminString);

    return nullptr;
}

Object Compiler::visitFunctionCallFactor(
                                CmmParser::FunctionCallFactorContext *ctx)
{
    statementCode->emitFunctionCall(ctx->functionCall());
    return nullptr;
}

Object Compiler::visitNotFactor(CmmParser::NotFactorContext *ctx)
{
    expressionCode->emitNotFactor(ctx);
    return nullptr;
}

Object Compiler::visitParenthesizedFactor(
                                CmmParser::ParenthesizedFactorContext *ctx)
{
    return visit(ctx->expression());
}

Object Compiler::visitWriteStatement(CmmParser::WriteStatementContext *ctx)
{
    statementCode->emitWrite(ctx);
    return nullptr;
}

Object Compiler::visitWritelnStatement(CmmParser::WritelnStatementContext *ctx)
{
    statementCode->emitWriteln(ctx);
    return nullptr;
}

Object Compiler::visitReadStatement(CmmParser::ReadStatementContext *ctx)
{
    statementCode->emitRead(ctx);
    return nullptr;
}

Object Compiler::visitReadlnStatement(CmmParser::ReadlnStatementContext *ctx)
{
    statementCode->emitReadln(ctx);
    return nullptr;
}

}}  // namespace backend::compiler
