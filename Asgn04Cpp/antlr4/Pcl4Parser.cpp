
// Generated from Pcl4.g4 by ANTLR 4.9.1


#include "Pcl4Visitor.h"

#include "Pcl4Parser.h"


using namespace antlrcpp;
using namespace antlr4;

Pcl4Parser::Pcl4Parser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

Pcl4Parser::~Pcl4Parser() {
  delete _interpreter;
}

std::string Pcl4Parser::getGrammarFileName() const {
  return "Pcl4.g4";
}

const std::vector<std::string>& Pcl4Parser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& Pcl4Parser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

Pcl4Parser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::ProgramHeaderContext* Pcl4Parser::ProgramContext::programHeader() {
  return getRuleContext<Pcl4Parser::ProgramHeaderContext>(0);
}

Pcl4Parser::BlockContext* Pcl4Parser::ProgramContext::block() {
  return getRuleContext<Pcl4Parser::BlockContext>(0);
}


size_t Pcl4Parser::ProgramContext::getRuleIndex() const {
  return Pcl4Parser::RuleProgram;
}


antlrcpp::Any Pcl4Parser::ProgramContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitProgram(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ProgramContext* Pcl4Parser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, Pcl4Parser::RuleProgram);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(88);
    programHeader();
    setState(89);
    block();
    setState(90);
    match(Pcl4Parser::T__0);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramHeaderContext ------------------------------------------------------------------

Pcl4Parser::ProgramHeaderContext::ProgramHeaderContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::ProgramHeaderContext::PROGRAM() {
  return getToken(Pcl4Parser::PROGRAM, 0);
}

tree::TerminalNode* Pcl4Parser::ProgramHeaderContext::IDENTIFIER() {
  return getToken(Pcl4Parser::IDENTIFIER, 0);
}

Pcl4Parser::ProgramParametersContext* Pcl4Parser::ProgramHeaderContext::programParameters() {
  return getRuleContext<Pcl4Parser::ProgramParametersContext>(0);
}


size_t Pcl4Parser::ProgramHeaderContext::getRuleIndex() const {
  return Pcl4Parser::RuleProgramHeader;
}


antlrcpp::Any Pcl4Parser::ProgramHeaderContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitProgramHeader(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ProgramHeaderContext* Pcl4Parser::programHeader() {
  ProgramHeaderContext *_localctx = _tracker.createInstance<ProgramHeaderContext>(_ctx, getState());
  enterRule(_localctx, 2, Pcl4Parser::RuleProgramHeader);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(92);
    match(Pcl4Parser::PROGRAM);
    setState(93);
    match(Pcl4Parser::IDENTIFIER);
    setState(95);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__2) {
      setState(94);
      programParameters();
    }
    setState(97);
    match(Pcl4Parser::T__1);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ProgramParametersContext ------------------------------------------------------------------

Pcl4Parser::ProgramParametersContext::ProgramParametersContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> Pcl4Parser::ProgramParametersContext::IDENTIFIER() {
  return getTokens(Pcl4Parser::IDENTIFIER);
}

tree::TerminalNode* Pcl4Parser::ProgramParametersContext::IDENTIFIER(size_t i) {
  return getToken(Pcl4Parser::IDENTIFIER, i);
}


size_t Pcl4Parser::ProgramParametersContext::getRuleIndex() const {
  return Pcl4Parser::RuleProgramParameters;
}


antlrcpp::Any Pcl4Parser::ProgramParametersContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitProgramParameters(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ProgramParametersContext* Pcl4Parser::programParameters() {
  ProgramParametersContext *_localctx = _tracker.createInstance<ProgramParametersContext>(_ctx, getState());
  enterRule(_localctx, 4, Pcl4Parser::RuleProgramParameters);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(99);
    match(Pcl4Parser::T__2);
    setState(100);
    match(Pcl4Parser::IDENTIFIER);
    setState(105);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Pcl4Parser::T__3) {
      setState(101);
      match(Pcl4Parser::T__3);
      setState(102);
      match(Pcl4Parser::IDENTIFIER);
      setState(107);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(108);
    match(Pcl4Parser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BlockContext ------------------------------------------------------------------

Pcl4Parser::BlockContext::BlockContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::DeclarationsContext* Pcl4Parser::BlockContext::declarations() {
  return getRuleContext<Pcl4Parser::DeclarationsContext>(0);
}

Pcl4Parser::CompoundStatementContext* Pcl4Parser::BlockContext::compoundStatement() {
  return getRuleContext<Pcl4Parser::CompoundStatementContext>(0);
}


size_t Pcl4Parser::BlockContext::getRuleIndex() const {
  return Pcl4Parser::RuleBlock;
}


antlrcpp::Any Pcl4Parser::BlockContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitBlock(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::BlockContext* Pcl4Parser::block() {
  BlockContext *_localctx = _tracker.createInstance<BlockContext>(_ctx, getState());
  enterRule(_localctx, 6, Pcl4Parser::RuleBlock);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(110);
    declarations();
    setState(111);
    compoundStatement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclarationsContext ------------------------------------------------------------------

Pcl4Parser::DeclarationsContext::DeclarationsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t Pcl4Parser::DeclarationsContext::getRuleIndex() const {
  return Pcl4Parser::RuleDeclarations;
}


antlrcpp::Any Pcl4Parser::DeclarationsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitDeclarations(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::DeclarationsContext* Pcl4Parser::declarations() {
  DeclarationsContext *_localctx = _tracker.createInstance<DeclarationsContext>(_ctx, getState());
  enterRule(_localctx, 8, Pcl4Parser::RuleDeclarations);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);

   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementContext ------------------------------------------------------------------

Pcl4Parser::StatementContext::StatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::CompoundStatementContext* Pcl4Parser::StatementContext::compoundStatement() {
  return getRuleContext<Pcl4Parser::CompoundStatementContext>(0);
}

Pcl4Parser::AssignmentStatementContext* Pcl4Parser::StatementContext::assignmentStatement() {
  return getRuleContext<Pcl4Parser::AssignmentStatementContext>(0);
}

Pcl4Parser::RepeatStatementContext* Pcl4Parser::StatementContext::repeatStatement() {
  return getRuleContext<Pcl4Parser::RepeatStatementContext>(0);
}

Pcl4Parser::WriteStatementContext* Pcl4Parser::StatementContext::writeStatement() {
  return getRuleContext<Pcl4Parser::WriteStatementContext>(0);
}

Pcl4Parser::WritelnStatementContext* Pcl4Parser::StatementContext::writelnStatement() {
  return getRuleContext<Pcl4Parser::WritelnStatementContext>(0);
}

Pcl4Parser::EmptyStatementContext* Pcl4Parser::StatementContext::emptyStatement() {
  return getRuleContext<Pcl4Parser::EmptyStatementContext>(0);
}

Pcl4Parser::CaseStatementContext* Pcl4Parser::StatementContext::caseStatement() {
  return getRuleContext<Pcl4Parser::CaseStatementContext>(0);
}

Pcl4Parser::ForStatementContext* Pcl4Parser::StatementContext::forStatement() {
  return getRuleContext<Pcl4Parser::ForStatementContext>(0);
}

Pcl4Parser::WhileStatementContext* Pcl4Parser::StatementContext::whileStatement() {
  return getRuleContext<Pcl4Parser::WhileStatementContext>(0);
}

Pcl4Parser::IfStatementContext* Pcl4Parser::StatementContext::ifStatement() {
  return getRuleContext<Pcl4Parser::IfStatementContext>(0);
}


size_t Pcl4Parser::StatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleStatement;
}


antlrcpp::Any Pcl4Parser::StatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::StatementContext* Pcl4Parser::statement() {
  StatementContext *_localctx = _tracker.createInstance<StatementContext>(_ctx, getState());
  enterRule(_localctx, 10, Pcl4Parser::RuleStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(125);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case Pcl4Parser::BEGIN: {
        enterOuterAlt(_localctx, 1);
        setState(115);
        compoundStatement();
        break;
      }

      case Pcl4Parser::IDENTIFIER: {
        enterOuterAlt(_localctx, 2);
        setState(116);
        assignmentStatement();
        break;
      }

      case Pcl4Parser::REPEAT: {
        enterOuterAlt(_localctx, 3);
        setState(117);
        repeatStatement();
        break;
      }

      case Pcl4Parser::WRITE: {
        enterOuterAlt(_localctx, 4);
        setState(118);
        writeStatement();
        break;
      }

      case Pcl4Parser::WRITELN: {
        enterOuterAlt(_localctx, 5);
        setState(119);
        writelnStatement();
        break;
      }

      case Pcl4Parser::T__1:
      case Pcl4Parser::END:
      case Pcl4Parser::ELSE:
      case Pcl4Parser::UNTIL: {
        enterOuterAlt(_localctx, 6);
        setState(120);
        emptyStatement();
        break;
      }

      case Pcl4Parser::CASE: {
        enterOuterAlt(_localctx, 7);
        setState(121);
        caseStatement();
        break;
      }

      case Pcl4Parser::FOR: {
        enterOuterAlt(_localctx, 8);
        setState(122);
        forStatement();
        break;
      }

      case Pcl4Parser::WHILE: {
        enterOuterAlt(_localctx, 9);
        setState(123);
        whileStatement();
        break;
      }

      case Pcl4Parser::IF: {
        enterOuterAlt(_localctx, 10);
        setState(124);
        ifStatement();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompoundStatementContext ------------------------------------------------------------------

Pcl4Parser::CompoundStatementContext::CompoundStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::CompoundStatementContext::BEGIN() {
  return getToken(Pcl4Parser::BEGIN, 0);
}

Pcl4Parser::StatementListContext* Pcl4Parser::CompoundStatementContext::statementList() {
  return getRuleContext<Pcl4Parser::StatementListContext>(0);
}

tree::TerminalNode* Pcl4Parser::CompoundStatementContext::END() {
  return getToken(Pcl4Parser::END, 0);
}


size_t Pcl4Parser::CompoundStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleCompoundStatement;
}


antlrcpp::Any Pcl4Parser::CompoundStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitCompoundStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::CompoundStatementContext* Pcl4Parser::compoundStatement() {
  CompoundStatementContext *_localctx = _tracker.createInstance<CompoundStatementContext>(_ctx, getState());
  enterRule(_localctx, 12, Pcl4Parser::RuleCompoundStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(127);
    match(Pcl4Parser::BEGIN);
    setState(128);
    statementList();
    setState(129);
    match(Pcl4Parser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- EmptyStatementContext ------------------------------------------------------------------

Pcl4Parser::EmptyStatementContext::EmptyStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t Pcl4Parser::EmptyStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleEmptyStatement;
}


antlrcpp::Any Pcl4Parser::EmptyStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitEmptyStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::EmptyStatementContext* Pcl4Parser::emptyStatement() {
  EmptyStatementContext *_localctx = _tracker.createInstance<EmptyStatementContext>(_ctx, getState());
  enterRule(_localctx, 14, Pcl4Parser::RuleEmptyStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);

   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatementListContext ------------------------------------------------------------------

Pcl4Parser::StatementListContext::StatementListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::StatementContext *> Pcl4Parser::StatementListContext::statement() {
  return getRuleContexts<Pcl4Parser::StatementContext>();
}

Pcl4Parser::StatementContext* Pcl4Parser::StatementListContext::statement(size_t i) {
  return getRuleContext<Pcl4Parser::StatementContext>(i);
}


size_t Pcl4Parser::StatementListContext::getRuleIndex() const {
  return Pcl4Parser::RuleStatementList;
}


antlrcpp::Any Pcl4Parser::StatementListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitStatementList(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::StatementListContext* Pcl4Parser::statementList() {
  StatementListContext *_localctx = _tracker.createInstance<StatementListContext>(_ctx, getState());
  enterRule(_localctx, 16, Pcl4Parser::RuleStatementList);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(133);
    statement();
    setState(138);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(134);
        match(Pcl4Parser::T__1);
        setState(135);
        statement(); 
      }
      setState(140);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentStatementContext ------------------------------------------------------------------

Pcl4Parser::AssignmentStatementContext::AssignmentStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::LhsContext* Pcl4Parser::AssignmentStatementContext::lhs() {
  return getRuleContext<Pcl4Parser::LhsContext>(0);
}

Pcl4Parser::RhsContext* Pcl4Parser::AssignmentStatementContext::rhs() {
  return getRuleContext<Pcl4Parser::RhsContext>(0);
}


size_t Pcl4Parser::AssignmentStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleAssignmentStatement;
}


antlrcpp::Any Pcl4Parser::AssignmentStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitAssignmentStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::AssignmentStatementContext* Pcl4Parser::assignmentStatement() {
  AssignmentStatementContext *_localctx = _tracker.createInstance<AssignmentStatementContext>(_ctx, getState());
  enterRule(_localctx, 18, Pcl4Parser::RuleAssignmentStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(141);
    lhs();
    setState(142);
    match(Pcl4Parser::T__5);
    setState(143);
    rhs();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RepeatStatementContext ------------------------------------------------------------------

Pcl4Parser::RepeatStatementContext::RepeatStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::RepeatStatementContext::REPEAT() {
  return getToken(Pcl4Parser::REPEAT, 0);
}

Pcl4Parser::StatementListContext* Pcl4Parser::RepeatStatementContext::statementList() {
  return getRuleContext<Pcl4Parser::StatementListContext>(0);
}

tree::TerminalNode* Pcl4Parser::RepeatStatementContext::UNTIL() {
  return getToken(Pcl4Parser::UNTIL, 0);
}

Pcl4Parser::ExpressionContext* Pcl4Parser::RepeatStatementContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}


size_t Pcl4Parser::RepeatStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleRepeatStatement;
}


antlrcpp::Any Pcl4Parser::RepeatStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitRepeatStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::RepeatStatementContext* Pcl4Parser::repeatStatement() {
  RepeatStatementContext *_localctx = _tracker.createInstance<RepeatStatementContext>(_ctx, getState());
  enterRule(_localctx, 20, Pcl4Parser::RuleRepeatStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(145);
    match(Pcl4Parser::REPEAT);
    setState(146);
    statementList();
    setState(147);
    match(Pcl4Parser::UNTIL);
    setState(148);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LhsContext ------------------------------------------------------------------

Pcl4Parser::LhsContext::LhsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::VariableContext* Pcl4Parser::LhsContext::variable() {
  return getRuleContext<Pcl4Parser::VariableContext>(0);
}


size_t Pcl4Parser::LhsContext::getRuleIndex() const {
  return Pcl4Parser::RuleLhs;
}


antlrcpp::Any Pcl4Parser::LhsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitLhs(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::LhsContext* Pcl4Parser::lhs() {
  LhsContext *_localctx = _tracker.createInstance<LhsContext>(_ctx, getState());
  enterRule(_localctx, 22, Pcl4Parser::RuleLhs);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(150);
    variable();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RhsContext ------------------------------------------------------------------

Pcl4Parser::RhsContext::RhsContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::ExpressionContext* Pcl4Parser::RhsContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}


size_t Pcl4Parser::RhsContext::getRuleIndex() const {
  return Pcl4Parser::RuleRhs;
}


antlrcpp::Any Pcl4Parser::RhsContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitRhs(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::RhsContext* Pcl4Parser::rhs() {
  RhsContext *_localctx = _tracker.createInstance<RhsContext>(_ctx, getState());
  enterRule(_localctx, 24, Pcl4Parser::RuleRhs);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(152);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteStatementContext ------------------------------------------------------------------

Pcl4Parser::WriteStatementContext::WriteStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::WriteStatementContext::WRITE() {
  return getToken(Pcl4Parser::WRITE, 0);
}

Pcl4Parser::WriteArgumentsOnContext* Pcl4Parser::WriteStatementContext::writeArgumentsOn() {
  return getRuleContext<Pcl4Parser::WriteArgumentsOnContext>(0);
}


size_t Pcl4Parser::WriteStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteStatement;
}


antlrcpp::Any Pcl4Parser::WriteStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteStatementContext* Pcl4Parser::writeStatement() {
  WriteStatementContext *_localctx = _tracker.createInstance<WriteStatementContext>(_ctx, getState());
  enterRule(_localctx, 26, Pcl4Parser::RuleWriteStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(154);
    match(Pcl4Parser::WRITE);
    setState(155);
    writeArgumentsOn();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WritelnStatementContext ------------------------------------------------------------------

Pcl4Parser::WritelnStatementContext::WritelnStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::WritelnStatementContext::WRITELN() {
  return getToken(Pcl4Parser::WRITELN, 0);
}

Pcl4Parser::WriteArgumentsLnContext* Pcl4Parser::WritelnStatementContext::writeArgumentsLn() {
  return getRuleContext<Pcl4Parser::WriteArgumentsLnContext>(0);
}


size_t Pcl4Parser::WritelnStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleWritelnStatement;
}


antlrcpp::Any Pcl4Parser::WritelnStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWritelnStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WritelnStatementContext* Pcl4Parser::writelnStatement() {
  WritelnStatementContext *_localctx = _tracker.createInstance<WritelnStatementContext>(_ctx, getState());
  enterRule(_localctx, 28, Pcl4Parser::RuleWritelnStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(157);
    match(Pcl4Parser::WRITELN);
    setState(159);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__2) {
      setState(158);
      writeArgumentsLn();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

Pcl4Parser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::SimpleExpressionContext *> Pcl4Parser::ExpressionContext::simpleExpression() {
  return getRuleContexts<Pcl4Parser::SimpleExpressionContext>();
}

Pcl4Parser::SimpleExpressionContext* Pcl4Parser::ExpressionContext::simpleExpression(size_t i) {
  return getRuleContext<Pcl4Parser::SimpleExpressionContext>(i);
}

Pcl4Parser::RelOpContext* Pcl4Parser::ExpressionContext::relOp() {
  return getRuleContext<Pcl4Parser::RelOpContext>(0);
}


size_t Pcl4Parser::ExpressionContext::getRuleIndex() const {
  return Pcl4Parser::RuleExpression;
}


antlrcpp::Any Pcl4Parser::ExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitExpression(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ExpressionContext* Pcl4Parser::expression() {
  ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, getState());
  enterRule(_localctx, 30, Pcl4Parser::RuleExpression);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(161);
    simpleExpression();
    setState(165);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__9)
      | (1ULL << Pcl4Parser::T__10)
      | (1ULL << Pcl4Parser::T__11)
      | (1ULL << Pcl4Parser::T__12)
      | (1ULL << Pcl4Parser::T__13)
      | (1ULL << Pcl4Parser::T__14))) != 0)) {
      setState(162);
      relOp();
      setState(163);
      simpleExpression();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SimpleExpressionContext ------------------------------------------------------------------

Pcl4Parser::SimpleExpressionContext::SimpleExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::TermContext *> Pcl4Parser::SimpleExpressionContext::term() {
  return getRuleContexts<Pcl4Parser::TermContext>();
}

Pcl4Parser::TermContext* Pcl4Parser::SimpleExpressionContext::term(size_t i) {
  return getRuleContext<Pcl4Parser::TermContext>(i);
}

Pcl4Parser::SignContext* Pcl4Parser::SimpleExpressionContext::sign() {
  return getRuleContext<Pcl4Parser::SignContext>(0);
}

std::vector<Pcl4Parser::AddOpContext *> Pcl4Parser::SimpleExpressionContext::addOp() {
  return getRuleContexts<Pcl4Parser::AddOpContext>();
}

Pcl4Parser::AddOpContext* Pcl4Parser::SimpleExpressionContext::addOp(size_t i) {
  return getRuleContext<Pcl4Parser::AddOpContext>(i);
}


size_t Pcl4Parser::SimpleExpressionContext::getRuleIndex() const {
  return Pcl4Parser::RuleSimpleExpression;
}


antlrcpp::Any Pcl4Parser::SimpleExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitSimpleExpression(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::SimpleExpressionContext* Pcl4Parser::simpleExpression() {
  SimpleExpressionContext *_localctx = _tracker.createInstance<SimpleExpressionContext>(_ctx, getState());
  enterRule(_localctx, 32, Pcl4Parser::RuleSimpleExpression);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(168);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 6, _ctx)) {
    case 1: {
      setState(167);
      sign();
      break;
    }

    default:
      break;
    }
    setState(170);
    term();
    setState(176);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__7)
      | (1ULL << Pcl4Parser::T__8)
      | (1ULL << Pcl4Parser::OR))) != 0)) {
      setState(171);
      addOp();
      setState(172);
      term();
      setState(178);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CaseStatementContext ------------------------------------------------------------------

Pcl4Parser::CaseStatementContext::CaseStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::CaseStatementContext::CASE() {
  return getToken(Pcl4Parser::CASE, 0);
}

Pcl4Parser::ExpressionContext* Pcl4Parser::CaseStatementContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}

tree::TerminalNode* Pcl4Parser::CaseStatementContext::OF() {
  return getToken(Pcl4Parser::OF, 0);
}

std::vector<Pcl4Parser::CasesContext *> Pcl4Parser::CaseStatementContext::cases() {
  return getRuleContexts<Pcl4Parser::CasesContext>();
}

Pcl4Parser::CasesContext* Pcl4Parser::CaseStatementContext::cases(size_t i) {
  return getRuleContext<Pcl4Parser::CasesContext>(i);
}

tree::TerminalNode* Pcl4Parser::CaseStatementContext::END() {
  return getToken(Pcl4Parser::END, 0);
}

tree::TerminalNode* Pcl4Parser::CaseStatementContext::ELSE() {
  return getToken(Pcl4Parser::ELSE, 0);
}

Pcl4Parser::StatementListContext* Pcl4Parser::CaseStatementContext::statementList() {
  return getRuleContext<Pcl4Parser::StatementListContext>(0);
}


size_t Pcl4Parser::CaseStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleCaseStatement;
}


antlrcpp::Any Pcl4Parser::CaseStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitCaseStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::CaseStatementContext* Pcl4Parser::caseStatement() {
  CaseStatementContext *_localctx = _tracker.createInstance<CaseStatementContext>(_ctx, getState());
  enterRule(_localctx, 34, Pcl4Parser::RuleCaseStatement);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(179);
    match(Pcl4Parser::CASE);
    setState(180);
    expression();
    setState(181);
    match(Pcl4Parser::OF);
    setState(182);
    cases();
    setState(187);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        setState(183);
        match(Pcl4Parser::T__1);
        setState(184);
        cases(); 
      }
      setState(189);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 8, _ctx);
    }
    setState(193);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__1) {
      setState(190);
      match(Pcl4Parser::T__1);
      setState(191);
      match(Pcl4Parser::ELSE);
      setState(192);
      statementList();
    }
    setState(195);
    match(Pcl4Parser::END);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CasesContext ------------------------------------------------------------------

Pcl4Parser::CasesContext::CasesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::ExpressionContext *> Pcl4Parser::CasesContext::expression() {
  return getRuleContexts<Pcl4Parser::ExpressionContext>();
}

Pcl4Parser::ExpressionContext* Pcl4Parser::CasesContext::expression(size_t i) {
  return getRuleContext<Pcl4Parser::ExpressionContext>(i);
}

Pcl4Parser::StatementListContext* Pcl4Parser::CasesContext::statementList() {
  return getRuleContext<Pcl4Parser::StatementListContext>(0);
}


size_t Pcl4Parser::CasesContext::getRuleIndex() const {
  return Pcl4Parser::RuleCases;
}


antlrcpp::Any Pcl4Parser::CasesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitCases(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::CasesContext* Pcl4Parser::cases() {
  CasesContext *_localctx = _tracker.createInstance<CasesContext>(_ctx, getState());
  enterRule(_localctx, 36, Pcl4Parser::RuleCases);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(197);
    expression();
    setState(202);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Pcl4Parser::T__3) {
      setState(198);
      match(Pcl4Parser::T__3);
      setState(199);
      expression();
      setState(204);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(205);
    match(Pcl4Parser::T__6);
    setState(206);
    statementList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WhileStatementContext ------------------------------------------------------------------

Pcl4Parser::WhileStatementContext::WhileStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::WhileStatementContext::WHILE() {
  return getToken(Pcl4Parser::WHILE, 0);
}

Pcl4Parser::ExpressionContext* Pcl4Parser::WhileStatementContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}

tree::TerminalNode* Pcl4Parser::WhileStatementContext::DO() {
  return getToken(Pcl4Parser::DO, 0);
}

Pcl4Parser::StatementContext* Pcl4Parser::WhileStatementContext::statement() {
  return getRuleContext<Pcl4Parser::StatementContext>(0);
}


size_t Pcl4Parser::WhileStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleWhileStatement;
}


antlrcpp::Any Pcl4Parser::WhileStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWhileStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WhileStatementContext* Pcl4Parser::whileStatement() {
  WhileStatementContext *_localctx = _tracker.createInstance<WhileStatementContext>(_ctx, getState());
  enterRule(_localctx, 38, Pcl4Parser::RuleWhileStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(208);
    match(Pcl4Parser::WHILE);
    setState(209);
    expression();
    setState(210);
    match(Pcl4Parser::DO);
    setState(211);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ForStatementContext ------------------------------------------------------------------

Pcl4Parser::ForStatementContext::ForStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::ForStatementContext::FOR() {
  return getToken(Pcl4Parser::FOR, 0);
}

Pcl4Parser::VariableContext* Pcl4Parser::ForStatementContext::variable() {
  return getRuleContext<Pcl4Parser::VariableContext>(0);
}

Pcl4Parser::ForRangeContext* Pcl4Parser::ForStatementContext::forRange() {
  return getRuleContext<Pcl4Parser::ForRangeContext>(0);
}

tree::TerminalNode* Pcl4Parser::ForStatementContext::DO() {
  return getToken(Pcl4Parser::DO, 0);
}

Pcl4Parser::StatementContext* Pcl4Parser::ForStatementContext::statement() {
  return getRuleContext<Pcl4Parser::StatementContext>(0);
}


size_t Pcl4Parser::ForStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleForStatement;
}


antlrcpp::Any Pcl4Parser::ForStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitForStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ForStatementContext* Pcl4Parser::forStatement() {
  ForStatementContext *_localctx = _tracker.createInstance<ForStatementContext>(_ctx, getState());
  enterRule(_localctx, 40, Pcl4Parser::RuleForStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(213);
    match(Pcl4Parser::FOR);
    setState(214);
    variable();
    setState(215);
    match(Pcl4Parser::T__5);
    setState(216);
    forRange();
    setState(217);
    match(Pcl4Parser::DO);
    setState(218);
    statement();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ForRangeContext ------------------------------------------------------------------

Pcl4Parser::ForRangeContext::ForRangeContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::ExpressionContext *> Pcl4Parser::ForRangeContext::expression() {
  return getRuleContexts<Pcl4Parser::ExpressionContext>();
}

Pcl4Parser::ExpressionContext* Pcl4Parser::ForRangeContext::expression(size_t i) {
  return getRuleContext<Pcl4Parser::ExpressionContext>(i);
}

tree::TerminalNode* Pcl4Parser::ForRangeContext::TO() {
  return getToken(Pcl4Parser::TO, 0);
}

tree::TerminalNode* Pcl4Parser::ForRangeContext::DOWNTO() {
  return getToken(Pcl4Parser::DOWNTO, 0);
}


size_t Pcl4Parser::ForRangeContext::getRuleIndex() const {
  return Pcl4Parser::RuleForRange;
}


antlrcpp::Any Pcl4Parser::ForRangeContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitForRange(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::ForRangeContext* Pcl4Parser::forRange() {
  ForRangeContext *_localctx = _tracker.createInstance<ForRangeContext>(_ctx, getState());
  enterRule(_localctx, 42, Pcl4Parser::RuleForRange);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(220);
    expression();
    setState(221);
    _la = _input->LA(1);
    if (!(_la == Pcl4Parser::TO

    || _la == Pcl4Parser::DOWNTO)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
    setState(222);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IfStatementContext ------------------------------------------------------------------

Pcl4Parser::IfStatementContext::IfStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::IfStatementContext::IF() {
  return getToken(Pcl4Parser::IF, 0);
}

Pcl4Parser::ExpressionContext* Pcl4Parser::IfStatementContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}

tree::TerminalNode* Pcl4Parser::IfStatementContext::THEN() {
  return getToken(Pcl4Parser::THEN, 0);
}

std::vector<Pcl4Parser::StatementContext *> Pcl4Parser::IfStatementContext::statement() {
  return getRuleContexts<Pcl4Parser::StatementContext>();
}

Pcl4Parser::StatementContext* Pcl4Parser::IfStatementContext::statement(size_t i) {
  return getRuleContext<Pcl4Parser::StatementContext>(i);
}

tree::TerminalNode* Pcl4Parser::IfStatementContext::ELSE() {
  return getToken(Pcl4Parser::ELSE, 0);
}


size_t Pcl4Parser::IfStatementContext::getRuleIndex() const {
  return Pcl4Parser::RuleIfStatement;
}


antlrcpp::Any Pcl4Parser::IfStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitIfStatement(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::IfStatementContext* Pcl4Parser::ifStatement() {
  IfStatementContext *_localctx = _tracker.createInstance<IfStatementContext>(_ctx, getState());
  enterRule(_localctx, 44, Pcl4Parser::RuleIfStatement);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(224);
    match(Pcl4Parser::IF);
    setState(225);
    expression();
    setState(226);
    match(Pcl4Parser::THEN);
    setState(227);
    statement();
    setState(230);
    _errHandler->sync(this);

    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      setState(228);
      match(Pcl4Parser::ELSE);
      setState(229);
      statement();
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TermContext ------------------------------------------------------------------

Pcl4Parser::TermContext::TermContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::FactorContext *> Pcl4Parser::TermContext::factor() {
  return getRuleContexts<Pcl4Parser::FactorContext>();
}

Pcl4Parser::FactorContext* Pcl4Parser::TermContext::factor(size_t i) {
  return getRuleContext<Pcl4Parser::FactorContext>(i);
}

std::vector<Pcl4Parser::MulOpContext *> Pcl4Parser::TermContext::mulOp() {
  return getRuleContexts<Pcl4Parser::MulOpContext>();
}

Pcl4Parser::MulOpContext* Pcl4Parser::TermContext::mulOp(size_t i) {
  return getRuleContext<Pcl4Parser::MulOpContext>(i);
}


size_t Pcl4Parser::TermContext::getRuleIndex() const {
  return Pcl4Parser::RuleTerm;
}


antlrcpp::Any Pcl4Parser::TermContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitTerm(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::TermContext* Pcl4Parser::term() {
  TermContext *_localctx = _tracker.createInstance<TermContext>(_ctx, getState());
  enterRule(_localctx, 46, Pcl4Parser::RuleTerm);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(232);
    factor();
    setState(238);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__15)
      | (1ULL << Pcl4Parser::T__16)
      | (1ULL << Pcl4Parser::DIV)
      | (1ULL << Pcl4Parser::MOD)
      | (1ULL << Pcl4Parser::AND))) != 0)) {
      setState(233);
      mulOp();
      setState(234);
      factor();
      setState(240);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FactorContext ------------------------------------------------------------------

Pcl4Parser::FactorContext::FactorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t Pcl4Parser::FactorContext::getRuleIndex() const {
  return Pcl4Parser::RuleFactor;
}

void Pcl4Parser::FactorContext::copyFrom(FactorContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- StringFactorContext ------------------------------------------------------------------

Pcl4Parser::StringConstantContext* Pcl4Parser::StringFactorContext::stringConstant() {
  return getRuleContext<Pcl4Parser::StringConstantContext>(0);
}

Pcl4Parser::StringFactorContext::StringFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::StringFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitStringFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- CharacterFactorContext ------------------------------------------------------------------

Pcl4Parser::CharacterConstantContext* Pcl4Parser::CharacterFactorContext::characterConstant() {
  return getRuleContext<Pcl4Parser::CharacterConstantContext>(0);
}

Pcl4Parser::CharacterFactorContext::CharacterFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::CharacterFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitCharacterFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- ParenthesizedExpressionContext ------------------------------------------------------------------

Pcl4Parser::ExpressionContext* Pcl4Parser::ParenthesizedExpressionContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}

Pcl4Parser::ParenthesizedExpressionContext::ParenthesizedExpressionContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::ParenthesizedExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitParenthesizedExpression(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NumberExpressionContext ------------------------------------------------------------------

Pcl4Parser::NumberContext* Pcl4Parser::NumberExpressionContext::number() {
  return getRuleContext<Pcl4Parser::NumberContext>(0);
}

Pcl4Parser::NumberExpressionContext::NumberExpressionContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::NumberExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitNumberExpression(this);
  else
    return visitor->visitChildren(this);
}
//----------------- NotFactorContext ------------------------------------------------------------------

tree::TerminalNode* Pcl4Parser::NotFactorContext::NOT() {
  return getToken(Pcl4Parser::NOT, 0);
}

Pcl4Parser::FactorContext* Pcl4Parser::NotFactorContext::factor() {
  return getRuleContext<Pcl4Parser::FactorContext>(0);
}

Pcl4Parser::NotFactorContext::NotFactorContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::NotFactorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitNotFactor(this);
  else
    return visitor->visitChildren(this);
}
//----------------- VariableExpressionContext ------------------------------------------------------------------

Pcl4Parser::VariableContext* Pcl4Parser::VariableExpressionContext::variable() {
  return getRuleContext<Pcl4Parser::VariableContext>(0);
}

Pcl4Parser::VariableExpressionContext::VariableExpressionContext(FactorContext *ctx) { copyFrom(ctx); }


antlrcpp::Any Pcl4Parser::VariableExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitVariableExpression(this);
  else
    return visitor->visitChildren(this);
}
Pcl4Parser::FactorContext* Pcl4Parser::factor() {
  FactorContext *_localctx = _tracker.createInstance<FactorContext>(_ctx, getState());
  enterRule(_localctx, 48, Pcl4Parser::RuleFactor);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(251);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case Pcl4Parser::IDENTIFIER: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::VariableExpressionContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(241);
        variable();
        break;
      }

      case Pcl4Parser::T__7:
      case Pcl4Parser::T__8:
      case Pcl4Parser::INTEGER:
      case Pcl4Parser::REAL: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::NumberExpressionContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(242);
        number();
        break;
      }

      case Pcl4Parser::CHARACTER: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::CharacterFactorContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(243);
        characterConstant();
        break;
      }

      case Pcl4Parser::STRING: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::StringFactorContext>(_localctx));
        enterOuterAlt(_localctx, 4);
        setState(244);
        stringConstant();
        break;
      }

      case Pcl4Parser::NOT: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::NotFactorContext>(_localctx));
        enterOuterAlt(_localctx, 5);
        setState(245);
        match(Pcl4Parser::NOT);
        setState(246);
        factor();
        break;
      }

      case Pcl4Parser::T__2: {
        _localctx = dynamic_cast<FactorContext *>(_tracker.createInstance<Pcl4Parser::ParenthesizedExpressionContext>(_localctx));
        enterOuterAlt(_localctx, 6);
        setState(247);
        match(Pcl4Parser::T__2);
        setState(248);
        expression();
        setState(249);
        match(Pcl4Parser::T__4);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableContext ------------------------------------------------------------------

Pcl4Parser::VariableContext::VariableContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::VariableContext::IDENTIFIER() {
  return getToken(Pcl4Parser::IDENTIFIER, 0);
}


size_t Pcl4Parser::VariableContext::getRuleIndex() const {
  return Pcl4Parser::RuleVariable;
}


antlrcpp::Any Pcl4Parser::VariableContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitVariable(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::VariableContext* Pcl4Parser::variable() {
  VariableContext *_localctx = _tracker.createInstance<VariableContext>(_ctx, getState());
  enterRule(_localctx, 50, Pcl4Parser::RuleVariable);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(253);
    match(Pcl4Parser::IDENTIFIER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NumberContext ------------------------------------------------------------------

Pcl4Parser::NumberContext::NumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::UnsignedNumberContext* Pcl4Parser::NumberContext::unsignedNumber() {
  return getRuleContext<Pcl4Parser::UnsignedNumberContext>(0);
}

Pcl4Parser::SignContext* Pcl4Parser::NumberContext::sign() {
  return getRuleContext<Pcl4Parser::SignContext>(0);
}


size_t Pcl4Parser::NumberContext::getRuleIndex() const {
  return Pcl4Parser::RuleNumber;
}


antlrcpp::Any Pcl4Parser::NumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitNumber(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::NumberContext* Pcl4Parser::number() {
  NumberContext *_localctx = _tracker.createInstance<NumberContext>(_ctx, getState());
  enterRule(_localctx, 52, Pcl4Parser::RuleNumber);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(256);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__7

    || _la == Pcl4Parser::T__8) {
      setState(255);
      sign();
    }
    setState(258);
    unsignedNumber();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- UnsignedNumberContext ------------------------------------------------------------------

Pcl4Parser::UnsignedNumberContext::UnsignedNumberContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::IntegerConstantContext* Pcl4Parser::UnsignedNumberContext::integerConstant() {
  return getRuleContext<Pcl4Parser::IntegerConstantContext>(0);
}

Pcl4Parser::RealConstantContext* Pcl4Parser::UnsignedNumberContext::realConstant() {
  return getRuleContext<Pcl4Parser::RealConstantContext>(0);
}


size_t Pcl4Parser::UnsignedNumberContext::getRuleIndex() const {
  return Pcl4Parser::RuleUnsignedNumber;
}


antlrcpp::Any Pcl4Parser::UnsignedNumberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitUnsignedNumber(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::UnsignedNumberContext* Pcl4Parser::unsignedNumber() {
  UnsignedNumberContext *_localctx = _tracker.createInstance<UnsignedNumberContext>(_ctx, getState());
  enterRule(_localctx, 54, Pcl4Parser::RuleUnsignedNumber);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(262);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case Pcl4Parser::INTEGER: {
        enterOuterAlt(_localctx, 1);
        setState(260);
        integerConstant();
        break;
      }

      case Pcl4Parser::REAL: {
        enterOuterAlt(_localctx, 2);
        setState(261);
        realConstant();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IntegerConstantContext ------------------------------------------------------------------

Pcl4Parser::IntegerConstantContext::IntegerConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::IntegerConstantContext::INTEGER() {
  return getToken(Pcl4Parser::INTEGER, 0);
}


size_t Pcl4Parser::IntegerConstantContext::getRuleIndex() const {
  return Pcl4Parser::RuleIntegerConstant;
}


antlrcpp::Any Pcl4Parser::IntegerConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitIntegerConstant(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::IntegerConstantContext* Pcl4Parser::integerConstant() {
  IntegerConstantContext *_localctx = _tracker.createInstance<IntegerConstantContext>(_ctx, getState());
  enterRule(_localctx, 56, Pcl4Parser::RuleIntegerConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(264);
    match(Pcl4Parser::INTEGER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RealConstantContext ------------------------------------------------------------------

Pcl4Parser::RealConstantContext::RealConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::RealConstantContext::REAL() {
  return getToken(Pcl4Parser::REAL, 0);
}


size_t Pcl4Parser::RealConstantContext::getRuleIndex() const {
  return Pcl4Parser::RuleRealConstant;
}


antlrcpp::Any Pcl4Parser::RealConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitRealConstant(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::RealConstantContext* Pcl4Parser::realConstant() {
  RealConstantContext *_localctx = _tracker.createInstance<RealConstantContext>(_ctx, getState());
  enterRule(_localctx, 58, Pcl4Parser::RuleRealConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(266);
    match(Pcl4Parser::REAL);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CharacterConstantContext ------------------------------------------------------------------

Pcl4Parser::CharacterConstantContext::CharacterConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::CharacterConstantContext::CHARACTER() {
  return getToken(Pcl4Parser::CHARACTER, 0);
}


size_t Pcl4Parser::CharacterConstantContext::getRuleIndex() const {
  return Pcl4Parser::RuleCharacterConstant;
}


antlrcpp::Any Pcl4Parser::CharacterConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitCharacterConstant(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::CharacterConstantContext* Pcl4Parser::characterConstant() {
  CharacterConstantContext *_localctx = _tracker.createInstance<CharacterConstantContext>(_ctx, getState());
  enterRule(_localctx, 60, Pcl4Parser::RuleCharacterConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(268);
    match(Pcl4Parser::CHARACTER);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StringConstantContext ------------------------------------------------------------------

Pcl4Parser::StringConstantContext::StringConstantContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::StringConstantContext::STRING() {
  return getToken(Pcl4Parser::STRING, 0);
}


size_t Pcl4Parser::StringConstantContext::getRuleIndex() const {
  return Pcl4Parser::RuleStringConstant;
}


antlrcpp::Any Pcl4Parser::StringConstantContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitStringConstant(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::StringConstantContext* Pcl4Parser::stringConstant() {
  StringConstantContext *_localctx = _tracker.createInstance<StringConstantContext>(_ctx, getState());
  enterRule(_localctx, 62, Pcl4Parser::RuleStringConstant);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(270);
    match(Pcl4Parser::STRING);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SignContext ------------------------------------------------------------------

Pcl4Parser::SignContext::SignContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t Pcl4Parser::SignContext::getRuleIndex() const {
  return Pcl4Parser::RuleSign;
}


antlrcpp::Any Pcl4Parser::SignContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitSign(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::SignContext* Pcl4Parser::sign() {
  SignContext *_localctx = _tracker.createInstance<SignContext>(_ctx, getState());
  enterRule(_localctx, 64, Pcl4Parser::RuleSign);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(272);
    _la = _input->LA(1);
    if (!(_la == Pcl4Parser::T__7

    || _la == Pcl4Parser::T__8)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RelOpContext ------------------------------------------------------------------

Pcl4Parser::RelOpContext::RelOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t Pcl4Parser::RelOpContext::getRuleIndex() const {
  return Pcl4Parser::RuleRelOp;
}


antlrcpp::Any Pcl4Parser::RelOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitRelOp(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::RelOpContext* Pcl4Parser::relOp() {
  RelOpContext *_localctx = _tracker.createInstance<RelOpContext>(_ctx, getState());
  enterRule(_localctx, 66, Pcl4Parser::RuleRelOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(274);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__9)
      | (1ULL << Pcl4Parser::T__10)
      | (1ULL << Pcl4Parser::T__11)
      | (1ULL << Pcl4Parser::T__12)
      | (1ULL << Pcl4Parser::T__13)
      | (1ULL << Pcl4Parser::T__14))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AddOpContext ------------------------------------------------------------------

Pcl4Parser::AddOpContext::AddOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::AddOpContext::OR() {
  return getToken(Pcl4Parser::OR, 0);
}


size_t Pcl4Parser::AddOpContext::getRuleIndex() const {
  return Pcl4Parser::RuleAddOp;
}


antlrcpp::Any Pcl4Parser::AddOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitAddOp(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::AddOpContext* Pcl4Parser::addOp() {
  AddOpContext *_localctx = _tracker.createInstance<AddOpContext>(_ctx, getState());
  enterRule(_localctx, 68, Pcl4Parser::RuleAddOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(276);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__7)
      | (1ULL << Pcl4Parser::T__8)
      | (1ULL << Pcl4Parser::OR))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- MulOpContext ------------------------------------------------------------------

Pcl4Parser::MulOpContext::MulOpContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* Pcl4Parser::MulOpContext::DIV() {
  return getToken(Pcl4Parser::DIV, 0);
}

tree::TerminalNode* Pcl4Parser::MulOpContext::MOD() {
  return getToken(Pcl4Parser::MOD, 0);
}

tree::TerminalNode* Pcl4Parser::MulOpContext::AND() {
  return getToken(Pcl4Parser::AND, 0);
}


size_t Pcl4Parser::MulOpContext::getRuleIndex() const {
  return Pcl4Parser::RuleMulOp;
}


antlrcpp::Any Pcl4Parser::MulOpContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitMulOp(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::MulOpContext* Pcl4Parser::mulOp() {
  MulOpContext *_localctx = _tracker.createInstance<MulOpContext>(_ctx, getState());
  enterRule(_localctx, 70, Pcl4Parser::RuleMulOp);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(278);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << Pcl4Parser::T__15)
      | (1ULL << Pcl4Parser::T__16)
      | (1ULL << Pcl4Parser::DIV)
      | (1ULL << Pcl4Parser::MOD)
      | (1ULL << Pcl4Parser::AND))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentsOnContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentsOnContext::WriteArgumentsOnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::WriteArgumentListOnContext* Pcl4Parser::WriteArgumentsOnContext::writeArgumentListOn() {
  return getRuleContext<Pcl4Parser::WriteArgumentListOnContext>(0);
}


size_t Pcl4Parser::WriteArgumentsOnContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgumentsOn;
}


antlrcpp::Any Pcl4Parser::WriteArgumentsOnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgumentsOn(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentsOnContext* Pcl4Parser::writeArgumentsOn() {
  WriteArgumentsOnContext *_localctx = _tracker.createInstance<WriteArgumentsOnContext>(_ctx, getState());
  enterRule(_localctx, 72, Pcl4Parser::RuleWriteArgumentsOn);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(280);
    match(Pcl4Parser::T__2);
    setState(281);
    writeArgumentListOn();
    setState(282);
    match(Pcl4Parser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentListOnContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentListOnContext::WriteArgumentListOnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::WriteArgumentListContext* Pcl4Parser::WriteArgumentListOnContext::writeArgumentList() {
  return getRuleContext<Pcl4Parser::WriteArgumentListContext>(0);
}


size_t Pcl4Parser::WriteArgumentListOnContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgumentListOn;
}


antlrcpp::Any Pcl4Parser::WriteArgumentListOnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgumentListOn(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentListOnContext* Pcl4Parser::writeArgumentListOn() {
  WriteArgumentListOnContext *_localctx = _tracker.createInstance<WriteArgumentListOnContext>(_ctx, getState());
  enterRule(_localctx, 74, Pcl4Parser::RuleWriteArgumentListOn);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(284);
    writeArgumentList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentsLnContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentsLnContext::WriteArgumentsLnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::WriteArgumentListLnContext* Pcl4Parser::WriteArgumentsLnContext::writeArgumentListLn() {
  return getRuleContext<Pcl4Parser::WriteArgumentListLnContext>(0);
}


size_t Pcl4Parser::WriteArgumentsLnContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgumentsLn;
}


antlrcpp::Any Pcl4Parser::WriteArgumentsLnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgumentsLn(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentsLnContext* Pcl4Parser::writeArgumentsLn() {
  WriteArgumentsLnContext *_localctx = _tracker.createInstance<WriteArgumentsLnContext>(_ctx, getState());
  enterRule(_localctx, 76, Pcl4Parser::RuleWriteArgumentsLn);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(286);
    match(Pcl4Parser::T__2);
    setState(287);
    writeArgumentListLn();
    setState(288);
    match(Pcl4Parser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentListLnContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentListLnContext::WriteArgumentListLnContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::WriteArgumentListContext* Pcl4Parser::WriteArgumentListLnContext::writeArgumentList() {
  return getRuleContext<Pcl4Parser::WriteArgumentListContext>(0);
}


size_t Pcl4Parser::WriteArgumentListLnContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgumentListLn;
}


antlrcpp::Any Pcl4Parser::WriteArgumentListLnContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgumentListLn(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentListLnContext* Pcl4Parser::writeArgumentListLn() {
  WriteArgumentListLnContext *_localctx = _tracker.createInstance<WriteArgumentListLnContext>(_ctx, getState());
  enterRule(_localctx, 78, Pcl4Parser::RuleWriteArgumentListLn);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(290);
    writeArgumentList();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentListContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentListContext::WriteArgumentListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<Pcl4Parser::WriteArgumentContext *> Pcl4Parser::WriteArgumentListContext::writeArgument() {
  return getRuleContexts<Pcl4Parser::WriteArgumentContext>();
}

Pcl4Parser::WriteArgumentContext* Pcl4Parser::WriteArgumentListContext::writeArgument(size_t i) {
  return getRuleContext<Pcl4Parser::WriteArgumentContext>(i);
}


size_t Pcl4Parser::WriteArgumentListContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgumentList;
}


antlrcpp::Any Pcl4Parser::WriteArgumentListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgumentList(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentListContext* Pcl4Parser::writeArgumentList() {
  WriteArgumentListContext *_localctx = _tracker.createInstance<WriteArgumentListContext>(_ctx, getState());
  enterRule(_localctx, 80, Pcl4Parser::RuleWriteArgumentList);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(292);
    writeArgument();
    setState(297);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == Pcl4Parser::T__3) {
      setState(293);
      match(Pcl4Parser::T__3);
      setState(294);
      writeArgument();
      setState(299);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WriteArgumentContext ------------------------------------------------------------------

Pcl4Parser::WriteArgumentContext::WriteArgumentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::ExpressionContext* Pcl4Parser::WriteArgumentContext::expression() {
  return getRuleContext<Pcl4Parser::ExpressionContext>(0);
}

Pcl4Parser::FieldWidthContext* Pcl4Parser::WriteArgumentContext::fieldWidth() {
  return getRuleContext<Pcl4Parser::FieldWidthContext>(0);
}


size_t Pcl4Parser::WriteArgumentContext::getRuleIndex() const {
  return Pcl4Parser::RuleWriteArgument;
}


antlrcpp::Any Pcl4Parser::WriteArgumentContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitWriteArgument(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::WriteArgumentContext* Pcl4Parser::writeArgument() {
  WriteArgumentContext *_localctx = _tracker.createInstance<WriteArgumentContext>(_ctx, getState());
  enterRule(_localctx, 82, Pcl4Parser::RuleWriteArgument);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(300);
    expression();
    setState(303);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__6) {
      setState(301);
      match(Pcl4Parser::T__6);
      setState(302);
      fieldWidth();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FieldWidthContext ------------------------------------------------------------------

Pcl4Parser::FieldWidthContext::FieldWidthContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::IntegerConstantContext* Pcl4Parser::FieldWidthContext::integerConstant() {
  return getRuleContext<Pcl4Parser::IntegerConstantContext>(0);
}

Pcl4Parser::SignContext* Pcl4Parser::FieldWidthContext::sign() {
  return getRuleContext<Pcl4Parser::SignContext>(0);
}

Pcl4Parser::DecimalPlacesContext* Pcl4Parser::FieldWidthContext::decimalPlaces() {
  return getRuleContext<Pcl4Parser::DecimalPlacesContext>(0);
}


size_t Pcl4Parser::FieldWidthContext::getRuleIndex() const {
  return Pcl4Parser::RuleFieldWidth;
}


antlrcpp::Any Pcl4Parser::FieldWidthContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitFieldWidth(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::FieldWidthContext* Pcl4Parser::fieldWidth() {
  FieldWidthContext *_localctx = _tracker.createInstance<FieldWidthContext>(_ctx, getState());
  enterRule(_localctx, 84, Pcl4Parser::RuleFieldWidth);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(306);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__7

    || _la == Pcl4Parser::T__8) {
      setState(305);
      sign();
    }
    setState(308);
    integerConstant();
    setState(311);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == Pcl4Parser::T__6) {
      setState(309);
      match(Pcl4Parser::T__6);
      setState(310);
      decimalPlaces();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DecimalPlacesContext ------------------------------------------------------------------

Pcl4Parser::DecimalPlacesContext::DecimalPlacesContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

Pcl4Parser::IntegerConstantContext* Pcl4Parser::DecimalPlacesContext::integerConstant() {
  return getRuleContext<Pcl4Parser::IntegerConstantContext>(0);
}


size_t Pcl4Parser::DecimalPlacesContext::getRuleIndex() const {
  return Pcl4Parser::RuleDecimalPlaces;
}


antlrcpp::Any Pcl4Parser::DecimalPlacesContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<Pcl4Visitor*>(visitor))
    return parserVisitor->visitDecimalPlaces(this);
  else
    return visitor->visitChildren(this);
}

Pcl4Parser::DecimalPlacesContext* Pcl4Parser::decimalPlaces() {
  DecimalPlacesContext *_localctx = _tracker.createInstance<DecimalPlacesContext>(_ctx, getState());
  enterRule(_localctx, 86, Pcl4Parser::RuleDecimalPlaces);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(313);
    integerConstant();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> Pcl4Parser::_decisionToDFA;
atn::PredictionContextCache Pcl4Parser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN Pcl4Parser::_atn;
std::vector<uint16_t> Pcl4Parser::_serializedATN;

std::vector<std::string> Pcl4Parser::_ruleNames = {
  "program", "programHeader", "programParameters", "block", "declarations", 
  "statement", "compoundStatement", "emptyStatement", "statementList", "assignmentStatement", 
  "repeatStatement", "lhs", "rhs", "writeStatement", "writelnStatement", 
  "expression", "simpleExpression", "caseStatement", "cases", "whileStatement", 
  "forStatement", "forRange", "ifStatement", "term", "factor", "variable", 
  "number", "unsignedNumber", "integerConstant", "realConstant", "characterConstant", 
  "stringConstant", "sign", "relOp", "addOp", "mulOp", "writeArgumentsOn", 
  "writeArgumentListOn", "writeArgumentsLn", "writeArgumentListLn", "writeArgumentList", 
  "writeArgument", "fieldWidth", "decimalPlaces"
};

std::vector<std::string> Pcl4Parser::_literalNames = {
  "", "'.'", "';'", "'('", "','", "')'", "':='", "':'", "'-'", "'+'", "'='", 
  "'<>'", "'<'", "'<='", "'>'", "'>='", "'*'", "'/'", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "'''"
};

std::vector<std::string> Pcl4Parser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "PROGRAM", "CONST", "TYPE", "ARRAY", "OF", "RECORD", "VAR", "BEGIN", "END", 
  "DIV", "MOD", "AND", "OR", "NOT", "IF", "THEN", "ELSE", "CASE", "REPEAT", 
  "UNTIL", "WHILE", "DO", "FOR", "TO", "DOWNTO", "WRITE", "WRITELN", "READ", 
  "READLN", "PROCEDURE", "FUNCTION", "IDENTIFIER", "INTEGER", "REAL", "NEWLINE", 
  "WS", "QUOTE", "CHARACTER", "STRING"
};

dfa::Vocabulary Pcl4Parser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> Pcl4Parser::_tokenNames;

Pcl4Parser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x3a, 0x13e, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x4, 0x23, 0x9, 0x23, 0x4, 0x24, 0x9, 0x24, 0x4, 0x25, 0x9, 
    0x25, 0x4, 0x26, 0x9, 0x26, 0x4, 0x27, 0x9, 0x27, 0x4, 0x28, 0x9, 0x28, 
    0x4, 0x29, 0x9, 0x29, 0x4, 0x2a, 0x9, 0x2a, 0x4, 0x2b, 0x9, 0x2b, 0x4, 
    0x2c, 0x9, 0x2c, 0x4, 0x2d, 0x9, 0x2d, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 
    0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x62, 0xa, 0x3, 0x3, 
    0x3, 0x3, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x7, 0x4, 0x6a, 
    0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x6d, 0xb, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 
    0x7, 0x5, 0x7, 0x80, 0xa, 0x7, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 0x3, 0x8, 
    0x3, 0x9, 0x3, 0x9, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x7, 0xa, 0x8b, 0xa, 
    0xa, 0xc, 0xa, 0xe, 0xa, 0x8e, 0xb, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 
    0x3, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x3, 0xd, 
    0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 
    0x3, 0x10, 0x5, 0x10, 0xa2, 0xa, 0x10, 0x3, 0x11, 0x3, 0x11, 0x3, 0x11, 
    0x3, 0x11, 0x5, 0x11, 0xa8, 0xa, 0x11, 0x3, 0x12, 0x5, 0x12, 0xab, 0xa, 
    0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x7, 0x12, 0xb1, 0xa, 
    0x12, 0xc, 0x12, 0xe, 0x12, 0xb4, 0xb, 0x12, 0x3, 0x13, 0x3, 0x13, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x7, 0x13, 0xbc, 0xa, 0x13, 0xc, 
    0x13, 0xe, 0x13, 0xbf, 0xb, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x5, 
    0x13, 0xc4, 0xa, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 
    0x14, 0x7, 0x14, 0xcb, 0xa, 0x14, 0xc, 0x14, 0xe, 0x14, 0xce, 0xb, 0x14, 
    0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 
    0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x3, 0x16, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 
    0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x3, 0x18, 0x5, 0x18, 
    0xe9, 0xa, 0x18, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x3, 0x19, 0x7, 0x19, 
    0xef, 0xa, 0x19, 0xc, 0x19, 0xe, 0x19, 0xf2, 0xb, 0x19, 0x3, 0x1a, 0x3, 
    0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1a, 
    0x3, 0x1a, 0x3, 0x1a, 0x5, 0x1a, 0xfe, 0xa, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 
    0x3, 0x1c, 0x5, 0x1c, 0x103, 0xa, 0x1c, 0x3, 0x1c, 0x3, 0x1c, 0x3, 0x1d, 
    0x3, 0x1d, 0x5, 0x1d, 0x109, 0xa, 0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1f, 
    0x3, 0x1f, 0x3, 0x20, 0x3, 0x20, 0x3, 0x21, 0x3, 0x21, 0x3, 0x22, 0x3, 
    0x22, 0x3, 0x23, 0x3, 0x23, 0x3, 0x24, 0x3, 0x24, 0x3, 0x25, 0x3, 0x25, 
    0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x3, 0x26, 0x3, 0x27, 0x3, 0x27, 0x3, 
    0x28, 0x3, 0x28, 0x3, 0x28, 0x3, 0x28, 0x3, 0x29, 0x3, 0x29, 0x3, 0x2a, 
    0x3, 0x2a, 0x3, 0x2a, 0x7, 0x2a, 0x12a, 0xa, 0x2a, 0xc, 0x2a, 0xe, 0x2a, 
    0x12d, 0xb, 0x2a, 0x3, 0x2b, 0x3, 0x2b, 0x3, 0x2b, 0x5, 0x2b, 0x132, 
    0xa, 0x2b, 0x3, 0x2c, 0x5, 0x2c, 0x135, 0xa, 0x2c, 0x3, 0x2c, 0x3, 0x2c, 
    0x3, 0x2c, 0x5, 0x2c, 0x13a, 0xa, 0x2c, 0x3, 0x2d, 0x3, 0x2d, 0x3, 0x2d, 
    0x2, 0x2, 0x2e, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 
    0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 
    0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x44, 
    0x46, 0x48, 0x4a, 0x4c, 0x4e, 0x50, 0x52, 0x54, 0x56, 0x58, 0x2, 0x7, 
    0x3, 0x2, 0x2b, 0x2c, 0x3, 0x2, 0xa, 0xb, 0x3, 0x2, 0xc, 0x11, 0x4, 
    0x2, 0xa, 0xb, 0x20, 0x20, 0x4, 0x2, 0x12, 0x13, 0x1d, 0x1f, 0x2, 0x131, 
    0x2, 0x5a, 0x3, 0x2, 0x2, 0x2, 0x4, 0x5e, 0x3, 0x2, 0x2, 0x2, 0x6, 0x65, 
    0x3, 0x2, 0x2, 0x2, 0x8, 0x70, 0x3, 0x2, 0x2, 0x2, 0xa, 0x73, 0x3, 0x2, 
    0x2, 0x2, 0xc, 0x7f, 0x3, 0x2, 0x2, 0x2, 0xe, 0x81, 0x3, 0x2, 0x2, 0x2, 
    0x10, 0x85, 0x3, 0x2, 0x2, 0x2, 0x12, 0x87, 0x3, 0x2, 0x2, 0x2, 0x14, 
    0x8f, 0x3, 0x2, 0x2, 0x2, 0x16, 0x93, 0x3, 0x2, 0x2, 0x2, 0x18, 0x98, 
    0x3, 0x2, 0x2, 0x2, 0x1a, 0x9a, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x9c, 0x3, 
    0x2, 0x2, 0x2, 0x1e, 0x9f, 0x3, 0x2, 0x2, 0x2, 0x20, 0xa3, 0x3, 0x2, 
    0x2, 0x2, 0x22, 0xaa, 0x3, 0x2, 0x2, 0x2, 0x24, 0xb5, 0x3, 0x2, 0x2, 
    0x2, 0x26, 0xc7, 0x3, 0x2, 0x2, 0x2, 0x28, 0xd2, 0x3, 0x2, 0x2, 0x2, 
    0x2a, 0xd7, 0x3, 0x2, 0x2, 0x2, 0x2c, 0xde, 0x3, 0x2, 0x2, 0x2, 0x2e, 
    0xe2, 0x3, 0x2, 0x2, 0x2, 0x30, 0xea, 0x3, 0x2, 0x2, 0x2, 0x32, 0xfd, 
    0x3, 0x2, 0x2, 0x2, 0x34, 0xff, 0x3, 0x2, 0x2, 0x2, 0x36, 0x102, 0x3, 
    0x2, 0x2, 0x2, 0x38, 0x108, 0x3, 0x2, 0x2, 0x2, 0x3a, 0x10a, 0x3, 0x2, 
    0x2, 0x2, 0x3c, 0x10c, 0x3, 0x2, 0x2, 0x2, 0x3e, 0x10e, 0x3, 0x2, 0x2, 
    0x2, 0x40, 0x110, 0x3, 0x2, 0x2, 0x2, 0x42, 0x112, 0x3, 0x2, 0x2, 0x2, 
    0x44, 0x114, 0x3, 0x2, 0x2, 0x2, 0x46, 0x116, 0x3, 0x2, 0x2, 0x2, 0x48, 
    0x118, 0x3, 0x2, 0x2, 0x2, 0x4a, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x4c, 0x11e, 
    0x3, 0x2, 0x2, 0x2, 0x4e, 0x120, 0x3, 0x2, 0x2, 0x2, 0x50, 0x124, 0x3, 
    0x2, 0x2, 0x2, 0x52, 0x126, 0x3, 0x2, 0x2, 0x2, 0x54, 0x12e, 0x3, 0x2, 
    0x2, 0x2, 0x56, 0x134, 0x3, 0x2, 0x2, 0x2, 0x58, 0x13b, 0x3, 0x2, 0x2, 
    0x2, 0x5a, 0x5b, 0x5, 0x4, 0x3, 0x2, 0x5b, 0x5c, 0x5, 0x8, 0x5, 0x2, 
    0x5c, 0x5d, 0x7, 0x3, 0x2, 0x2, 0x5d, 0x3, 0x3, 0x2, 0x2, 0x2, 0x5e, 
    0x5f, 0x7, 0x14, 0x2, 0x2, 0x5f, 0x61, 0x7, 0x33, 0x2, 0x2, 0x60, 0x62, 
    0x5, 0x6, 0x4, 0x2, 0x61, 0x60, 0x3, 0x2, 0x2, 0x2, 0x61, 0x62, 0x3, 
    0x2, 0x2, 0x2, 0x62, 0x63, 0x3, 0x2, 0x2, 0x2, 0x63, 0x64, 0x7, 0x4, 
    0x2, 0x2, 0x64, 0x5, 0x3, 0x2, 0x2, 0x2, 0x65, 0x66, 0x7, 0x5, 0x2, 
    0x2, 0x66, 0x6b, 0x7, 0x33, 0x2, 0x2, 0x67, 0x68, 0x7, 0x6, 0x2, 0x2, 
    0x68, 0x6a, 0x7, 0x33, 0x2, 0x2, 0x69, 0x67, 0x3, 0x2, 0x2, 0x2, 0x6a, 
    0x6d, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x69, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x6c, 
    0x3, 0x2, 0x2, 0x2, 0x6c, 0x6e, 0x3, 0x2, 0x2, 0x2, 0x6d, 0x6b, 0x3, 
    0x2, 0x2, 0x2, 0x6e, 0x6f, 0x7, 0x7, 0x2, 0x2, 0x6f, 0x7, 0x3, 0x2, 
    0x2, 0x2, 0x70, 0x71, 0x5, 0xa, 0x6, 0x2, 0x71, 0x72, 0x5, 0xe, 0x8, 
    0x2, 0x72, 0x9, 0x3, 0x2, 0x2, 0x2, 0x73, 0x74, 0x3, 0x2, 0x2, 0x2, 
    0x74, 0xb, 0x3, 0x2, 0x2, 0x2, 0x75, 0x80, 0x5, 0xe, 0x8, 0x2, 0x76, 
    0x80, 0x5, 0x14, 0xb, 0x2, 0x77, 0x80, 0x5, 0x16, 0xc, 0x2, 0x78, 0x80, 
    0x5, 0x1c, 0xf, 0x2, 0x79, 0x80, 0x5, 0x1e, 0x10, 0x2, 0x7a, 0x80, 0x5, 
    0x10, 0x9, 0x2, 0x7b, 0x80, 0x5, 0x24, 0x13, 0x2, 0x7c, 0x80, 0x5, 0x2a, 
    0x16, 0x2, 0x7d, 0x80, 0x5, 0x28, 0x15, 0x2, 0x7e, 0x80, 0x5, 0x2e, 
    0x18, 0x2, 0x7f, 0x75, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x76, 0x3, 0x2, 0x2, 
    0x2, 0x7f, 0x77, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x78, 0x3, 0x2, 0x2, 0x2, 
    0x7f, 0x79, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x7a, 0x3, 0x2, 0x2, 0x2, 0x7f, 
    0x7b, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x7c, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x7d, 
    0x3, 0x2, 0x2, 0x2, 0x7f, 0x7e, 0x3, 0x2, 0x2, 0x2, 0x80, 0xd, 0x3, 
    0x2, 0x2, 0x2, 0x81, 0x82, 0x7, 0x1b, 0x2, 0x2, 0x82, 0x83, 0x5, 0x12, 
    0xa, 0x2, 0x83, 0x84, 0x7, 0x1c, 0x2, 0x2, 0x84, 0xf, 0x3, 0x2, 0x2, 
    0x2, 0x85, 0x86, 0x3, 0x2, 0x2, 0x2, 0x86, 0x11, 0x3, 0x2, 0x2, 0x2, 
    0x87, 0x8c, 0x5, 0xc, 0x7, 0x2, 0x88, 0x89, 0x7, 0x4, 0x2, 0x2, 0x89, 
    0x8b, 0x5, 0xc, 0x7, 0x2, 0x8a, 0x88, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x8e, 
    0x3, 0x2, 0x2, 0x2, 0x8c, 0x8a, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x8d, 0x3, 
    0x2, 0x2, 0x2, 0x8d, 0x13, 0x3, 0x2, 0x2, 0x2, 0x8e, 0x8c, 0x3, 0x2, 
    0x2, 0x2, 0x8f, 0x90, 0x5, 0x18, 0xd, 0x2, 0x90, 0x91, 0x7, 0x8, 0x2, 
    0x2, 0x91, 0x92, 0x5, 0x1a, 0xe, 0x2, 0x92, 0x15, 0x3, 0x2, 0x2, 0x2, 
    0x93, 0x94, 0x7, 0x26, 0x2, 0x2, 0x94, 0x95, 0x5, 0x12, 0xa, 0x2, 0x95, 
    0x96, 0x7, 0x27, 0x2, 0x2, 0x96, 0x97, 0x5, 0x20, 0x11, 0x2, 0x97, 0x17, 
    0x3, 0x2, 0x2, 0x2, 0x98, 0x99, 0x5, 0x34, 0x1b, 0x2, 0x99, 0x19, 0x3, 
    0x2, 0x2, 0x2, 0x9a, 0x9b, 0x5, 0x20, 0x11, 0x2, 0x9b, 0x1b, 0x3, 0x2, 
    0x2, 0x2, 0x9c, 0x9d, 0x7, 0x2d, 0x2, 0x2, 0x9d, 0x9e, 0x5, 0x4a, 0x26, 
    0x2, 0x9e, 0x1d, 0x3, 0x2, 0x2, 0x2, 0x9f, 0xa1, 0x7, 0x2e, 0x2, 0x2, 
    0xa0, 0xa2, 0x5, 0x4e, 0x28, 0x2, 0xa1, 0xa0, 0x3, 0x2, 0x2, 0x2, 0xa1, 
    0xa2, 0x3, 0x2, 0x2, 0x2, 0xa2, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xa3, 0xa7, 
    0x5, 0x22, 0x12, 0x2, 0xa4, 0xa5, 0x5, 0x44, 0x23, 0x2, 0xa5, 0xa6, 
    0x5, 0x22, 0x12, 0x2, 0xa6, 0xa8, 0x3, 0x2, 0x2, 0x2, 0xa7, 0xa4, 0x3, 
    0x2, 0x2, 0x2, 0xa7, 0xa8, 0x3, 0x2, 0x2, 0x2, 0xa8, 0x21, 0x3, 0x2, 
    0x2, 0x2, 0xa9, 0xab, 0x5, 0x42, 0x22, 0x2, 0xaa, 0xa9, 0x3, 0x2, 0x2, 
    0x2, 0xaa, 0xab, 0x3, 0x2, 0x2, 0x2, 0xab, 0xac, 0x3, 0x2, 0x2, 0x2, 
    0xac, 0xb2, 0x5, 0x30, 0x19, 0x2, 0xad, 0xae, 0x5, 0x46, 0x24, 0x2, 
    0xae, 0xaf, 0x5, 0x30, 0x19, 0x2, 0xaf, 0xb1, 0x3, 0x2, 0x2, 0x2, 0xb0, 
    0xad, 0x3, 0x2, 0x2, 0x2, 0xb1, 0xb4, 0x3, 0x2, 0x2, 0x2, 0xb2, 0xb0, 
    0x3, 0x2, 0x2, 0x2, 0xb2, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xb3, 0x23, 0x3, 
    0x2, 0x2, 0x2, 0xb4, 0xb2, 0x3, 0x2, 0x2, 0x2, 0xb5, 0xb6, 0x7, 0x25, 
    0x2, 0x2, 0xb6, 0xb7, 0x5, 0x20, 0x11, 0x2, 0xb7, 0xb8, 0x7, 0x18, 0x2, 
    0x2, 0xb8, 0xbd, 0x5, 0x26, 0x14, 0x2, 0xb9, 0xba, 0x7, 0x4, 0x2, 0x2, 
    0xba, 0xbc, 0x5, 0x26, 0x14, 0x2, 0xbb, 0xb9, 0x3, 0x2, 0x2, 0x2, 0xbc, 
    0xbf, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xbe, 
    0x3, 0x2, 0x2, 0x2, 0xbe, 0xc3, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xbd, 0x3, 
    0x2, 0x2, 0x2, 0xc0, 0xc1, 0x7, 0x4, 0x2, 0x2, 0xc1, 0xc2, 0x7, 0x24, 
    0x2, 0x2, 0xc2, 0xc4, 0x5, 0x12, 0xa, 0x2, 0xc3, 0xc0, 0x3, 0x2, 0x2, 
    0x2, 0xc3, 0xc4, 0x3, 0x2, 0x2, 0x2, 0xc4, 0xc5, 0x3, 0x2, 0x2, 0x2, 
    0xc5, 0xc6, 0x7, 0x1c, 0x2, 0x2, 0xc6, 0x25, 0x3, 0x2, 0x2, 0x2, 0xc7, 
    0xcc, 0x5, 0x20, 0x11, 0x2, 0xc8, 0xc9, 0x7, 0x6, 0x2, 0x2, 0xc9, 0xcb, 
    0x5, 0x20, 0x11, 0x2, 0xca, 0xc8, 0x3, 0x2, 0x2, 0x2, 0xcb, 0xce, 0x3, 
    0x2, 0x2, 0x2, 0xcc, 0xca, 0x3, 0x2, 0x2, 0x2, 0xcc, 0xcd, 0x3, 0x2, 
    0x2, 0x2, 0xcd, 0xcf, 0x3, 0x2, 0x2, 0x2, 0xce, 0xcc, 0x3, 0x2, 0x2, 
    0x2, 0xcf, 0xd0, 0x7, 0x9, 0x2, 0x2, 0xd0, 0xd1, 0x5, 0x12, 0xa, 0x2, 
    0xd1, 0x27, 0x3, 0x2, 0x2, 0x2, 0xd2, 0xd3, 0x7, 0x28, 0x2, 0x2, 0xd3, 
    0xd4, 0x5, 0x20, 0x11, 0x2, 0xd4, 0xd5, 0x7, 0x29, 0x2, 0x2, 0xd5, 0xd6, 
    0x5, 0xc, 0x7, 0x2, 0xd6, 0x29, 0x3, 0x2, 0x2, 0x2, 0xd7, 0xd8, 0x7, 
    0x2a, 0x2, 0x2, 0xd8, 0xd9, 0x5, 0x34, 0x1b, 0x2, 0xd9, 0xda, 0x7, 0x8, 
    0x2, 0x2, 0xda, 0xdb, 0x5, 0x2c, 0x17, 0x2, 0xdb, 0xdc, 0x7, 0x29, 0x2, 
    0x2, 0xdc, 0xdd, 0x5, 0xc, 0x7, 0x2, 0xdd, 0x2b, 0x3, 0x2, 0x2, 0x2, 
    0xde, 0xdf, 0x5, 0x20, 0x11, 0x2, 0xdf, 0xe0, 0x9, 0x2, 0x2, 0x2, 0xe0, 
    0xe1, 0x5, 0x20, 0x11, 0x2, 0xe1, 0x2d, 0x3, 0x2, 0x2, 0x2, 0xe2, 0xe3, 
    0x7, 0x22, 0x2, 0x2, 0xe3, 0xe4, 0x5, 0x20, 0x11, 0x2, 0xe4, 0xe5, 0x7, 
    0x23, 0x2, 0x2, 0xe5, 0xe8, 0x5, 0xc, 0x7, 0x2, 0xe6, 0xe7, 0x7, 0x24, 
    0x2, 0x2, 0xe7, 0xe9, 0x5, 0xc, 0x7, 0x2, 0xe8, 0xe6, 0x3, 0x2, 0x2, 
    0x2, 0xe8, 0xe9, 0x3, 0x2, 0x2, 0x2, 0xe9, 0x2f, 0x3, 0x2, 0x2, 0x2, 
    0xea, 0xf0, 0x5, 0x32, 0x1a, 0x2, 0xeb, 0xec, 0x5, 0x48, 0x25, 0x2, 
    0xec, 0xed, 0x5, 0x32, 0x1a, 0x2, 0xed, 0xef, 0x3, 0x2, 0x2, 0x2, 0xee, 
    0xeb, 0x3, 0x2, 0x2, 0x2, 0xef, 0xf2, 0x3, 0x2, 0x2, 0x2, 0xf0, 0xee, 
    0x3, 0x2, 0x2, 0x2, 0xf0, 0xf1, 0x3, 0x2, 0x2, 0x2, 0xf1, 0x31, 0x3, 
    0x2, 0x2, 0x2, 0xf2, 0xf0, 0x3, 0x2, 0x2, 0x2, 0xf3, 0xfe, 0x5, 0x34, 
    0x1b, 0x2, 0xf4, 0xfe, 0x5, 0x36, 0x1c, 0x2, 0xf5, 0xfe, 0x5, 0x3e, 
    0x20, 0x2, 0xf6, 0xfe, 0x5, 0x40, 0x21, 0x2, 0xf7, 0xf8, 0x7, 0x21, 
    0x2, 0x2, 0xf8, 0xfe, 0x5, 0x32, 0x1a, 0x2, 0xf9, 0xfa, 0x7, 0x5, 0x2, 
    0x2, 0xfa, 0xfb, 0x5, 0x20, 0x11, 0x2, 0xfb, 0xfc, 0x7, 0x7, 0x2, 0x2, 
    0xfc, 0xfe, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xf3, 0x3, 0x2, 0x2, 0x2, 0xfd, 
    0xf4, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xf5, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xf6, 
    0x3, 0x2, 0x2, 0x2, 0xfd, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xfd, 0xf9, 0x3, 
    0x2, 0x2, 0x2, 0xfe, 0x33, 0x3, 0x2, 0x2, 0x2, 0xff, 0x100, 0x7, 0x33, 
    0x2, 0x2, 0x100, 0x35, 0x3, 0x2, 0x2, 0x2, 0x101, 0x103, 0x5, 0x42, 
    0x22, 0x2, 0x102, 0x101, 0x3, 0x2, 0x2, 0x2, 0x102, 0x103, 0x3, 0x2, 
    0x2, 0x2, 0x103, 0x104, 0x3, 0x2, 0x2, 0x2, 0x104, 0x105, 0x5, 0x38, 
    0x1d, 0x2, 0x105, 0x37, 0x3, 0x2, 0x2, 0x2, 0x106, 0x109, 0x5, 0x3a, 
    0x1e, 0x2, 0x107, 0x109, 0x5, 0x3c, 0x1f, 0x2, 0x108, 0x106, 0x3, 0x2, 
    0x2, 0x2, 0x108, 0x107, 0x3, 0x2, 0x2, 0x2, 0x109, 0x39, 0x3, 0x2, 0x2, 
    0x2, 0x10a, 0x10b, 0x7, 0x34, 0x2, 0x2, 0x10b, 0x3b, 0x3, 0x2, 0x2, 
    0x2, 0x10c, 0x10d, 0x7, 0x35, 0x2, 0x2, 0x10d, 0x3d, 0x3, 0x2, 0x2, 
    0x2, 0x10e, 0x10f, 0x7, 0x39, 0x2, 0x2, 0x10f, 0x3f, 0x3, 0x2, 0x2, 
    0x2, 0x110, 0x111, 0x7, 0x3a, 0x2, 0x2, 0x111, 0x41, 0x3, 0x2, 0x2, 
    0x2, 0x112, 0x113, 0x9, 0x3, 0x2, 0x2, 0x113, 0x43, 0x3, 0x2, 0x2, 0x2, 
    0x114, 0x115, 0x9, 0x4, 0x2, 0x2, 0x115, 0x45, 0x3, 0x2, 0x2, 0x2, 0x116, 
    0x117, 0x9, 0x5, 0x2, 0x2, 0x117, 0x47, 0x3, 0x2, 0x2, 0x2, 0x118, 0x119, 
    0x9, 0x6, 0x2, 0x2, 0x119, 0x49, 0x3, 0x2, 0x2, 0x2, 0x11a, 0x11b, 0x7, 
    0x5, 0x2, 0x2, 0x11b, 0x11c, 0x5, 0x4c, 0x27, 0x2, 0x11c, 0x11d, 0x7, 
    0x7, 0x2, 0x2, 0x11d, 0x4b, 0x3, 0x2, 0x2, 0x2, 0x11e, 0x11f, 0x5, 0x52, 
    0x2a, 0x2, 0x11f, 0x4d, 0x3, 0x2, 0x2, 0x2, 0x120, 0x121, 0x7, 0x5, 
    0x2, 0x2, 0x121, 0x122, 0x5, 0x50, 0x29, 0x2, 0x122, 0x123, 0x7, 0x7, 
    0x2, 0x2, 0x123, 0x4f, 0x3, 0x2, 0x2, 0x2, 0x124, 0x125, 0x5, 0x52, 
    0x2a, 0x2, 0x125, 0x51, 0x3, 0x2, 0x2, 0x2, 0x126, 0x12b, 0x5, 0x54, 
    0x2b, 0x2, 0x127, 0x128, 0x7, 0x6, 0x2, 0x2, 0x128, 0x12a, 0x5, 0x54, 
    0x2b, 0x2, 0x129, 0x127, 0x3, 0x2, 0x2, 0x2, 0x12a, 0x12d, 0x3, 0x2, 
    0x2, 0x2, 0x12b, 0x129, 0x3, 0x2, 0x2, 0x2, 0x12b, 0x12c, 0x3, 0x2, 
    0x2, 0x2, 0x12c, 0x53, 0x3, 0x2, 0x2, 0x2, 0x12d, 0x12b, 0x3, 0x2, 0x2, 
    0x2, 0x12e, 0x131, 0x5, 0x20, 0x11, 0x2, 0x12f, 0x130, 0x7, 0x9, 0x2, 
    0x2, 0x130, 0x132, 0x5, 0x56, 0x2c, 0x2, 0x131, 0x12f, 0x3, 0x2, 0x2, 
    0x2, 0x131, 0x132, 0x3, 0x2, 0x2, 0x2, 0x132, 0x55, 0x3, 0x2, 0x2, 0x2, 
    0x133, 0x135, 0x5, 0x42, 0x22, 0x2, 0x134, 0x133, 0x3, 0x2, 0x2, 0x2, 
    0x134, 0x135, 0x3, 0x2, 0x2, 0x2, 0x135, 0x136, 0x3, 0x2, 0x2, 0x2, 
    0x136, 0x139, 0x5, 0x3a, 0x1e, 0x2, 0x137, 0x138, 0x7, 0x9, 0x2, 0x2, 
    0x138, 0x13a, 0x5, 0x58, 0x2d, 0x2, 0x139, 0x137, 0x3, 0x2, 0x2, 0x2, 
    0x139, 0x13a, 0x3, 0x2, 0x2, 0x2, 0x13a, 0x57, 0x3, 0x2, 0x2, 0x2, 0x13b, 
    0x13c, 0x5, 0x3a, 0x1e, 0x2, 0x13c, 0x59, 0x3, 0x2, 0x2, 0x2, 0x16, 
    0x61, 0x6b, 0x7f, 0x8c, 0xa1, 0xa7, 0xaa, 0xb2, 0xbd, 0xc3, 0xcc, 0xe8, 
    0xf0, 0xfd, 0x102, 0x108, 0x12b, 0x131, 0x134, 0x139, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

Pcl4Parser::Initializer Pcl4Parser::_init;
