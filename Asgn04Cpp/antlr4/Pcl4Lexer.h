
// Generated from Pcl4.g4 by ANTLR 4.9.1

#pragma once


#include "antlr4-runtime.h"




class  Pcl4Lexer : public antlr4::Lexer {
public:
  enum {
    T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, 
    T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, 
    T__14 = 15, T__15 = 16, T__16 = 17, PROGRAM = 18, CONST = 19, TYPE = 20, 
    ARRAY = 21, OF = 22, RECORD = 23, VAR = 24, BEGIN = 25, END = 26, DIV = 27, 
    MOD = 28, AND = 29, OR = 30, NOT = 31, IF = 32, THEN = 33, ELSE = 34, 
    CASE = 35, REPEAT = 36, UNTIL = 37, WHILE = 38, DO = 39, FOR = 40, TO = 41, 
    DOWNTO = 42, WRITE = 43, WRITELN = 44, READ = 45, READLN = 46, PROCEDURE = 47, 
    FUNCTION = 48, IDENTIFIER = 49, INTEGER = 50, REAL = 51, NEWLINE = 52, 
    WS = 53, QUOTE = 54, CHARACTER = 55, STRING = 56
  };

  explicit Pcl4Lexer(antlr4::CharStream *input);
  ~Pcl4Lexer();

  virtual std::string getGrammarFileName() const override;
  virtual const std::vector<std::string>& getRuleNames() const override;

  virtual const std::vector<std::string>& getChannelNames() const override;
  virtual const std::vector<std::string>& getModeNames() const override;
  virtual const std::vector<std::string>& getTokenNames() const override; // deprecated, use vocabulary instead
  virtual antlr4::dfa::Vocabulary& getVocabulary() const override;

  virtual const std::vector<uint16_t> getSerializedATN() const override;
  virtual const antlr4::atn::ATN& getATN() const override;

private:
  static std::vector<antlr4::dfa::DFA> _decisionToDFA;
  static antlr4::atn::PredictionContextCache _sharedContextCache;
  static std::vector<std::string> _ruleNames;
  static std::vector<std::string> _tokenNames;
  static std::vector<std::string> _channelNames;
  static std::vector<std::string> _modeNames;

  static std::vector<std::string> _literalNames;
  static std::vector<std::string> _symbolicNames;
  static antlr4::dfa::Vocabulary _vocabulary;
  static antlr4::atn::ATN _atn;
  static std::vector<uint16_t> _serializedATN;


  // Individual action functions triggered by action() above.

  // Individual semantic predicate functions triggered by sempred() above.

  struct Initializer {
    Initializer();
  };
  static Initializer _init;
};

