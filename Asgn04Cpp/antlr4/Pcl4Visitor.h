
// Generated from Pcl4.g4 by ANTLR 4.9.1

#pragma once


#include "antlr4-runtime.h"
#include "Pcl4Parser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by Pcl4Parser.
 */
class  Pcl4Visitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by Pcl4Parser.
   */
    virtual antlrcpp::Any visitProgram(Pcl4Parser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitProgramHeader(Pcl4Parser::ProgramHeaderContext *context) = 0;

    virtual antlrcpp::Any visitProgramParameters(Pcl4Parser::ProgramParametersContext *context) = 0;

    virtual antlrcpp::Any visitBlock(Pcl4Parser::BlockContext *context) = 0;

    virtual antlrcpp::Any visitDeclarations(Pcl4Parser::DeclarationsContext *context) = 0;

    virtual antlrcpp::Any visitStatement(Pcl4Parser::StatementContext *context) = 0;

    virtual antlrcpp::Any visitCompoundStatement(Pcl4Parser::CompoundStatementContext *context) = 0;

    virtual antlrcpp::Any visitEmptyStatement(Pcl4Parser::EmptyStatementContext *context) = 0;

    virtual antlrcpp::Any visitStatementList(Pcl4Parser::StatementListContext *context) = 0;

    virtual antlrcpp::Any visitAssignmentStatement(Pcl4Parser::AssignmentStatementContext *context) = 0;

    virtual antlrcpp::Any visitRepeatStatement(Pcl4Parser::RepeatStatementContext *context) = 0;

    virtual antlrcpp::Any visitLhs(Pcl4Parser::LhsContext *context) = 0;

    virtual antlrcpp::Any visitRhs(Pcl4Parser::RhsContext *context) = 0;

    virtual antlrcpp::Any visitWriteStatement(Pcl4Parser::WriteStatementContext *context) = 0;

    virtual antlrcpp::Any visitWritelnStatement(Pcl4Parser::WritelnStatementContext *context) = 0;

    virtual antlrcpp::Any visitExpression(Pcl4Parser::ExpressionContext *context) = 0;

    virtual antlrcpp::Any visitSimpleExpression(Pcl4Parser::SimpleExpressionContext *context) = 0;

    virtual antlrcpp::Any visitCaseStatement(Pcl4Parser::CaseStatementContext *context) = 0;

    virtual antlrcpp::Any visitCases(Pcl4Parser::CasesContext *context) = 0;

    virtual antlrcpp::Any visitWhileStatement(Pcl4Parser::WhileStatementContext *context) = 0;

    virtual antlrcpp::Any visitForStatement(Pcl4Parser::ForStatementContext *context) = 0;

    virtual antlrcpp::Any visitForRange(Pcl4Parser::ForRangeContext *context) = 0;

    virtual antlrcpp::Any visitIfStatement(Pcl4Parser::IfStatementContext *context) = 0;

    virtual antlrcpp::Any visitTerm(Pcl4Parser::TermContext *context) = 0;

    virtual antlrcpp::Any visitVariableExpression(Pcl4Parser::VariableExpressionContext *context) = 0;

    virtual antlrcpp::Any visitNumberExpression(Pcl4Parser::NumberExpressionContext *context) = 0;

    virtual antlrcpp::Any visitCharacterFactor(Pcl4Parser::CharacterFactorContext *context) = 0;

    virtual antlrcpp::Any visitStringFactor(Pcl4Parser::StringFactorContext *context) = 0;

    virtual antlrcpp::Any visitNotFactor(Pcl4Parser::NotFactorContext *context) = 0;

    virtual antlrcpp::Any visitParenthesizedExpression(Pcl4Parser::ParenthesizedExpressionContext *context) = 0;

    virtual antlrcpp::Any visitVariable(Pcl4Parser::VariableContext *context) = 0;

    virtual antlrcpp::Any visitNumber(Pcl4Parser::NumberContext *context) = 0;

    virtual antlrcpp::Any visitUnsignedNumber(Pcl4Parser::UnsignedNumberContext *context) = 0;

    virtual antlrcpp::Any visitIntegerConstant(Pcl4Parser::IntegerConstantContext *context) = 0;

    virtual antlrcpp::Any visitRealConstant(Pcl4Parser::RealConstantContext *context) = 0;

    virtual antlrcpp::Any visitCharacterConstant(Pcl4Parser::CharacterConstantContext *context) = 0;

    virtual antlrcpp::Any visitStringConstant(Pcl4Parser::StringConstantContext *context) = 0;

    virtual antlrcpp::Any visitSign(Pcl4Parser::SignContext *context) = 0;

    virtual antlrcpp::Any visitRelOp(Pcl4Parser::RelOpContext *context) = 0;

    virtual antlrcpp::Any visitAddOp(Pcl4Parser::AddOpContext *context) = 0;

    virtual antlrcpp::Any visitMulOp(Pcl4Parser::MulOpContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgumentsOn(Pcl4Parser::WriteArgumentsOnContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgumentListOn(Pcl4Parser::WriteArgumentListOnContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgumentsLn(Pcl4Parser::WriteArgumentsLnContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgumentListLn(Pcl4Parser::WriteArgumentListLnContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgumentList(Pcl4Parser::WriteArgumentListContext *context) = 0;

    virtual antlrcpp::Any visitWriteArgument(Pcl4Parser::WriteArgumentContext *context) = 0;

    virtual antlrcpp::Any visitFieldWidth(Pcl4Parser::FieldWidthContext *context) = 0;

    virtual antlrcpp::Any visitDecimalPlaces(Pcl4Parser::DecimalPlacesContext *context) = 0;


};

