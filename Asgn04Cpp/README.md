

## To set up environment
```bash
export CLASSPATH="${PWD}/lib/antlr-4.9.1-complete.jar:$CLASSPATH"
alias antlr4="java -jar ${PWD}/lib/antlr-4.9.1-complete.jar"
alias grun='java org.antlr.v4.gui.TestRig'
```
## Create parser library from antlr4
```bash
antlr4 -Dlanguage=Cpp -no-listener -visitor -encoding UTF-8 -o antlr4 Pcl4.g4
```
## Compile

### Compile the project with g++
```bash
g++ -std=c++11 -I${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/antlr4-runtime -L${PWD}/antlr4-cpp-runtime-4-9-1-MacOS/lib  -I${PWD}/antlr4 -I${PWD}/backend/interpreter/ -I${PWD}/intermediate/symtab/ -lantlr4-runtime ${PWD}/antlr4/*.cpp backend/interpreter/Executor.cpp -o Pcl4 Pcl4.cpp
```

### Compile the project with Cmake
```bash
cmake build .
make
```

## Run sample program
```bash
./Pcl4 -execute inputs/HelloWorld.txt
```

### To build grun UI tool and run
```bash
antlr4 Pcl4.g4 -o grun-java
javac grun-java/*.java

cd grun-java;
grun Pcl4 program -gui ../inputs/HelloWorld.txt
```

### Generate outputs
```bash
./Pcl4 -execute inputs/TestFor.txt > Outputs/ExecutionOutput/TestFor.out.txt
./Pcl4 -execute inputs/TestIf.txt > Outputs/ExecutionOutput/TestIf.out.txt
./Pcl4 -execute inputs/TestWhile.txt > Outputs/ExecutionOutput/TestWhile.out.txt
./Pcl4 -execute inputs/TestCase.txt > Outputs/ExecutionOutput/TestCase.out.txt
./Pcl4 -execute inputs/HelloWorld.txt > Outputs/ExecutionOutput/HelloWorld.out.txt

```


## Testing purpose, ignore this
```bash
#(https://github.com/bkiers/rrd-antlr4)
java -jar PATH/TO/rrd-antlr4-0.1.2.jar /Volumes/Data/CMPE152/git/assignments/Asgn04Cpp/Pcl4.g4 
```