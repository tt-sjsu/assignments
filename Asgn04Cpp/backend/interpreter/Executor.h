#ifndef EXECUTOR_H_
#define EXECUTOR_H_

#include <string>
#include <map>

#include "antlr4-runtime.h"
#include "Pcl4BaseVisitor.h"

#include "../Object.h"
#include "Symtab.h"
namespace backend { namespace interpreter {

using namespace std;
using namespace intermediate::symtab;

typedef antlrcpp::Any Object;

struct StringLiteral {
    string text;
    StringLiteral(string text) : text(text) {};
    StringLiteral(const StringLiteral&) noexcept = default; // trivial and non-throwing
};

class Executor : public Pcl4BaseVisitor
{
public:
    Executor() {}
    virtual ~Executor() {}

    Object visitProgram(Pcl4Parser::ProgramContext *ctx) override;
    Object visitStatement(Pcl4Parser::StatementContext *ctx) override;
    Object visitStatementList(Pcl4Parser::StatementListContext *ctx) override;
    Object visitCompoundStatement(Pcl4Parser::CompoundStatementContext *ctx) override;
    Object visitAssignmentStatement(Pcl4Parser::AssignmentStatementContext *ctx) override;
    Object visitRepeatStatement(Pcl4Parser::RepeatStatementContext *ctx) override;
    Object visitWhileStatement(Pcl4Parser::WhileStatementContext *ctx) override;
    Object visitForStatement(Pcl4Parser::ForStatementContext *ctx) override;
    Object visitIfStatement(Pcl4Parser::IfStatementContext *ctx) override;
    Object visitCaseStatement(Pcl4Parser::CaseStatementContext *ctx) override;
    Object visitWriteStatement(Pcl4Parser::WriteStatementContext *ctx) override;
    Object visitWritelnStatement(Pcl4Parser::WritelnStatementContext *ctx) override;
    Object visitExpression(Pcl4Parser::ExpressionContext *ctx) override;
    Object visitParenthesizedExpression(Pcl4Parser::ParenthesizedExpressionContext *ctx) override;
    Object visitSimpleExpression(Pcl4Parser::SimpleExpressionContext *ctx) override;
    Object visitTerm(Pcl4Parser::TermContext *ctx) override;
    Object visitVariable(Pcl4Parser::VariableContext *ctx) override;
    Object visitNotFactor(Pcl4Parser::NotFactorContext *ctx) override;
    Object visitNumber(Pcl4Parser::NumberContext *ctx) override;
    Object visitRealConstant(Pcl4Parser::RealConstantContext *ctx) override;
    Object visitIntegerConstant(Pcl4Parser::IntegerConstantContext *ctx) override;
    Object visitCharacterConstant(Pcl4Parser::CharacterConstantContext *ctx) override;
    Object visitStringConstant(Pcl4Parser::StringConstantContext *ctx) override;
private:
    Symtab symtab;
    double convertValueToDouble(Object obj);
    void runtimeError(string message);
    void printValue(Pcl4Parser::WriteArgumentContext *ctx);

    // Complete this class!

};

}}  // namespace backend::interpreter

#endif /* EXECUTOR_H_ */
