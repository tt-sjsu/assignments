#include <iostream>

#include "../Object.h"
#include "Executor.h"

namespace backend { namespace interpreter {

using namespace std;

Object Executor::visitProgram(Pcl4Parser::ProgramContext *ctx)
{
    return visit(ctx->block());
}

Object Executor::visitStatement(Pcl4Parser::StatementContext *ctx)
{
    return visitChildren(ctx);
}

Object Executor::visitStatementList(Pcl4Parser::StatementListContext *ctx)
{
    for (Pcl4Parser::StatementContext *stmtCtx : ctx->statement())
    {
        visit(stmtCtx);
    }

    return nullptr;
}

Object Executor::visitCompoundStatement(Pcl4Parser::CompoundStatementContext *ctx)
{
    return visit(ctx->statementList());
}

Object Executor::visitAssignmentStatement(Pcl4Parser::AssignmentStatementContext *ctx)
{
    string variableName = ctx->lhs()->variable()->getText();
    visit(ctx->lhs());
    Object value = visit(ctx->rhs());
    SymtabEntry *entry = symtab.lookup(variableName);       //get entry from the table
    if (!entry) { // entry not created yet, create it
        entry = symtab.enter(variableName);
    }
    entry->setValue(convertValueToDouble(value)); // store it into the symbol table
    return value;
}

Object Executor::visitRepeatStatement(Pcl4Parser::RepeatStatementContext *ctx)
{
    Object check = visit(ctx->expression());
    Object value;
    while(!check.as<bool>()) {
        value = visit(ctx->statementList());
        check = visit(ctx->expression());
    }
    return value;
}

Object Executor::visitWhileStatement(Pcl4Parser::WhileStatementContext *ctx)
{
    Object check = visit(ctx->expression());
    Object value;
    while(check.as<bool>()) {
        value = visit(ctx->statement());
        check = visit(ctx->expression());
    }
    return value;
}

Object Executor::visitForStatement(Pcl4Parser::ForStatementContext *ctx)
{
    string variable = ctx->variable()->getText();
    SymtabEntry *entry = symtab.lookup(variable);       //get entry from the table
    if (!entry) { // entry not created yet, create it
        entry = symtab.enter(variable);
    }


    Object begin = visit(ctx->forRange()->expression(0));
    Object end = visit(ctx->forRange()->expression(1));
    bool is_up = true;
    if(ctx->forRange()->DOWNTO()) {
        is_up = false;
    }

    entry->setValue(convertValueToDouble(begin));

    while(is_up && entry->getValue() <= convertValueToDouble(end)) {
        visit(ctx->statement());
        entry->setValue(entry->getValue() + 1);
    }

    while(!is_up && entry->getValue() >= convertValueToDouble(end)) {
        visit(ctx->statement());
        entry->setValue(entry->getValue() - 1);
    }

    return nullptr;
}

Object Executor::visitIfStatement(Pcl4Parser::IfStatementContext *ctx)
{
    Object check = visit(ctx->expression());
    if(check.as<bool>()) {
        visit(ctx->statement(0));
    }
    else {
        if(ctx->statement().size() > 1) {
            visit(ctx->statement(1));
        }
    }
    return nullptr;
}
Object Executor::visitCaseStatement(Pcl4Parser::CaseStatementContext *ctx)
{
    Object check = visit(ctx->expression());
    bool found = false;
    for(int i = 0; i < ctx->cases().size(); i++) {
        for (int j = 0; j < ctx->cases(i)->expression().size(); j++) {
           Object case_check = visit(ctx->cases(i)->expression(j));
            if ((check.is<int>() || check.is<double>()) && convertValueToDouble(check) ==  convertValueToDouble(case_check)) {
                found = true;
                break;
            }
            else if (check.is<StringLiteral>() && check.as<StringLiteral>().text == case_check.as<StringLiteral>().text) {
                found = true;
                break;
            }
        }
        if (found) {
            visit(ctx->cases(i));
            break;
        }
    }
    return nullptr;
}

Object Executor::visitWriteStatement(Pcl4Parser::WriteStatementContext *ctx)
{
    for (int i = 0; i < ctx->writeArgumentsOn()->writeArgumentListOn()->writeArgumentList()->writeArgument().size(); i++) {
        printValue(ctx->writeArgumentsOn()->writeArgumentListOn()->writeArgumentList()->writeArgument(i));
    }
    return Object();
}

Object Executor::visitWritelnStatement(Pcl4Parser::WritelnStatementContext *ctx)
{
    if (ctx->writeArgumentsLn()) {
        for (int i = 0; i < ctx->writeArgumentsLn()->writeArgumentListLn()->writeArgumentList()->writeArgument().size(); i++) {
            printValue(ctx->writeArgumentsLn()->writeArgumentListLn()->writeArgumentList()->writeArgument(i));
            cout << endl;
        }
    }
    else {
        cout << endl;
    }

    return Object();
}

Object Executor::visitExpression(Pcl4Parser::ExpressionContext *ctx)
{
    if (!ctx->relOp()) {
        return visitChildren(ctx);
    }

    Object value1 = visit(ctx->children[0]);
    Object value2 = visit(ctx->children[2]);
    bool value = false;
//    relOp : '=' | '<>' | '<' | '<=' | '>' | '>=' ;

    string op = ctx->relOp()->getText();
    if (op == "=") {
        value = convertValueToDouble(value1) == convertValueToDouble(value2);
    }
    else if (op == "<>") {
        value = convertValueToDouble(value1) != convertValueToDouble(value2);
    }
    else if (op == "<>") {
        value = convertValueToDouble(value1) != convertValueToDouble(value2);
    }
    else if (op == "<") {
        value = convertValueToDouble(value1) < convertValueToDouble(value2);
    }
    else if (op == "<=") {
        value = convertValueToDouble(value1) <= convertValueToDouble(value2);
    }
    else if (op == ">") {
        value = convertValueToDouble(value1) > convertValueToDouble(value2);
    }
    else if (op == ">=") {
        value = convertValueToDouble(value1) >= convertValueToDouble(value2);
    }

//        case AND : value = value1.B && value2.B; break;
//        case OR  : value = value1.B || value2.B; break;
    return value;
}

Object Executor::visitSimpleExpression(Pcl4Parser::SimpleExpressionContext *ctx)
{
    bool negative = ctx->sign() && ctx->sign()->getText() == "-";

    Object value1, value2;
    value1 = visit(ctx->term(0));
    if (negative) {
        value1 = -1 * convertValueToDouble(value1);
    }

    for (int i = 0; i < ctx->addOp().size(); i++) {
        string op = toLowerCase(ctx->addOp(i)->getText());
        value2 = visit(ctx->term(i + 1));

        if (op == "+") {
            value1 = convertValueToDouble(value1) + convertValueToDouble(value2);
        }
        else if (op == "-") {
            value1 = convertValueToDouble(value1) - convertValueToDouble(value2);
        }
        else if (op == "or") {
            value1 = value1.as<bool>() || value2.as<bool>();
        }
    }
    return value1;
}

Object Executor::visitTerm(Pcl4Parser::TermContext *ctx)
{

    Object value1, value2;
    value1 = visit(ctx->factor(0));


    for (int i = 0; i < ctx->mulOp().size(); i++) {
        string op = toLowerCase(ctx->mulOp(i)->getText());
        value2 = visit(ctx->factor(i + 1));

        if (op == "*") {
            value1 = convertValueToDouble(value1) * convertValueToDouble(value2);
        }
        else if (op == "/"  || op == "div") {
            value1 = convertValueToDouble(value1) / convertValueToDouble(value2);
        }
        else if (op == "mod") {
            value1 = value1.as<int>() % value2.as<int>();
        }
        else if (op == "and") {
            value1 = value1.as<bool>() && value2.as<bool>();
        }
    }
    return value1;
}

Object Executor::visitNotFactor(Pcl4Parser::NotFactorContext *ctx)
{
    Object value = visit(ctx->factor());

    if (value.is<double>()) {
        value = value.as<double>() * -1;
    }
    else if (value.is<int>()) {
        value = value.as<int>() * -1;
    }
    else if (value.is<bool>()) {
        value = !value.as<bool>();
    }

    return value;
}

Object Executor::visitParenthesizedExpression(Pcl4Parser::ParenthesizedExpressionContext *ctx)
{
    return visit(ctx->expression());
}

Object Executor::visitVariable(Pcl4Parser::VariableContext *ctx)
{
    string variableName = ctx->getText();
    SymtabEntry *entry = symtab.lookup(variableName);
    if (entry) {
        return entry->getValue();
    }
    return nullptr;  // should return the variable's value!
}

Object Executor::visitNumber(Pcl4Parser::NumberContext *ctx)
{
    int sign = ctx->sign() && ctx->sign()->getText() == "-" ? -1 : 1;
    Object value = visit(ctx->unsignedNumber());
    if (sign == -1) {
        if (value.is<double>()) {
            value = value.as<double>() * -1;
        }
        else if (value.is<int>()) {
            value = value.as<int>() * -1;
        }
    }

    return value;
}

Object Executor::visitIntegerConstant(Pcl4Parser::IntegerConstantContext *ctx)
{
    return stoi(ctx->INTEGER()->getText());
}
Object Executor::visitRealConstant(Pcl4Parser::RealConstantContext *ctx)
{
    return stod(ctx->REAL()->getText());
}
Object Executor::visitCharacterConstant(Pcl4Parser::CharacterConstantContext *ctx)
{
    string text = ctx->CHARACTER()->getText();
    text =  text.substr(1, text.length() - 2);
    StringLiteral stringLiteral(text);
    return stringLiteral;
}

Object Executor::visitStringConstant(Pcl4Parser::StringConstantContext *ctx)
{
    string text = ctx->STRING()->getText();
    text =  text.substr(1, text.length() - 2);
    // a hack to bypass Any object, it requires to have a type with is_nothrow_copy_constructible
    // antlr4-runtime/support/Any.h
    // template<int N = 0, typename std::enable_if<N == N && std::is_nothrow_copy_constructible<T>::value, int>::type = 0>
    StringLiteral stringLiteral(text);
    return stringLiteral;
}

double Executor::convertValueToDouble(Object obj) {
    if (obj.is<int>()) {
        return (double) obj.as<int>();
    } else if (obj.is<double>()) {
        return obj.as<double>();
    }
    return 0;
}

void Executor::runtimeError(string message)
{
//    printf("RUNTIME ERROR at line %d: %s: %s\n",
//           lineNumber, message.c_str(), node->text.c_str());
    printf("RUNTIME ERROR: %s\n", message.c_str());
    exit(-2);
}

void Executor::printValue(Pcl4Parser::WriteArgumentContext *ctx)
{
    Object v = visit(ctx->expression());

    int fieldWidth    = -1;
    int decimalPlaces = 0;

    if (ctx->fieldWidth()) {
        fieldWidth = visit(ctx->fieldWidth()->integerConstant()).as<int>();

        if(ctx->fieldWidth()->decimalPlaces()) {
            decimalPlaces = visit(ctx->fieldWidth()->decimalPlaces()).as<int>();
        }

    }

    string format = "%";
    if (fieldWidth >= 0)    format += to_string(fieldWidth);
    if (decimalPlaces >= 0) format += "." + to_string(decimalPlaces);
    format += "f";



    if (v.is<int>()) {
        printf(format.c_str(), v.as<int>());
    } else if (v.is<double>()) {
        printf(format.c_str(), v.as<double>());
    } else if (v.is<StringLiteral>()) {
        cout << v.as<StringLiteral>().text;
    }
}
// Complete this class!

}}  // namespace backend::interpreter
